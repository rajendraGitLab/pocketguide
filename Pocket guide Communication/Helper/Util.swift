//
//  Util.swift
//  Ma2asat
//
//  Created by Deepak Kumar on 12/10/20.
//  Copyright © 2020 Yaas. All rights reserved.
//

import UIKit
import CoreLocation
import SideMenu
import MBProgressHUD
import SDWebImage

class Util: NSObject {
    
    static func setTabRootController(selectedtab:Int){
        
//        if let vc = SB_Tab.instantiateViewController(withIdentifier: "TB_Main") as? TB_Main{
//            DEFAULTS.set(true, forKey: keyIsUserLogin)
//            let nav = UINavigationController(rootViewController: vc)
//            nav.interactivePopGestureRecognizer?.delegate = nil
//            nav.setNavigationBarHidden(true, animated: false)
//            vc.selectedIndex = selectedtab
//            window?.rootViewController = nav
//            window?.makeKeyAndVisible()
//        }
        
    }
    
    static func setLoginRootController(){
        
//        if let vc = SB_Login.instantiateViewController(withIdentifier: "VC_Login") as? VC_Login{
//            DEFAULTS.set(true, forKey: keyIsUserLogin)
//            let nav = UINavigationController(rootViewController: vc)
//            nav.interactivePopGestureRecognizer?.delegate = nil
//            nav.setNavigationBarHidden(true, animated: false)
//            window?.rootViewController = nav
//            window?.makeKeyAndVisible()
//        }
        
    }
    
    static func showHud(view:UIView){
        MBProgressHUD.showAdded(to: view, animated: false)
    }
    
    static func showHud(view:UIView, text:String, detail:String){
        
        let hud = MBProgressHUD.showAdded(to: view, animated: false)
        hud.label.text = text
        hud.isUserInteractionEnabled = false
        hud.detailsLabel.text = detail
        
    }
    
    static func hideHud(view:UIView){
        
        DispatchQueue.main.async {
            MBProgressHUD.hide(for: view, animated: false)
        }
    }
    
    static func saveImageInDocDir(image:UIImage, name:String){
        
        let fileURL = Util.getDocDirPath(name: name)
        // get your UIImage jpeg data representation and check if the destination file url already exists
        if let data = image.jpegData(compressionQuality:  0.5),
           !FileManager.default.fileExists(atPath: fileURL.path) {
            do {
                // writes the image data to disk
                try data.write(to: fileURL)
                print("file saved")
            } catch {
                print("error saving file:", error)
            }
        }
    }
    
    static func getImageDocDir(name:String) -> UIImage?{
        
        let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
        let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
        if let dirPath          = paths.first
        {
            let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent("\(name).jpg")
            let image    = UIImage(contentsOfFile: imageURL.path)
            return image
            // Do whatever you want with the image
        }
        
        return nil
    }
    
    static func getDocDirPath( name:String) -> URL{
        // get the documents directory url
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        // choose a name for your image
        let fileName = "\(name).jpg"
        // create the destination file url to save your image
        let fileURL = documentsDirectory.appendingPathComponent(fileName)
        
        return fileURL
    }
    
    static func clearAllFile() {
        let fileManager = FileManager.default
        let myDocuments = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first!
        do {
            print("myDocuments \(myDocuments.absoluteString)")
            try fileManager.removeItem(at: myDocuments)
        } catch {
            return
        }
    }
    
    static func removeFileFromDocDir(filename:String){
        
        let fileNameToDelete = filename
        var filePath = ""
        
        // Fine documents directory on device
        let dirs : [String] = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.allDomainsMask, true)
        
        if dirs.count > 0 {
            let dir = dirs[0] //documents directory
            filePath = dir.appendingFormat("/" + fileNameToDelete)
            print("Local path = \(filePath)")
            
        } else {
            print("Could not find local directory to store file")
            return
        }
        
        
        do {
            let fileManager = FileManager.default
            
            // Check if file exists
            if fileManager.fileExists(atPath: filePath) {
                // Delete file
                try fileManager.removeItem(atPath: filePath)
            } else {
                print("File does not exist")
            }
            
        }
        catch let error as NSError {
            print("An error took place: \(error)")
        }
    }
    
    static func fileExist(name:String) -> Bool{
        
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let url = NSURL(fileURLWithPath: path)
        if let pathComponent = url.appendingPathComponent("\(name).jpg") {
            let filePath = pathComponent.path
            let fileManager = FileManager.default
            if fileManager.fileExists(atPath: filePath) {
                print("FILE AVAILABLE")
                return true
            } else {
                print("FILE NOT AVAILABLE")
                return false
            }
        } else {
            print("FILE PATH NOT AVAILABLE")
            return false
        }
    }
    
    static func resetDefaults() {
        
        let defaults = UserDefaults.standard
        let dictionary = defaults.dictionaryRepresentation()
        dictionary.keys.forEach { key in
            
            if key == UserDefaultKeys.kLanguage{
                
            }
            else{
                defaults.removeObject(forKey: key)
            }
            
        }
        defaults.synchronize()
        
//        var selectedLanguage = Languages.ar
//        if Util.getCurrentLanguage() == en {
//            selectedLanguage = Languages.en
//        }
//        LanguageManager.shared.setLanguage(language: selectedLanguage)
    }
    
    
    static func setCartCount(label:UILabel, controller:UIViewController, userId:String){
        
        label.clipsToBounds = true
        label.layer.cornerRadius = label.frame.height/2
        let currentCount = Int(label.text ?? "") ?? 0
        label.backgroundColor = .clear
        label.textColor = .clear
        if currentCount > 0{
            label.backgroundColor = .red
            label.textColor = .white
        }
        
       /* let parameter : [String:Any] = ["method":API_METHOD.cartcount,
                                        "userId":userId]
        APIUtil.APICall(postName: BASE_URL, method: .post, parameters: parameter, controller: controller, onSuccess: { (response) in
            
            let data = response as! NSDictionary
            if let cartCount = data.value(forKeyPath: "response.CartCount") as? Int{
                
                DispatchQueue.main.async {
                    
                    label.text = "\(cartCount)"
                    if cartCount > 8{
                        label.text = "9+"
                    }
                    
                    if cartCount == 0{
                        label.text = ""
                        label.backgroundColor = .clear
                        label.textColor = .clear
                    }else{
                        label.backgroundColor = .red
                        label.textColor = .white
                    }
                    
                }
            }
            
            
        }) { (reason, statusCode) in
            
        }*/
        
        
    }
    
    
    /*static func getFormattedAddress(address:Address) -> String{
        
        var fAddress = ""
        
        fAddress = "\(address.address ?? ""), \(address.area ?? ""), (\(address.landmark ?? "")), \(address.city ?? ""), \(address.country ?? ""), \(address.pin ?? "")"
        
        return fAddress
    }
    */
    static func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
    
    static func logError(text:String){
        print("-------SKG------- Error \n\(text)")
    }
}


//MARK: - Side Menu
extension Util{
    
    static func makeSettings(view:UIView) -> SideMenuSettings {
        
        let presentationStyle = selectedPresentationStyle()
        presentationStyle.menuStartAlpha = 0.5
        presentationStyle.menuScaleFactor = 1.5
        presentationStyle.onTopShadowOpacity = 0.3
        presentationStyle.presentingEndAlpha = 0.5
        presentationStyle.presentingScaleFactor = 1
        var settings = SideMenuSettings()
        settings.presentationStyle = presentationStyle
        settings.menuWidth = min(view.frame.width, view.frame.height) * 1
        let styles:[UIBlurEffect.Style?] = [nil, .dark, .light, .extraLight]
        settings.blurEffectStyle = styles[0]
        settings.statusBarEndAlpha = 0//blackOutStatusBar.isOn ? 1 : 0
        return settings
    }
    
    static func selectedPresentationStyle() -> SideMenuPresentationStyle {
        let modes: [SideMenuPresentationStyle] = [.menuSlideIn, .viewSlideOut, .viewSlideOutMenuIn, .menuDissolveIn]
        return modes[0]
    }
    
    static func showSideMenu(navC:UIViewController){
        
        let menuVC : Side_Menu = SB_Side_Menu.instantiateViewController(withIdentifier: "Side_Menu") as! Side_Menu
        let menu = SideMenuNavigationController(rootViewController: menuVC)
        //DPK
        menu.isNavigationBarHidden = true

        SideMenuManager.default.leftMenuNavigationController = menu

        menu.leftSide = true
        SideMenuManager.default.addPanGestureToPresent(toView: navC.view)
        SideMenuManager.default.addScreenEdgePanGesturesToPresent(toView: navC.view)
        menu.statusBarEndAlpha = 1

        menu.settings = Util.makeSettings(view: navC.view)

        navC.present(menu, animated: true, completion: nil)
        
//        let menuVC : Side_Menu = SB_Side_Menu.instantiateViewController(withIdentifier: "Side_Menu") as! Side_Menu
//        let menu = UINavigationController(rootViewController: menuVC)
//        let transition = CATransition()
//        transition.duration = 0.5
//        transition.type = CATransitionType.reveal
//        transition.subtype = CATransitionSubtype.fromLeft
//        transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
//        navC.view.window!.layer.add(transition, forKey: kCATransition)
//        navC.present(menu, animated: false, completion: nil)
        
    }
}

/*
extension Util{
    
    static func getAddressFromLatLon(pdblLatitude: Double, withLongitude pdblLongitude: Double, onSuccess success: @escaping (_ address: Address) -> Void){
        
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = pdblLatitude
        //21.228124
        let lon: Double = pdblLongitude
        //72.833770
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        
        var address = Address()
        address.latitude = "\(pdblLatitude)"
        address.longitude = "\(pdblLongitude)"
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        
        ceo.reverseGeocodeLocation(loc, completionHandler:
                                    {(placemarks, error) in
                                        if (error != nil)
                                        {
                                            print("reverse geodcode fail: \(error!.localizedDescription)")
                                        }
                                        let pm = placemarks! as [CLPlacemark]
                                        
                                        if pm.count > 0 {
                                            let pm = placemarks![0]
                                            var addressString : String = ""
                                            if pm.subLocality != nil {
                                                addressString = addressString + pm.subLocality! + ", "
                                                address.area = pm.subLocality!
                                            }
                                            if pm.thoroughfare != nil {
                                                addressString = addressString + pm.thoroughfare! + ", "
                                                address.address = pm.thoroughfare!
                                            }
                                            if pm.locality != nil {
                                                addressString = addressString + pm.locality! + ", "
                                                address.city = pm.locality!
                                            }
                                            if pm.country != nil {
                                                addressString = addressString + pm.country! + ", "
                                                address.country = pm.country!
                                            }
                                            if pm.postalCode != nil {
                                                addressString = addressString + pm.postalCode! + " "
                                                address.pin = pm.postalCode!
                                            }
                                            
                                            success(address)
                                            print(addressString)
                                        }
                                    })
        
    }
}
 */


extension Util{
    
    static func getCurrentLanguage() -> String{
        var language = "ar"
        if let lng = DEFAULTS.value(forKey: UserDefaultKeys.kLanguage) as? String{
            language = lng
        }
        return language
    }
    
    static func setImage(imageView:UIImageView, imagepath:String){
        
        if imagepath != ""
        {
            imageView.sd_imageIndicator = SDWebImageActivityIndicator.white
            
            if let urlString = imagepath.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed){
                if let imageUrl = URL(string: urlString)  {
                    imageView.sd_setImage(with: imageUrl, placeholderImage:UIImage(named: "placeholder"), options: SDWebImageOptions.progressiveLoad) { (image, error, cacheType, url) in
                        if image != nil{
                            imageView.image = image
                        }
                        else{
                            imageView.image = UIImage(named: "placeholder")
                        }
                        
                    }
                }
            }
        }
        else{
            imageView.image = UIImage(named: "placeholder")
        }
        
        
        
    }
    
}

class DEFAULTSUtils: NSObject {
    
    
    //Current Coordinates
//    static func saveProfileInDefaults(profile:Profile){
//        let encoder = JSONEncoder()
//        if let encoded = try? encoder.encode(profile) {
//            DEFAULTS.set(encoded, forKey: UserDefaultKeys.kProfile)
//        }
//    }
//
//    static func getSavedProfileFromDefaults() -> Profile?{
//
//        if let savedData = DEFAULTS.object(forKey: UserDefaultKeys.kProfile) as? Data {
//            let decoder = JSONDecoder()
//            if let loadedData = try? decoder.decode(Profile.self, from: savedData) {
//                return loadedData
//            }
//        }
//
//        return nil
//    }
 
}


extension UIViewController {
    
    func methodMakeRootTabbar(index : Int){
        let strbd = UIStoryboard(name: "Tab_bar", bundle: nil)
        let initialVC = strbd.instantiateViewController(withIdentifier: "Tab_bar") as! UITabBarController
        initialVC.selectedIndex = index
        let nav = UINavigationController(rootViewController: initialVC)
        nav.isNavigationBarHidden = true
        self.window?.rootViewController = nav
        self.window?.makeKeyAndVisible()
    }
}
extension UIViewController {
    
    var window: UIWindow? {
        if #available(iOS 13, *) {
            guard let windowScene = UIApplication.shared.connectedScenes.first as? UIWindowScene,
                let delegate = windowScene.delegate as? SceneDelegate, let window = delegate.window else { return nil }
                   return window
        }
        
        guard let delegate = UIApplication.shared.delegate as? AppDelegate, let window = delegate.window else { return nil }
        return window
    }
}
