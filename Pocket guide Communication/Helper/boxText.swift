//
//  boxText.swift
//  Pocket guide Communication
//
//  Created by Admin on 16/09/22.
//

import Foundation
import UIKit

class imageHelper{
    static func pgImage(textValue:String,txtColor: UIColor) ->UIImage{
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 15, height: 16))
        label.lineBreakMode = .byClipping
        label.textAlignment = .center
        label.textColor = txtColor
        label.layer.borderColor = UIColor.systemBlue.cgColor
        label.layer.borderWidth = 2
        label.layer.cornerRadius = 2
        label.text = textValue
        label.sizeToFit()
        label.bounds = CGRect(x: 0, y: 0.5, width: label.bounds.size.width + 8, height: label.bounds.size.height + 4)

        UIGraphicsBeginImageContextWithOptions(label.bounds.size, false, UIScreen.main.scale)
        label.layer.allowsEdgeAntialiasing = true
        label.layer.render(in: UIGraphicsGetCurrentContext()!)

        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
}
