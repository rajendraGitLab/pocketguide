//
//  APIUtil.swift
//  Ma2asat
//
//  Created by Deepak Kumar on 19/10/20.
//  Copyright © 2020 JPLoft. All rights reserved.
//

import UIKit
import Alamofire
import SystemConfiguration
import Foundation

//class APIUtil: NSObject {
//
//
//    static func prepareModalFromData(_ data: Any, apiName : String, modelName:String, onSuccess success: @escaping (_ JSON: Any) -> Void, onFailure failure: @escaping (_ error: Error?, _ reason: String) -> Void) {
//
//        if data is NSDictionary{
//
//            let dict = data as! NSDictionary
//            if let JSONData = try?  JSONSerialization.data(
//                withJSONObject: dict,
//                options: .prettyPrinted
//            )
//            {
//                do {
//                    if apiName == API.login || apiName == API.otpMatch || apiName == API.companyRegister || apiName == API.companyprofileupdate || apiName == API.userRegister || apiName == API.userprofile {
//
//                        if modelName == "Profile"{
//                            let modalData = try Profile(data: JSONData)
//                            success(modalData)
//                        }
//                    }
//                    else if apiName == API.forgetpassword{
//
//                        if modelName == "ForgotPasswordResponse"{
//                            let modalData = try ForgotPasswordResponse(data: JSONData)
//                            success(modalData)
//                        }
//                    }
//                    else{
//                        print("\(apiName) not matched")
//                    }
//
//                }catch let error as NSError {
//                    // error
//                    print("error \(error)")
//                    failure(error, apiName)
//                }
//            }
//
//        }
//        else if data is NSArray{
//
//            let array = data as! NSArray
//            var modalArray = [Any]()
//            for item in array{
//
//                let vac = item as! NSDictionary
//
//                if let JSONData = try?  JSONSerialization.data(
//                    withJSONObject: vac,
//                    options: .prettyPrinted
//                )
//                {
//                    do {
//                        if (apiName == API.citylist) || (apiName == API.countrylist) {
//                            if modelName == "Item"{
//                                let modalData = try Item(data: JSONData)
//                                modalArray.append(modalData)
//                            }
//                        }
//                        else if (apiName == API.broadcastlistcompany) || (apiName == API.broadcastlist) || (apiName == API.completepost){
//                            if modelName == "Broadcast"{
//                                let modalData = try Broadcast(data: JSONData)
//                                modalArray.append(modalData)
//                            }
//                        }
//                        else if (apiName == API.area){
//                            if modelName == "Area"{
//                                let modalData = try Area(data: JSONData)
//                                modalArray.append(modalData)
//                            }
//                        }
//                        else if (apiName == API.offerjobsusers){
//                            if modelName == "ProjectOffer"{
//                                let modalData = try ProjectOffer(data: JSONData)
//                                modalArray.append(modalData)
//                            }
//                        }
//                        else if (apiName == API.notificationlist){
//                            if modelName == "AppNotification"{
//                                let modalData = try AppNotification(data: JSONData)
//                                modalArray.append(modalData)
//                            }
//                        }
//                        else if (apiName == API.interestlist){
//                            if modelName == "Intrest"{
//                                let modalData = try Intrest(data: JSONData)
//                                modalArray.append(modalData)
//                            }
//                        }
//                        else if (apiName == API.age){
//                            if modelName == "Age"{
//                                let modalData = try Age(data: JSONData)
//                                modalArray.append(modalData)
//                            }
//                        }
//                        else if (apiName == API.viewteam){
//                            if modelName == "Team"{
//                                let modalData = try Team(data: JSONData)
//                                modalArray.append(modalData)
//                            }
//                        }
//                        else if (apiName == API.joblistcompany){
//                            if modelName == "CompanyJob"{
//                                let modalData = try CompanyJob(data: JSONData)
//                                modalArray.append(modalData)
//                            }
//                        }
//
//                        else{
//                            print("\(apiName) not matched")
//                        }
//
//                    }catch let error as NSError {
//                        // error
//                        print("error \(error)")
//                        failure(error, apiName)
//                    }
//                }
//
//            }
//
//            if !modalArray.isEmpty{
//                success(modalArray)
//            }
//        }
//    }
//
//    static func APICallWithMultipleImagesUpload(imagesData:[ImageData], postName:String, method: HTTPMethod, parameters:[String:Any], controller:UIViewController, loaderMessage:String?, onSuccess success: @escaping (_ JSON: Any) -> Void, onFailure failure: @escaping (_ reason: String, _ statusCode:Int) -> Void){
//
//        if let message = loaderMessage{
//            Util.showHud(view: controller.view, text:
//                            "Please wait...".localized(), detail: message)
//        }else{
//            Util.showHud(view: controller.view)
//        }
//
//        if !NetworkUtils.isNetworkReachable(){
//            failure("Please check your internet connection".localized(), 0)
//            Util.hideHud(view: controller.view)
//            controller.presentAlert(title: "Alert".localized(), message: "Please check your internet connection".localized())
//        }
//
//        let authorizationToken = UserDefaults.standard.value(forKey: "authorizationToken") as? String ?? ""
//
//        //For file upload make send content-type  multipart
//        let headers = HTTPHeaders([ "content-type": "multipart/form-data",
//                                    "Authorization":"Bearer \(authorizationToken)"])
//        print("url \(postName)")
//        print("param \(parameters)")
//        print("headers \(headers)")
//
//        AF.upload(multipartFormData: { multiPart in
//
//            for (key, value) in parameters {
//                if let temp = value as? String {
//                    multiPart.append(temp.data(using: .utf8)!, withName: key)
//                }
//                if let temp = value as? Int {
//                    multiPart.append("\(temp)".data(using: .utf8)!, withName: key)
//                }
//                if let temp = value as? NSArray {
//                    temp.forEach({ element in
//                        let keyObj = key + "[]"
//                        if let string = element as? String {
//                            multiPart.append(string.data(using: .utf8)!, withName: keyObj)
//                        } else
//                        if let num = element as? Int {
//                            let value = "\(num)"
//                            multiPart.append(value.data(using: .utf8)!, withName: keyObj)
//                        }
//                    })
//
//                }
//                if let dict = value as? NSDictionary{
//
//                    var workingHoursJson = String()
//
//                    if let theJSONData = try? JSONSerialization.data(
//                        withJSONObject: dict,
//                        options: []) {
//                        multiPart.append(theJSONData, withName: "working_hours")
//                        workingHoursJson = String(data: theJSONData,
//                                                  encoding: .ascii)!
//                        print("JSON string = \(workingHoursJson)")
//                    }
//                }
//            }
//
//            for imgData in imagesData{
//
//                let data = imgData.data
//                let key = imgData.key
//
//                if key == "user_file[]"{
//
//                    for item in imgData.mediaItems{
//
//                        print("skg appending media item ")
//                        if item.type == MediaType.photo{
//
//                            if let newData = item.photo?.image.jpegData(compressionQuality: 0.7){
//                                multiPart.append(newData, withName: key, fileName: "\(key).jpg", mimeType: "image/jpeg")
//                            }
//                        }
//                        else if item.type == MediaType.video{
//                            // video sending pending
//                            if let videoUrl = item.video?.url{
//                                guard let videodata = try? Data(contentsOf: videoUrl) else {
//                                    return
//                                }
//                                multiPart.append(videodata, withName: key, fileName: "video.mp4", mimeType: "video/mp4")
//                            }
//
//                        }
//                    }
//                }
//                else{
//
//                    multiPart.append(data, withName: key, fileName: "\(key).jpg", mimeType: "image/jpeg")
//                }
//            }
//
//        }, to: postName, method: method, headers: headers)
//        .uploadProgress(queue: .main, closure: { progress in
//            //Current upload progress of file
//            print("Upload Progress: \(progress.fractionCompleted)")
//
//            if Int(progress.fractionCompleted) == 1{
//                print("Image upload success")
//            }
//        })
//        .responseJSON(completionHandler: { response in
//            //Do what ever you want to do with response
//            Util.hideHud(view: controller.view)
//            print("image upload debug \(parameters)\n response \(response)")
//            switch(response.result) {
//
//            case .success(_):
//                if let data = response.value
//                {
//                    print(response.value as Any)
//                    print(data)
//                    let statusCode = response.response?.statusCode ?? 0
//                    if statusCode == 401{
//                        //Handel session expired work
//                    }else{
//                        DispatchQueue.main.async {
//                            success(data)
//                        }
//                    }
//                }
//                break
//            case .failure(_):
//                if let data = response.error
//                {
//                    print(response.value as Any)
//                    print(data)
//                    let statusCode = response.response?.statusCode ?? 0
//                    failure("serverError", statusCode)
//                }
//                break
//            }
//
//        })
//        .cURLDescription { (description) in
//            print("curl description \(description)")
//        }
//
//
//    }
//
//    static func APICallWithProfileImageUpload(imageData:Data, postName:String, method: HTTPMethod, parameters:[String:Any], controller:UIViewController, loaderMessage:String?, onSuccess success: @escaping (_ JSON: Any) -> Void, onFailure failure: @escaping (_ reason: String, _ statusCode:Int) -> Void){
//
//        if let message = loaderMessage{
//            Util.showHud(view: controller.view, text: "Please wait...".localized(), detail: message)
//        }else{
//            Util.showHud(view: controller.view)
//        }
//
//        if !NetworkUtils.isNetworkReachable(){
//            failure("Please check your internet connection".localized(), 0)
//            Util.hideHud(view: controller.view)
//            controller.presentAlert(title: "Alert".localized(), message: "Please check your internet connection".localized())
//        }
//
//        let authorizationToken = UserDefaults.standard.value(forKey: "authorizationToken") as? String ?? ""
//
//        //For file upload make send content-type  multipart
//        let headers = HTTPHeaders([ "content-type": "multipart/form-data",
//                                    "Authorization":"Bearer \(authorizationToken)"])
//        print("url \(postName)")
//        print("param \(parameters)")
//        print("headers \(headers)")
//
//        AF.upload(multipartFormData: { multiPart in
//
//            for (key, value) in parameters {
//                if let temp = value as? String {
//                    multiPart.append(temp.data(using: .utf8)!, withName: key)
//                }
//                if let temp = value as? Int {
//                    multiPart.append("\(temp)".data(using: .utf8)!, withName: key)
//                }
//                if let temp = value as? NSArray {
//                    temp.forEach({ element in
//                        let keyObj = key + "[]"
//                        if let string = element as? String {
//                            multiPart.append(string.data(using: .utf8)!, withName: keyObj)
//                        } else
//                        if let num = element as? Int {
//                            let value = "\(num)"
//                            multiPart.append(value.data(using: .utf8)!, withName: keyObj)
//                        }
//                    })
//
//                }
//                if let dict = value as? NSDictionary{
//
//                    var workingHoursJson = String()
//
//                    if let theJSONData = try? JSONSerialization.data(
//                        withJSONObject: dict,
//                        options: []) {
//                        multiPart.append(theJSONData, withName: "working_hours")
//                        workingHoursJson = String(data: theJSONData,
//                                                  encoding: .ascii)!
//                        print("JSON string = \(workingHoursJson)")
//                    }
//                }
//            }
//
//            multiPart.append(imageData, withName: "image", fileName: "profileImage.jpg", mimeType: "image/jpeg")
//
//        }, to: postName, method: method, headers: headers)
//        .uploadProgress(queue: .main, closure: { progress in
//            //Current upload progress of file
//            print("Upload Progress: \(progress.fractionCompleted)")
//
//            if Int(progress.fractionCompleted) == 1{
//                print("Image upload success")
//            }
//        })
//        .responseJSON(completionHandler: { response in
//            //Do what ever you want to do with response
//            Util.hideHud(view: controller.view)
//            print("image upload debug \(parameters)\n response \(response)")
//            switch(response.result) {
//
//            case .success(_):
//                if let data = response.value
//                {
//                    print(response.value as Any)
//                    print(data)
//                    let statusCode = response.response?.statusCode ?? 0
//                    if statusCode == 401{
//                        //Handel session expired work
//                    }else{
//                        DispatchQueue.main.async {
//                            success(data)
//                        }
//                    }
//                }
//                break
//            case .failure(_):
//                if let data = response.error
//                {
//                    print(response.value as Any)
//                    print(data)
//                    let statusCode = response.response?.statusCode ?? 0
//                    failure("serverError", statusCode)
//                }
//                break
//            }
//
//        })
//        .cURLDescription { (description) in
//            print("curl description \(description)")
//        }
//
//
//    }
//
//
//    static func APICall(postName:String, method: HTTPMethod, parameters:[String:Any], controller:UIViewController, onSuccess success: @escaping (_ JSON: Any) -> Void, onFailure failure: @escaping (_ reason: String, _ statusCode:Int) -> Void) {
//
//        Util.showHud(view: controller.view)
//        if !NetworkUtils.isNetworkReachable(){
//            failure("No internet connection available", 0)
//            controller.presentAlert(title: "Alert", message: "No internet connection available")
//            Util.hideHud(view: controller.view)
//        }
//
//        let authorizationToken = UserDefaults.standard.value(forKey: "authorizationToken") as? String ?? ""
//        let headers = HTTPHeaders(["Authorization":"Bearer \(authorizationToken)"])
//
//        print("param \(parameters) \n header \(headers)")
//
//        guard let url = URL(string: postName)else{
//            Util.hideHud(view: controller.view)
//            return
//        }
//
//        if method == .post{
//
//            AF.request(url, method: method, parameters: parameters , encoding: URLEncoding.httpBody,headers: headers).responseJSON {
//                response in
//                Util.hideHud(view: controller.view)
//                debugPrint(response)
//                switch(response.result) {
//
//                case .success(_):
//                    if let data = response.value
//                    {
//                        DispatchQueue.main.async {
//                            success(data)
//                        }
//                    }
//                    break
//                case .failure(_):
//                    if response.error != nil
//                    {
//                        let statusCode = response.response?.statusCode ?? 0
//                        failure("serverError", statusCode)
//                    }
//                    break
//                }
//
//            }
//        }
//        else if method == .get{
//
//            AF.request(url, encoding:  URLEncoding.default, headers: headers).responseJSON { response in
//                Util.hideHud(view: controller.view)
//                debugPrint(response)
//
//                switch(response.result) {
//
//                case .success(_):
//                    if let data = response.value
//                    {
//                        DispatchQueue.main.async {
//                            success(data)
//                        }
//                    }
//                    break
//                case .failure(_):
//                    if response.error != nil
//                    {
//                        let statusCode = response.response?.statusCode ?? 0
//                        failure("serverError", statusCode)
//                    }
//                    break
//                }
//            }
//        }
//    }
//}


class NetworkUtils{
    
    static func isNetworkReachable() -> Bool {
        
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
        if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
            return false
        }
        
        // Working for Cellular and WIFI
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        let ret = (isReachable && !needsConnection)
        
        return ret
    }
}

