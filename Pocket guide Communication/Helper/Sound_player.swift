//
//  Sound_player.swift
//  Pocket guide Communication
//
//  Created by Deepak on 11/11/21.
//

import UIKit

import AVFoundation

var player: AVAudioPlayer?

func playSound(fileName : String,file_Extension  :String) {
    //Pain.wav
    guard let url = Bundle.main.url(forResource: fileName, withExtension: file_Extension) else {
        
        print("need Audio : \(fileName)")
        let utterance = AVSpeechUtterance(string: fileName)
        utterance.voice = AVSpeechSynthesisVoice(language: "en-GB")
        utterance.rate = 0.5
//        utterance.voice =
        let synthesizer = AVSpeechSynthesizer()
        synthesizer.speak(utterance)
        
        return }
    do {
        try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default)
        try AVAudioSession.sharedInstance().setActive(true)

        /* The following line is required for the player to work on iOS 11. Change the file type accordingly*/
        player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)

        /* iOS 10 and earlier require the following line:
        player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileTypeMPEGLayer3) */

        guard let player = player else { return }

        player.play()

    } catch let error {
        print(error.localizedDescription)
    }
}
