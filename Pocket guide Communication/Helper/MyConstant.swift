//
//  MyConstant.swift
//  Job Portal Employee
//
//  Created by Deepak Kumar on 07/10/21.
//

import Foundation
import UIKit
import SwiftUI

var DEFAULTS = UserDefaults.standard
var isIpad = UIDevice.current.userInterfaceIdiom == .pad ? true : false

var noOfCellsInRow = 4
var SELECTED_LANG = "en"
var screenWidth = UIScreen.main.bounds.width
var screenHeight = UIScreen.main.bounds.height
var IS_MALE = Bool()
var SUBSCRIPTION_STATUS = false
//var iconTitleSize = isIpad ? 30 : noOfCellsInRow == 6 ? 14 : 19
var iconTitleFont: UIFont {
    return UIFont.init(name: "Navigo-Regular", size: CGFloat(isIpad ? 30 : noOfCellsInRow == 6 ? 14 : 19))!
}
//var TabbarObj : TB_Main!

var APPLE_NAME = String()
var APPLE_EMAIL = String()
var APPLE_ID = String()


enum applicationType: String {
    case user = "User"
    case compnay = "compnay"
}
var Application_Type = applicationType.compnay

// : UIStoryboard
var SB_Fill_Details = UIStoryboard(name: "Fill_Details", bundle: nil)
var SB_HomePage = UIStoryboard(name: "HomePage", bundle: nil)
var SB_Tab = UIStoryboard(name: "Tab_bar", bundle: nil)
var SB_Side_Menu = UIStoryboard(name: "Side_Menu", bundle: nil)
var SB_Contacts = UIStoryboard(name: "Contacts", bundle: nil)
var SB_Healthcare_Providers_Aphasia = UIStoryboard(name: "Healthcare_Providers_Aphasia", bundle: nil)
var SB_Tips = UIStoryboard(name: "Tips", bundle: nil)
var SB_Yes_no_clarify = UIStoryboard(name: "Yes_no_clarify", bundle: nil)
var SB_Concerns = UIStoryboard(name: "Concerns", bundle: nil)
var SB_Concerns_Details = UIStoryboard(name: "Concerns_Details", bundle: nil)
var SB_HumanBody = UIStoryboard(name: "HumanBody", bundle: nil)
var SB_Calender = UIStoryboard(name: "Calender", bundle: nil)
var SB_White_bord = UIStoryboard(name: "White_bord", bundle: nil)
var SB_SaveBoard = UIStoryboard(name: "SaveBoard", bundle: nil)
var SB_How_often = UIStoryboard(name: "How_often", bundle: nil)
var SB_Whiteboard_List = UIStoryboard(name: "Whiteboard_List", bundle: nil)
var SB_Problem_Confirmation = UIStoryboard(name: "Problem_Confirmation", bundle: nil)
var SB_Settings = UIStoryboard(name: "Settings", bundle: nil)
var SB_Summery = UIStoryboard(name: "SummeryVC", bundle: nil)
var SB_JustChoose = UIStoryboard(name: "JustChoose", bundle: nil)
var SB_Main = UIStoryboard(name: "Main", bundle: nil)

func setItemsPerRowInUserDefault(itemCount: Int){
    DEFAULTS.setValue(itemCount, forKey: "collectionViewItemCount")
    let count = DEFAULTS.value(forKey: "collectionViewItemCount") as? Int ?? 4
    noOfCellsInRow = count
}
func setGenderInUserDefault(isMale: Bool){
    DEFAULTS.setValue(isMale, forKey: "userGender")
    let gender = DEFAULTS.value(forKey: "userGender") as? Bool ?? true
    IS_MALE = gender
}
func setlanguageInUserDefault(isEnglish: String){
    DEFAULTS.setValue(isEnglish, forKey: "userLanguage")
    let language = DEFAULTS.value(forKey: "userLanguage") as? String ?? "en"
    SELECTED_LANG = language
}
