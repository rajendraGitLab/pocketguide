//
//  API.swift
//  Yaas
//
//  Created by Deepak Kumar on 23/12/20.
//

import Foundation

let BASE_PATH = "http://sample.jploftsolutions.in/yaash/api"

struct API {
    
    static let login = "\(BASE_PATH)/login"// Used for both user and company
    static let userRegister = "\(BASE_PATH)/register"
    static let companyRegister = "\(BASE_PATH)/companyregister"
    static let otpMatch = "\(BASE_PATH)/otpMatch"
    static let resendOtp = "\(BASE_PATH)/resendOtp"
    static let forgetpassword = "\(BASE_PATH)/forgetpassword"
    
    static let changepassword = "\(BASE_PATH)/changepassword"
    static let area = "\(BASE_PATH)/area"
    static let age = "\(BASE_PATH)/age"
    static let broadcast = "\(BASE_PATH)/broadcast"
    static let copybroadcast = "\(BASE_PATH)/copybroadcast"
    static let broadcastlist = "\(BASE_PATH)/broadcastlist"
    static let companyprofile = "\(BASE_PATH)/companyprofile"
    static let like = "\(BASE_PATH)/like"
    static let interestlist = "\(BASE_PATH)/interestlist"
    static let userprofile = "\(BASE_PATH)/userprofile"
    static let companyprofileupdate = "\(BASE_PATH)/companyprofileupdate"
    static let userinterest = "\(BASE_PATH)/userinterest"
    static let removeuserImage = "\(BASE_PATH)/removeuserImage"
    static let broadcastlistcompany = "\(BASE_PATH)/broadcastlistcompany"
    static let closebroadcast = "\(BASE_PATH)/closebroadcast"
    static let offerjob = "\(BASE_PATH)/offerjob"
    static let offerjobsusers = "\(BASE_PATH)/offerjobsusers"
    static let acceptoffer = "\(BASE_PATH)/acceptoffer"
    static let viewteam = "\(BASE_PATH)/viewteam"
    static let offerdetail = "\(BASE_PATH)/offerdetail"
    static let specification = "\(BASE_PATH)/specification"
    static let joblistcompany = "\(BASE_PATH)/joblistcompany"
    static let notificationlist = "\(BASE_PATH)/notificationlist"
    static let citylist = "\(BASE_PATH)/citylist"
    static let countrylist = "\(BASE_PATH)/countrylist"
    static let completepost = "\(BASE_PATH)/completepost"
    static let getnotificationcount = "\(BASE_PATH)/getnotificationcount"
    
}
