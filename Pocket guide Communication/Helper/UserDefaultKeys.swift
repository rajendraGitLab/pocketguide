//
//  UserDefaultKeys.swift
//  Yaas
//
//  Created by Deepak Kumar on 23/12/20.
//

import Foundation

struct UserDefaultKeys {
    static let kLanguage = "kLanguage"
    static let kisUserLogin = "kisUserLogin"
    static let kProfile = "kProfile"
    static let kFCMPushToken = "kFCMPushToken"
}


extension Collection {
    
    func chunked(by distance: Int) -> [[Element]] {
        precondition(distance > 0, "distance must be greater than 0") // prevents infinite loop

        var index = startIndex
        let iterator: AnyIterator<Array<Element>> = AnyIterator({
            let newIndex = self.index(index, offsetBy: distance, limitedBy: self.endIndex) ?? self.endIndex
            defer { index = newIndex }
            let range = index ..< newIndex
            return index != self.endIndex ? Array(self[range]) : nil
        })
        
        return Array(iterator)
    }
    
}
