//
//  SummeryModel.swift
//  Pocket guide Communication
//
//  Created by Admin on 16/12/22.
//

import Foundation
import UIKit

class SummeryInfo {
    
    static let shared: SummeryInfo = SummeryInfo()
    var summeryModel = [SummeryModel]()
    
    private init(){}
    
    func appendSummeryInfo(imgName: UIImage,imgTitle: String,timeStamp: TimeInterval){
        let arr = summeryModel.filter({ $0.imgTitle == imgTitle })
        if arr.count >= 1{
            print("Details already exists")
        }else{
            summeryModel.append(SummeryModel(imgName: imgName,imgTitle: imgTitle.localized(),timeStamp: timeStamp))
        }
    }
    
    func getAll() -> [SummeryModel]{
        return summeryModel
    }
    func removeAll(){
        summeryModel.removeAll()
    }
    func removeWithTitle(imgtitle: String){
        summeryModel = summeryModel.filter({ $0.imgTitle != imgtitle })
    }
}

struct SummeryModel{
    var imgName = UIImage()
    var imgTitle = String()
    var timeStamp = TimeInterval()
}
