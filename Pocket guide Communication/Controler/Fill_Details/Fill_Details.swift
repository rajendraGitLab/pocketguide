//
//  Fill_Details.swift
//  Pocket guide Communication
//
//  Created by Deepak on 28/10/21.
//

import UIKit

var userDefault = UserDefaults.standard

class Fill_Details: UIViewController {

    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtRooltype: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtName.text = APPLE_NAME
    }
    
    override func viewDidAppear(_ animated: Bool) {
      
        comeFromHowRuVC = false
        if !isNew {
            
                if let getUserData = userDefault.object(forKey: "user_data") as? NSDictionary {
                    
                    txtName.text = getUserData["name"] as? String ?? ""
                    txtRooltype.text = getUserData["rool_type"] as? String ?? ""
                    
                }
                
            }
            
    }
    //MARK: - Action
    
    @IBAction func homebtnTapped(_ sender: Any) {
//        if let vc = SB_Tab.instantiateViewController(withIdentifier: "Tab_bar") as? Tab_bar {
//
//            let nav = UINavigationController(rootViewController: vc)
//            nav.interactivePopGestureRecognizer?.delegate = nil
//            nav.setNavigationBarHidden(true, animated: false)
//            UIApplication.shared.keyWindow?.rootViewController = nav
//            UIApplication.shared.keyWindow?.makeKeyAndVisible()
//        }
        Util.showSideMenu(navC: self)
    }
    
    
    @IBAction func btnNextAction(_ sender: UIButton) {
        
        if let vc = SB_Tab.instantiateViewController(withIdentifier: "Home_page") as? Home_page {
            vc.comeFromIntroduction = true
            isNew = false
            userDefault.setValue(isNew, forKey: "isNew")
            
            
            let saveData = ["name" : txtName.text ?? "",
                            "rool_type" : txtRooltype.text ?? ""
            ] as [String : Any]
            userDefault.setValue(saveData, forKey: "user_data")
            let nav = UINavigationController(rootViewController: vc)
            nav.interactivePopGestureRecognizer?.delegate = nil
            nav.setNavigationBarHidden(true, animated: false)
            UIApplication.shared.keyWindow?.rootViewController = nav
            UIApplication.shared.keyWindow?.makeKeyAndVisible()
        }
        
    }
    
}
