//
//  GuideTabView.swift
//  Pocket guide Communication
//
//  Created by Admin on 31/08/22.
//

import Foundation
import UIKit

class GuideTabView: UIView{
    
    @IBOutlet weak var dashView1: DashedView!
    @IBOutlet weak var dashView2: DashedView!
    @IBOutlet weak var dashView3: DashedView!
    
    @IBOutlet weak var stackViewFromX: NSLayoutConstraint!
    @IBOutlet weak var stackViewFromY: NSLayoutConstraint!
    @IBOutlet weak var stackViewHeight: NSLayoutConstraint!
    
    var tabBarHeight = Int()
    
    class func intitiateFromNib(tabHeight: Int) -> GuideTabView {
            let View1 = UINib.init(nibName: "GuideTabView", bundle: nil).instantiate(withOwner: self, options: nil).first as! GuideTabView
        View1.frame = CGRect(x: 0, y: CGFloat(Int(screenHeight - CGFloat((tabHeight)))), width: screenWidth, height: CGFloat(tabHeight))
            return View1
        }
    override func awakeFromNib() {
        super.awakeFromNib()
        dashView1.layer.opacity = 0
        dashView2.layer.opacity = 0
        dashView3.layer.opacity = 0
        
        dashView1.dashWidth = 4
        dashView2.dashWidth = 4
        dashView3.dashWidth = 4
        
        stackViewFromX.constant = screenWidth/8
        stackViewFromY.constant = screenWidth/8
        NotificationCenter.default.addObserver(self, selector: #selector(showView(_:)), name: Notification.Name("showDash"), object: nil)
    }
    
    @objc func showView(_ sender: Notification){
        let dict = sender.userInfo as? NSDictionary
        let view = dict?["show"] as? String
        switch view {
        case "1":
            self.dashView1.layer.opacity = 1
            self.dashView2.layer.opacity = 0
            self.dashView3.layer.opacity = 0
        case "2":
            self.dashView1.layer.opacity = 0
            self.dashView2.layer.opacity = 1
            self.dashView3.layer.opacity = 0
        case "3":
            self.dashView1.layer.opacity = 0
            self.dashView2.layer.opacity = 0
            self.dashView3.layer.opacity = 1
        default:
            print("")
        }
    }
}
