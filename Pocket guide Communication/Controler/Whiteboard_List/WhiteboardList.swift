//
//  Whiteboard_List.swift
//  Pocket guide Communication
//
//  Created by Deepak on 09/11/21.
//

import UIKit
import Alamofire

class WhiteboardList: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var titleLbl: UILabel!
    
    @IBOutlet weak var viewHeader: UIView!
    
    var list = NSArray()
    var comeFromSummary = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLbl.text = comeFromSummary ? "Summary List".localized() : "Whiteboard List".localized()
        apiForGetData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    @IBAction func backbtnTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
}

extension WhiteboardList : UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if list.count == 0 {
            self.tableView.setEmptyView(title: "", message: "No Data")
            self.viewHeader.isHidden = true
        }else{
            tableView.restore()
            self.viewHeader.isHidden = false
        }
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Whiteboard_Cell", for: indexPath) as! Whiteboard_Cell
        
        if let index = list[indexPath.row] as? NSDictionary {
            cell.lblNo.text = "\(indexPath.row + 1)"
            cell.lblTitle.text = index["image_title"] as? String ?? ""
            
            let date = index["created_at"] as? String ?? ""
            
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            

            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "MM-dd-yyyy"
            
            cell.btnDeleteOnCell.tag = indexPath.row
            cell.btnDeleteOnCell.addTarget(self, action: #selector(btnDeleteActionOnCell(_:)), for: .touchUpInside)
            
            if let date = dateFormatterGet.date(from: date) {
                cell.lblDate.text = dateFormatterPrint.string(from: date)
            } else {
               print("There was an error decoding the string")
            }
        }
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return isIpad ? 120 : 58
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let index = list[indexPath.row] as? NSDictionary {
            
            if let vc = SB_Whiteboard_List.instantiateViewController(withIdentifier: "WhiteBordDetails") as? WhiteBordDetails {
                vc.getImage = index.object(forKey: "image_url") as? String
                vc.getText = index.object(forKey: "image_title") as? String
                self.navigationController?.pushViewController(vc, animated: false)
            }
        }
    }
    
    @objc func btnDeleteActionOnCell(_ sender : UIButton){
        
        let alert = UIAlertController(title: "Alert", message: "Are you sure you want to delete?", preferredStyle: UIAlertController.Style.alert)

                // add the actions (buttons)
                alert.addAction(UIAlertAction(title: "Okay", style: UIAlertAction.Style.default, handler: { [self] action in
                    switch action.style{
                        case .default:
                        print("default")
                        
                        print(sender.tag)
                        if let index = self.list[sender.tag] as? NSDictionary {
                            if let id = index.object(forKey: "id") as? Int {
                                apiForDeleteItemOnCell(id: id)
                            }
                        }
                        
                        case .cancel:
                        print("cancel")
                        case .destructive:
                        print("destructive")
                        
                    }
                }))
                alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
                // show the alert
                self.present(alert, animated: false, completion: nil)
        
        }
}

extension WhiteboardList {
    
    func apiForGetData(){
        
        if !NetworkUtils.isNetworkReachable() {
            Toastnew.show(message: "no internet connection", controller: self)
            return
        }
        
        let parm = ["user_id" : UIDevice.current.identifierForVendor!.uuidString,"type": comeFromSummary ? "summary" : "whiteboard"]
        
        Util.showHud(view: self.view)
        
        print("parm : \(parm)")

        AF.request(getList, method: .post, parameters: parm , encoding: URLEncoding.httpBody,headers: nil).responseJSON {
                        response in
                        Util.hideHud(view: self.view)
                        debugPrint(response)
                        switch(response.result) {
        
                        case .success(_):
                            if let data = response.value
                            {
                                DispatchQueue.main.async {
                                    //success(data)
                                    print(data)
                                    
                                    if let getDataresponce = data as? NSDictionary{
                                        if let getDataresponce = getDataresponce["data"] as? NSArray{
                                            self.list = getDataresponce
                                            DispatchQueue.main.async {
                                                self.tableView.reloadData()
                                            }
                                        }
                                        
                                    }else{
                                        self.list = []
                                        DispatchQueue.main.async {
                                            self.tableView.reloadData()
                                        }
                                    }
                                    
                                    
                                }
                            }
                            break
                        case .failure(_):
                            if response.error != nil
                            {
                                let statusCode = response.response?.statusCode ?? 0
                                //failure("serverError", statusCode)
                            }
                            break
                        }
        
                    }
    }
    
    func apiForDeleteItemOnCell(id : Int){
        
        if !NetworkUtils.isNetworkReachable() {
            
            Toastnew.show(message: "no internet connection", controller: self)

            return
        }
        var parm = [String:Any]()
        parm = ["type": comeFromSummary ? "summary" : "whiteboard","edit_image_id" : id,
//                     "api" : "deleteFile",
                    "user_id" : UIDevice.current.identifierForVendor!.uuidString
        ]
        
        Util.showHud(view: self.view)
        
        print("parm : \(parm)")
        print("baseUrl : \(deleteList)")

        AF.request(deleteList, method: .post, parameters: parm , encoding: URLEncoding.httpBody,headers: nil).responseJSON {
                        response in
                        Util.hideHud(view: self.view)
                        debugPrint(response)
                        switch(response.result) {
        
                        case .success(_):
                            if let data = response.value
                            {
                                DispatchQueue.main.async {
                                    //success(data)
                                    print(data)
                                    self.apiForGetData()
                                }
                            }
                            break
                        case .failure(_):
                            if response.error != nil
                            {
                                
                                Toastnew.show(message: response.error?.localizedDescription ?? "", controller: self)

                                let statusCode = response.response?.statusCode ?? 0
                                //failure("serverError", statusCode)
                            }
                            break
                        }
        
                    }
    }
}
