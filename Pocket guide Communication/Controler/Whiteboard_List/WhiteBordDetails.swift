//
//  WhiteBordDetails.swift
//  Pocket guide Communication
//
//  Created by Deepak on 11/11/21.
//

import UIKit

class WhiteBordDetails: UIViewController {
    
    var getImage : String?
    var getText : String?
    var comeFromSummary = false

    @IBOutlet weak var imageUplode: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        lblTitle.text = getText
        if let getsUrl = getImage {
            if let url = URL(string: "https://stage.tasksplan.com/pocket_guide/uploads/" + getsUrl){
                downloadImage(from: url)
            }
        }
    }
    
    @IBAction func btnNextAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }

    func downloadImage(from url: URL) {
        print("Download Started")
        if !NetworkUtils.isNetworkReachable() {
            Toastnew.show(message: "no internet connection", controller: self)
            return
        }
        getData(from: url) { data, response, error in
            guard let data = data, error == nil else { return }
            print(response?.suggestedFilename ?? url.lastPathComponent)
            print("Download Finished")
            DispatchQueue.main.async() { [weak self] in
                self?.imageUplode.image = UIImage(data: data)
            }
        }
    }
   
    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }

}
