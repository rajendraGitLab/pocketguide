//
//  AllimagesVC.swift
//  Pocket guide Communication
//
//  Created by Admin on 05/09/22.
//

import UIKit

class allImgCell: UICollectionViewCell{
    @IBOutlet weak var clsImg: UIImageView!
}
protocol selectedImage {
    func selected(imgStr: [String])
}


class AllimagesVC: UIViewController,UIScrollViewDelegate {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var zoomView : ZoomableView!
    @IBOutlet weak var sclView : UIScrollView!
    
    var delegate: selectedImage?
    var selectedArr = [Int]()
    var currentCellFrame = Int()
    override func viewDidLoad() {
        super.viewDidLoad()
        sclView.minimumZoomScale = 1
        sclView.maximumZoomScale = 10.0
        sclView.delegate = self
//        zoomView.sourceView = collectionView
//        zoomView.isZoomable = true
    }
    @IBAction func backBtn(_ sender: UIButton){
        self.dismiss(animated: true)
    }
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
            return zoomView
        }
    @IBAction func doneBtn(_ sender: UIButton){
        self.dismiss(animated: true)
        let result = selectedArr.compactMap { (allConcernImages.count > $0) ? allConcernImages[$0] : nil}
        self.delegate?.selected(imgStr: result)
    }
}
extension AllimagesVC : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return allConcernImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
           let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "allImgCell", for: indexPath) as! allImgCell
        if selectedArr.contains(indexPath.row){
            cell.clsImg.borderWidth = 4
        }else{
            cell.clsImg.borderWidth = 0.5
        }
        cell.clsImg.image = UIImage(named: allConcernImages[indexPath.row])
            return cell
        }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    
        let collectionHeight = collectionView.bounds.height
        
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        
        let totalSpace = flowLayout.sectionInset.left
        + flowLayout.sectionInset.right
        + ( (3 == 2 ? (collectionView.frame.width/3) : flowLayout.minimumLineSpacing) * CGFloat(3 - 1))
        
        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(3))
         currentCellFrame = 3 == 1 ? Int(collectionHeight - 20) : size
        
        return CGSize(width: currentCellFrame, height: currentCellFrame)
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if 3 <= 2 {
            let totalCellWidth = Int(currentCellFrame) * 3
            let totalSpacingWidth = Int(10) * (3 - 1)

            let leftInset = (collectionView.bounds.width - CGFloat(totalCellWidth + totalSpacingWidth)) / 2
            let rightInset = leftInset

            return UIEdgeInsets(top: 0, left: rightInset, bottom: 0, right: rightInset)
        }else{
            return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if selectedArr.contains(indexPath.row){
            selectedArr = selectedArr.filter { $0 != indexPath.row }
            print("removed")
        }else{
            selectedArr.append(indexPath.row)
            print("appedned")
        }
        collectionView.reloadData()
      }
    }
    
