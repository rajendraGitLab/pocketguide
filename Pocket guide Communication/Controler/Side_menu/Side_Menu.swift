//
//  Menu.swift
//  Job Portal Employee
//
//  Created by Deepak Kumar on 07/10/21.
//

import UIKit
import SwiftUI

var comefromSideMenu = false
class Side_Menu: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
//    Tips
//    Introduction
//    How are you?
//    Concerns
//    White Board
//    Yes/No/? Board
//    How Often?
//    Calendar
//    Bladder
//    Bowel
//    Breathing /Coughing
//    Fatigue
//    Nausea
//    Pain description
//    Pain scale
//    Pain location
//    Contact Us

    var item = ["Settings".localized(),"Introduction".localized(),"How are You?".localized(),"Concerns".localized(),"White Board".localized(),"Yes /No /? Board".localized()
                ,"Needs Board".localized()
                ,"Topic Board".localized(),
                "How Often?".localized(),"Calendar".localized(),"Bladder".localized(),"Bowels".localized(),"Breathing / Coughing".localized()
                
                ,"Emotions / Feelings".localized()
                ,"Fatigue".localized(),"Medication".localized(),"Nausea".localized(),"Pain Description".localized(),"Pain Scale".localized(),"Pain Location".localized(),"Swallowing".localized()
                
                ,"Trach".localized()
                ,"Vision Problems".localized()
                ,"Contact Us".localized(),"Tips".localized()
                ,"Saved Whiteboard".localized()
                ,"Saved Summary".localized()
                ,"Patient Education".localized()
//                ,"Subscription".localized()
                ,"About Us".localized(),
//                "Log out".localized()
    
    ]
    
    var menuicon = ["settings","SIntro","health_good","house","yes-no","idea"
                    ,"crossed",
                    "conversation"
                    ,"All Day","SCalender","concerns_urinationSide","concerns_bowels","concerns_BreathingSide"
                    ,"depressedN"
                    ,"concerns_fatigueSide","With Medication","concerns_NauseaSide","SqueezingSide","pain4Side","concerns_painSide","concerns_swallowingSide",
                    "Trach",
                    "glasses",
                    "SContact","STips",
                    "yes-no",
                    "summary",
                    "education"
//                    ,"subscription"
                    ,"aboutUs",
//                    "logout"
    ]
    
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblUserCat: UILabel!
    @IBOutlet weak var imageUser: UIImageView!
    
    @IBOutlet weak var itemView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        itemView.layer.maskedCorners = [.layerMaxXMinYCorner]
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        comeFromHowRuVC = false
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
          return .lightContent
    }
    
    @IBAction func btnCloseMenuAction(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)

    }
}


extension Side_Menu  : UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return item.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell_SideMenu", for: indexPath) as! Cell_SideMenu
        cell.labelTitle.text = item[indexPath.row].localized()
        cell.imgViewIcon.image = UIImage(named: menuicon[indexPath.row])
        
        return cell

    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        if isIpad {
            return 75
        }
        return 60//UITableView.automaticDimension
    }


    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        SummeryDict.removeAll()
        SummeryInfo.shared.removeAll()
        comefromSideMenu = true
        let item = item[indexPath.row]
//
        if item == "Tips".localized(){
           // methodMakeRootTabbar(index: 1)
//            self.dismiss(animated: false, completion: nil)
//           // TabbarObj.selectedIndex = 1
//
//            if let vc = SB_Healthcare_Providers_Aphasia.instantiateViewController(withIdentifier: "Healthcare_Providers_Aphasia") as? Healthcare_Providers_Aphasia{
//                self.navigationController?.pushViewController(vc, animated: false)
//            }
            
            methodMakeRootTabbar(index: 1)
            
                        if let vc = (TabbarObj.viewControllers?[1] as! UINavigationController).viewControllers.first as? Concerns {
                if let tips_vc = SB_Healthcare_Providers_Aphasia.instantiateViewController(withIdentifier: "Healthcare_Providers_Aphasia") as? Healthcare_Providers_Aphasia{
                    vc.navigationController?.pushViewController(tips_vc, animated: false)
                }
                        }

//
        }else if item == "Introduction".localized(){
           
            if let vc = SB_Fill_Details.instantiateViewController(withIdentifier: "Fill_Details") as? Fill_Details {
                self.navigationController?.pushViewController(vc, animated: false)
            }
            
//            if let vc = SB_Healthcare_Providers_Aphasia.instantiateViewController(withIdentifier: "Healthcare_Providers_Aphasia") as? Healthcare_Providers_Aphasia{
//                self.navigationController?.pushViewController(vc, animated: false)
//            }
        }else if item == "Contact Us".localized(){
//            methodMakeRootTabbar(index: 1)
//            if let vc = SB_Contacts.instantiateViewController(withIdentifier: "Contacts") as? Contacts{
//                self.navigationController?.pushViewController(vc, animated: false)
//            }
            methodMakeRootTabbar(index: 1)
            
                        if let vc = (TabbarObj.viewControllers?[1] as! UINavigationController).viewControllers.first as? Concerns {
                if let ContactUs_vc = SB_Contacts.instantiateViewController(withIdentifier: "Contacts") as? Contacts{
                    vc.navigationController?.pushViewController(ContactUs_vc, animated: false)
                }
                        }
            
        }else if item == "Yes /No /? Board".localized(){
            methodMakeRootTabbar(index: 0)
            TabbarObj.selectedIndex = 0
//            if let vc = SB_Yes_no_clarify.instantiateViewController(withIdentifier: "Yes_no_clarify") as? Yes_no_clarify{
//                self.navigationController?.pushViewController(vc, animated: false)
//            }
        }else if item == "Concerns".localized(){
            methodMakeRootTabbar(index: 1)
//                self.dismiss(animated: true, completion: nil)
//                methodMakeRootTabbar(index: 0)
//                if let vc = (TabbarObj.viewControllers?[1] as! UINavigationController).viewControllers.first as? Concerns {
//                    vc.navigationController?.pushViewController(Concerns_vc, animated: false)
//                }

            
//            if let Concerns_vc = SB_Concerns.instantiateViewController(withIdentifier: "Concerns") as? Concerns{
//
//                self.dismiss(animated: true, completion: nil)
//                if let vc = (TabbarObj.viewControllers?[1] as! UINavigationController).viewControllers.first as? Concerns {
//                    vc.navigationController?.pushViewController(Concerns_vc, animated: false)
//                }
//            }
            
        }else if item == "How are You?".localized(){
            methodMakeRootTabbar(index: 1)
            
                        if let vc = (TabbarObj.viewControllers?[1] as! UINavigationController).viewControllers.first as? Concerns {
//
                if let HomeVC = SB_Tab.instantiateViewController(withIdentifier: "Home_page") as? Home_page{
                    HomeVC.comeFromIntroduction = false
                    vc.navigationController?.pushViewController(HomeVC, animated: false)
                }
                        }

////                vc.navigationController?.popToRootViewController(animated: true)
//            }
            
//            if let vc = (TabbarObj.viewControllers?[0] as! UINavigationController).viewControllers.first as? Home_page {
//                vc.navigationController?.popToRootViewController(animated: true)
//            }
            
//            TabbarObj.selectedIndex = 0
            
        }else if item == "White Board".localized(){
            methodMakeRootTabbar(index: 2)
            TabbarObj.selectedIndex = 2
            
        }else if item == "Calendar".localized(){
            methodMakeRootTabbar(index: 1)
            if let Concerns_vc = SB_Calender.instantiateViewController(withIdentifier: "Calender") as? Calender{
                
                if let vc = (TabbarObj.viewControllers?[1] as! UINavigationController).viewControllers.first as? Concerns {
                    Concerns_vc.titleVC = "Calendar".localized()
                    Concerns_vc.concerns_Type = .none
                    vc.navigationController?.pushViewController(Concerns_vc, animated: false)
                }
            }
        }else if item == "How Often?".localized(){
            methodMakeRootTabbar(index: 1)
            if let Concerns_vc = SB_How_often.instantiateViewController(withIdentifier: "How_often") as? How_often{
                
                if let vc = (TabbarObj.viewControllers?[1] as! UINavigationController).viewControllers.first as? Concerns {
                    Concerns_vc.concerns_Type = .none

                    vc.navigationController?.pushViewController(Concerns_vc, animated: false)
                }
                

            }
        }else if item == "Bowels".localized(){
            
            methodMakeRootTabbar(index: 1)
            if let Concerns_vc = SB_Concerns.instantiateViewController(withIdentifier: "Concerns") as? Concerns{

                if let vc = (TabbarObj.viewControllers?[1] as! UINavigationController).viewControllers.first as? Concerns {
                    
                    Concerns_vc.concerns_Type = .Bowels

                    vc.navigationController?.pushViewController(Concerns_vc, animated: false)
                }
                

            }
            
//            if let Concerns_vc = SB_How_often.instantiateViewController(withIdentifier: "How_often") as? How_often{
//
//                if let vc = (TabbarObj.viewControllers?[0] as! UINavigationController).viewControllers.first as? Home_page {
//
//                    Concerns_vc.titleVC = "How often do you have problems with your bowels?"
//                    Concerns_vc.concerns_Type = .Bowels
//
//                    vc.navigationController?.pushViewController(Concerns_vc, animated: false)
//                }
//
//                self.dismiss(animated: true, completion: nil)
//
//            }
//            if let Concerns_vc = SB_Calender.instantiateViewController(withIdentifier: "Calender") as? Calender{
//
//                if let vc = (TabbarObj.viewControllers?[0] as! UINavigationController).viewControllers.first as? Home_page {
//                    Concerns_vc.titleVC = "How often do you have problems with your bowels?"
//
//                    vc.navigationController?.pushViewController(Concerns_vc, animated: false)
//                }
//
//                self.dismiss(animated: true, completion: nil)
//
//            }
            
        }else if item == "Subscription".localized(){
            methodMakeRootTabbar(index: 1)
            if let vc = (TabbarObj.viewControllers?[1] as! UINavigationController).viewControllers.first as? Concerns {
                if let edu_vc = SB_Main.instantiateViewController(withIdentifier: "SubscriptionVC") as? SubscriptionVC{
                    vc.navigationController?.pushViewController(edu_vc, animated: false)
                }
            }
        }else if item == "Patient Education".localized(){
            
//            self.dismiss(animated: false, completion: nil)
//           // TabbarObj.selectedIndex = 1
//
//            if let vc = SB_Tips.instantiateViewController(withIdentifier: "AphasiaVC") as? AphasiaVC{
//                self.navigationController?.pushViewController(vc, animated: false)
//            }
            methodMakeRootTabbar(index: 1)
            
                        if let vc = (TabbarObj.viewControllers?[1] as! UINavigationController).viewControllers.first as? Concerns {
                if let edu_vc = SB_Tips.instantiateViewController(withIdentifier: "AphasiaVC") as? AphasiaVC{
                    vc.navigationController?.pushViewController(edu_vc, animated: false)
                }
            }
            
            }else if item == "Log out".localized(){
               
                let alert = UIAlertController(title: "Alert".localized(), message: "Are you sure you want to logout?".localized(), preferredStyle: UIAlertController.Style.alert)
                
                alert.addAction(UIAlertAction(title: "Okay".localized(), style: UIAlertAction.Style.default, handler: {_ in
                    userDefault.setValue(false, forKey: "isAppleLogin")
                    if let vc = SB_Main.instantiateViewController(withIdentifier: "SignInVC") as? SignInVC {
                        let nav = UINavigationController(rootViewController: vc)
                        nav.interactivePopGestureRecognizer?.delegate = nil
                        nav.setNavigationBarHidden(true, animated: false)
                        self.window?.rootViewController = nav
                        self.window?.makeKeyAndVisible()
                    }
                }))
                alert.addAction(UIAlertAction(title: "Cancel".localized(), style: UIAlertAction.Style.cancel, handler: nil))
                // show the alert
                self.present(alert, animated: true, completion: nil)
             }
        else if item == "Bladder".localized(){
            methodMakeRootTabbar(index: 1)
            if let Concerns_vc = SB_Concerns.instantiateViewController(withIdentifier: "Concerns") as? Concerns{

                if let vc = (TabbarObj.viewControllers?[1] as! UINavigationController).viewControllers.first as? Concerns {
                    
                    Concerns_vc.concerns_Type = .Urination

                    vc.navigationController?.pushViewController(Concerns_vc, animated: false)
                }
                

            }

//            if let Concerns_vc = SB_How_often.instantiateViewController(withIdentifier: "How_often") as? How_often{
//
//                if let vc = (TabbarObj.viewControllers?[1] as! UINavigationController).viewControllers.first as? Concerns {
//
//                    Concerns_vc.titleVC = "How often do you have problems with your bladder?"
//                    Concerns_vc.concerns_Type = .Bladder
//
//                    vc.navigationController?.pushViewController(Concerns_vc, animated: false)
//                }
//
//                self.dismiss(animated: true, completion: nil)
//
//            }
        }else if item == "Breathing / Coughing".localized(){
            methodMakeRootTabbar(index: 1)
            if let Concerns_vc = SB_Concerns.instantiateViewController(withIdentifier: "Concerns") as? Concerns{

                if let vc = (TabbarObj.viewControllers?[1] as! UINavigationController).viewControllers.first as? Concerns {
                    
                    Concerns_vc.concerns_Type = .Breathing_Problems
                    vc.navigationController?.pushViewController(Concerns_vc, animated: false)
                }
                

            }
        }else if item == "Fatigue".localized(){
            methodMakeRootTabbar(index: 1)
            if let Concerns_vc = SB_Concerns.instantiateViewController(withIdentifier: "Concerns") as? Concerns{

                if let vc = (TabbarObj.viewControllers?[1] as! UINavigationController).viewControllers.first as? Concerns {
                    
                    Concerns_vc.concerns_Type = .Fatigue

                    vc.navigationController?.pushViewController(Concerns_vc, animated: false)
                }
                

            }
        }else if item == "Medication".localized(){
            methodMakeRootTabbar(index: 1)
            if let Concerns_vc = SB_Concerns.instantiateViewController(withIdentifier: "Concerns") as? Concerns{

                if let vc = (TabbarObj.viewControllers?[1] as! UINavigationController).viewControllers.first as? Concerns {
                    
                    Concerns_vc.concerns_Type = .Medication

                    vc.navigationController?.pushViewController(Concerns_vc, animated: false)
                }
                
                

            }
        }else if item == "About Us".localized(){
//            self.dismiss(animated: false, completion: nil)
//           // TabbarObj.selectedIndex = 1
//
//            if let vc = SB_Tips.instantiateViewController(withIdentifier: "AboutUs") as? AboutUs{
//                self.navigationController?.pushViewController(vc, animated: false)
//            }
            
            methodMakeRootTabbar(index: 1)
            
                        if let vc = (TabbarObj.viewControllers?[1] as! UINavigationController).viewControllers.first as? Concerns {
                if let tips_vc = SB_Tips.instantiateViewController(withIdentifier: "AboutUs") as? AboutUs{
                    vc.navigationController?.pushViewController(tips_vc, animated: false)
                }
                        }
            
        }else if item == "Nausea".localized(){
            methodMakeRootTabbar(index: 1)
            if let Concerns_vc = SB_Concerns.instantiateViewController(withIdentifier: "Concerns") as? Concerns{

                if let vc = (TabbarObj.viewControllers?[1] as! UINavigationController).viewControllers.first as? Concerns {
                    
                    Concerns_vc.concerns_Type = .Nausea

                    vc.navigationController?.pushViewController(Concerns_vc, animated: false)
                }
                

            }
        }else if item == "Emotions / Feelings".localized(){
            methodMakeRootTabbar(index: 1)
            if let Concerns_vc = SB_Concerns.instantiateViewController(withIdentifier: "Concerns") as? Concerns{

                if let vc = (TabbarObj.viewControllers?[1] as! UINavigationController).viewControllers.first as? Concerns {
                    
                    Concerns_vc.concerns_Type = .Emotions_Feelings

                    vc.navigationController?.pushViewController(Concerns_vc, animated: false)
                }
                

            }
        }else if item == "Needs Board".localized(){
            methodMakeRootTabbar(index: 1)
            if let Concerns_vc = SB_Concerns.instantiateViewController(withIdentifier: "Concerns") as? Concerns{

                if let vc = (TabbarObj.viewControllers?[1] as! UINavigationController).viewControllers.first as? Concerns {
                    Concerns_vc.concerns_Type = .Needs_Board
                    vc.navigationController?.pushViewController(Concerns_vc, animated: false)
                }
            }
        }else if item == "Topic Board".localized(){
            methodMakeRootTabbar(index: 1)
            if let Concerns_vc = SB_Concerns.instantiateViewController(withIdentifier: "Concerns") as? Concerns{

                if let vc = (TabbarObj.viewControllers?[1] as! UINavigationController).viewControllers.first as? Concerns {
                    
                    Concerns_vc.concerns_Type = .Topic_Board

                    vc.navigationController?.pushViewController(Concerns_vc, animated: false)
                }
                

            }
        }else if item == "Trach".localized(){
            methodMakeRootTabbar(index: 1)
            if let Concerns_vc = SB_Concerns.instantiateViewController(withIdentifier: "Concerns") as? Concerns{

                if let vc = (TabbarObj.viewControllers?[1] as! UINavigationController).viewControllers.first as? Concerns {
                    
                    Concerns_vc.concerns_Type = .Trach

                    vc.navigationController?.pushViewController(Concerns_vc, animated: false)
                }
                

            }
        }
        else if item == "Vision Problems".localized(){
            methodMakeRootTabbar(index: 1)
            if let Concerns_vc = SB_Concerns.instantiateViewController(withIdentifier: "Concerns") as? Concerns{

                if let vc = (TabbarObj.viewControllers?[1] as! UINavigationController).viewControllers.first as? Concerns {

                    Concerns_vc.concerns_Type = .vision_problem

                    vc.navigationController?.pushViewController(Concerns_vc, animated: false)
                }

            }
        }
        else if item == "Pain Description".localized(){
            methodMakeRootTabbar(index: 1)
            if let Concerns_vc = SB_Concerns.instantiateViewController(withIdentifier: "Concerns") as? Concerns{
                
                if let vc = (TabbarObj.viewControllers?[1] as! UINavigationController).viewControllers.first as? Concerns {
                    Concerns_vc.concerns_Type = .pain_feel
                    vc.navigationController?.pushViewController(Concerns_vc, animated: false)
                }
            }
            
        }else if item == "Pain Location".localized(){
            methodMakeRootTabbar(index: 1)
            //"Pain description","Pain Scale","Pain Location"
//            methodMakeRootTabbar(index: 0)
            if let Concerns_vc = SB_HumanBody.instantiateViewController(withIdentifier: "HumanBody") as? HumanBody{
            
                if let vc = (TabbarObj.viewControllers?[1] as! UINavigationController).viewControllers.first as? Concerns {
//                    Concerns_vc.concerns_Type = .Pain
                    vc.navigationController?.pushViewController(Concerns_vc, animated: false)
                }
                

            }
        }else if item == "Swallowing".localized(){
            methodMakeRootTabbar(index: 1)
            if let Concerns_vc = SB_Concerns.instantiateViewController(withIdentifier: "Concerns") as? Concerns{

                if let vc = (TabbarObj.viewControllers?[1] as! UINavigationController).viewControllers.first as? Concerns {
                    
                    Concerns_vc.concerns_Type = .Swallowing

                    vc.navigationController?.pushViewController(Concerns_vc, animated: false)
                }
                
            }
        }else if item == "Saved Whiteboard".localized(){
            methodMakeRootTabbar(index: 1)
            if let WhiteboardList_vc = SB_Whiteboard_List.instantiateViewController(withIdentifier: "WhiteboardList") as? WhiteboardList{
                if let vc = (TabbarObj.viewControllers?[1] as! UINavigationController).viewControllers.first as? Concerns {
                    WhiteboardList_vc.comeFromSummary = false
                    vc.navigationController?.pushViewController(WhiteboardList_vc, animated: false)
                }

            }
//            methodMakeRootTabbar(index: 1)
//            if let Summery_vc = SB_Summery.instantiateViewController(withIdentifier: "SummeryVC") as? SummeryVC{
//                if let vc = (TabbarObj.viewControllers?[1] as! UINavigationController).viewControllers.first as? Concerns {
////                    Concerns_vc.concerns_Type = .Swallowing
//                    vc.navigationController?.pushViewController(Summery_vc, animated: false)
//                }
//                self.dismiss(animated: true, completion: nil)
//
//            }
        }else if item == "Saved Summary".localized(){
            methodMakeRootTabbar(index: 1)
            if let WhiteboardList_vc = SB_Whiteboard_List.instantiateViewController(withIdentifier: "WhiteboardList") as? WhiteboardList{
                if let vc = (TabbarObj.viewControllers?[1] as! UINavigationController).viewControllers.first as? Concerns {
                    WhiteboardList_vc.comeFromSummary = true
                    vc.navigationController?.pushViewController(WhiteboardList_vc, animated: false)
                }

            }
//            methodMakeRootTabbar(index: 1)
//            if let Summery_vc = SB_Summery.instantiateViewController(withIdentifier: "SummeryVC") as? SummeryVC{
//                if let vc = (TabbarObj.viewControllers?[1] as! UINavigationController).viewControllers.first as? Concerns {
////                    Concerns_vc.concerns_Type = .Swallowing
//                    vc.navigationController?.pushViewController(Summery_vc, animated: false)
//                }
//                self.dismiss(animated: true, completion: nil)
//
//            }
        }
        else if item == "Settings".localized(){
            methodMakeRootTabbar(index: 1)
            if let Settings_vc = SB_Settings.instantiateViewController(withIdentifier: "Settings") as? Settings{
                if let vc = (TabbarObj.viewControllers?[1] as! UINavigationController).viewControllers.first as? Concerns {
//                    Concerns_vc.concerns_Type = .Swallowing
                    vc.navigationController?.pushViewController(Settings_vc, animated: false)
                }

            }
//            methodMakeRootTabbar(index: 1)
//            if let Summery_vc = SB_Summery.instantiateViewController(withIdentifier: "SummeryVC") as? SummeryVC{
//                if let vc = (TabbarObj.viewControllers?[1] as! UINavigationController).viewControllers.first as? Concerns {
////                    Concerns_vc.concerns_Type = .Swallowing
//                    vc.navigationController?.pushViewController(Summery_vc, animated: false)
//                }
//                self.dismiss(animated: true, completion: nil)
//
//            }
        }
        else if item == "Pain Scale".localized() {
            //"Pain description","Pain Scale","Pain Location"
            methodMakeRootTabbar(index: 1)
            if let Concerns_vc = SB_Concerns.instantiateViewController(withIdentifier: "Pain_meter") as? Pain_meter {
                      
                if let vc = (TabbarObj.viewControllers?[1] as! UINavigationController).viewControllers.first as? Concerns {
//                    Concerns_vc.concerns_Type = .Pain
                    vc.navigationController?.pushViewController(Concerns_vc, animated: false)
                }
                   
            }

        }else if item == "Exit".localized(){
            let refreshAlert = UIAlertController(title: "Exit", message: "Are you sure want to exit?", preferredStyle: UIAlertController.Style.alert)

            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                 
                if let vc = SB_Fill_Details.instantiateViewController(withIdentifier: "Fill_Details") as? Fill_Details {
                    self.navigationController?.pushViewController(vc, animated: false)
                }
                
            }))

            refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
                self.dismiss(animated: true, completion: nil)
            }))

            present(refreshAlert, animated: true, completion: nil)
            
            
            
            
            
//            if let Concerns_vc = SB_HumanBody.instantiateViewController(withIdentifier: "HumanBody") as? HumanBody{
//
//                self.dismiss(animated: true, completion: nil)
//                if let vc = (TabbarObj.viewControllers?[0] as! UINavigationController).viewControllers.first as? Home_page {
//                    vc.navigationController?.pushViewController(Concerns_vc, animated: false)
//                }
//            }
        }
    }
}
