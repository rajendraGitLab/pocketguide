//
//  White_bord.swift
//  Pocket guide Communication
//
//  Created by Deepak on 02/11/21.
//

import UIKit
import AASignatureView

class White_bord: UIViewController,UIScrollViewDelegate {

    @IBOutlet weak var signatureView: AASignatureView!
    @IBOutlet weak var txtView: UITextView!
    @IBOutlet weak var txtView1: UIView!
    
    @IBOutlet weak var bgEmptyView: UIView!
    
    @IBOutlet weak var btnText: UIButton!
    @IBOutlet weak var btnDraw: UIButton!
    @IBOutlet weak var btnGototList: UIButton!
    
    @IBOutlet weak var btnSave: UIButton!
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnBackHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var pickedimg: UIImageView!
    @IBOutlet weak var imgViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var clsView: UICollectionView!
    @IBOutlet weak var zoomView : ZoomableView!
    @IBOutlet weak var sclView : UIScrollView!
    
    @IBOutlet weak var whiteBoardView : UIView!
    
    var comeFromNavigation = false
    var pickedImgArr = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        sclView.minimumZoomScale = 1
        sclView.maximumZoomScale = 10.0
        sclView.delegate = self
//        zoomView.sourceView = whiteBoardView
//        zoomView.isZoomable = true
        
        txtView.isHidden = true
        txtView1.isHidden = true
        btnBackHeightConstraint.constant = comeFromNavigation == true ? (isIpad ? 65 : 44) : 0
        
        let button = UIButton()
        button.tag = 1
        btnsAllAction(button)
        
        
        imgViewHeightConstraint.constant = 0
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("touchBegin"), object: nil)

        // function which is triggered when handleTap is called
        
//        btnSave.isHidden = true
//        btnGototList.isHidden = true
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        btnGototList.layer.cornerRadius = isIpad ? 20 : 10
    }
    @objc func methodOfReceivedNotification(notification: Notification) {
        self.bgEmptyView.isHidden = true
    }
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
            return zoomView
        }
    @IBAction func goBackAction(_ sender: UIButton){
        let alert = UIAlertController(title: title, message: "Are you sure you want to leave the white board without saving?".localized(), preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Yes".localized(), style: UIAlertAction.Style.default, handler: {(action:UIAlertAction!) in
            self.navigationController?.popViewController(animated: true)
        }))
        alert.addAction(UIAlertAction(title: "Cancel".localized(), style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)

    }
    @IBAction func gotoListTapped(_ sender: UIButton){
        if let vc = SB_Whiteboard_List.instantiateViewController(withIdentifier: "WhiteboardList") as? WhiteboardList {
            vc.comeFromSummary = false
            self.navigationController?.pushViewController(vc, animated: false)
        }
    }
    
    @IBAction func btnsAllAction(_ sender: UIButton) {
        
        btnText.borderColor = .clear
        btnDraw.borderColor = .clear

        if sender.tag == 1 {
            
            btnDraw.borderWidth = 1
//            txtView.text = ""
//            self.pickedImgArr.removeAll()
//            self.imgViewHeightConstraint.constant = 0
//            txtView.isHidden = true
//            txtView1.isHidden = true
            txtView.resignFirstResponder()
            
            btnDraw.borderColor = .black//hexStringToUIColor(hex: "#DBE9F7")
            
        }else if sender.tag == 2{
            
            txtView.isHidden = false
            txtView1.isHidden = false
            txtView.becomeFirstResponder()
            
            
            btnText.borderColor = .black//hexStringToUIColor(hex: "#DBE9F7")
            btnText.borderWidth = 1

        }else if sender.tag == 3{
            
            clearAllDrawlines()
            
        }else if sender.tag == 4{
            print("Save line")
            
            if !signatureView.isEmpty || txtView.text.trimmingCharacters(in: .whitespaces).count != 0{
                if let vc = SB_SaveBoard.instantiateViewController(withIdentifier: "SaveBoard") as? SaveBoard {
                    if txtView.isHidden == false && pickedImgArr.count > 0 {
                        vc.imageData = txtView1.asImage().pngData()!
                    }
//                    if !signatureView.isEmpty{
//                        vc.imageData = (signatureView.signature?.pngData())!
//                    }
                    vc.comeFromSummry = false
                    vc.enterTest = (txtView.text)!
                    vc.backVC = self
                    self.navigationController?.pushViewController(vc, animated: false)
                }
            }else{
                Toastnew.show(message: "Can't empty", controller: self)
            }
            
        }else if sender.tag == 5{
            txtView.isHidden = false
            txtView1.isHidden = false
            if let vc = SB_Whiteboard_List.instantiateViewController(withIdentifier: "AllimagesVC") as? AllimagesVC {
                vc.delegate = self
                self.present(vc, animated: true, completion: nil)
            }
            
//            let imgPicker = UIImagePickerController()
//            imgPicker.sourceType = .photoLibrary
//            imgPicker.delegate = self
//            imgPicker.allowsEditing = false
//            self.present(imgPicker,animated: false)
        }else{
            print("Tap out area")
        }
        
    }
    
    func clearAllDrawlines(){
        signatureView.clear()
        txtView.text = ""
        self.pickedImgArr.removeAll()
        self.clsView.reloadData()
        self.imgViewHeightConstraint.constant = 0
        bgEmptyView.isHidden = false
        txtView1.isHidden = false
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            print(touch)
            if touch.view == signatureView{
                if !signatureView.isEmpty {
                    bgEmptyView.isHidden = true
                }else{
                    bgEmptyView.isHidden = false
                }
            }
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            print(touch)
            if touch.view == signatureView{
                if !signatureView.isEmpty {
                    bgEmptyView.isHidden = true
                }else{
                    bgEmptyView.isHidden = false
                }
            }
        }
    }
}
extension White_bord: UINavigationControllerDelegate,UIImagePickerControllerDelegate{
//    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
//        let img = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
//        pickedimg.image = img
//        imgViewHeightConstraint.constant = isIpad ? 200 : 100
//        self.dismiss(animated: false)
//    }
}
extension White_bord: selectedImage {
    func selected(imgStr: [String]) {
        self.dismiss(animated: true)
        pickedImgArr = imgStr
        imgViewHeightConstraint.constant = isIpad ? 200 : 100
        clsView.reloadData()
    }
}
extension White_bord: UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return pickedImgArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "whiteBoardImgCell", for: indexPath) as! whiteBoardImgCell
        cell.bgView.dropShadow()
        cell.img.image = UIImage(named: pickedImgArr[indexPath.row])
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: isIpad ? 200 : 100, height: isIpad ? 200 : 100)
    }
}
class whiteBoardImgCell: UICollectionViewCell{
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var bgView: UIView!
}
