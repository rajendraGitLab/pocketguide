//
//  Whiteboard.swift
//  Pocket guide Communication
//
//  Created by Deepak on 08/11/21.
//

import UIKit
import Alamofire

var baseUrl = "https://stage.tasksplan.com/pocket_guide/api/add_multi_image"
var getList = "https://stage.tasksplan.com/pocket_guide/api/get_user_images_list"
var deleteList = "https://stage.tasksplan.com/pocket_guide/api/delete_multi_image"
var signUp = "https://stage.tasksplan.com/pocket_guide/api/social_media_login"
var backendIAP = "https://stage.tasksplan.com/pocket_guide/api/add_transaction"
var getTransactionIAP = "https://stage.tasksplan.com/pocket_guide/api/get_txn_list"

var allConcernImages = ["health_good", "health_okay", "health_bad", "Comes and Goes", "getting_better", "getting_worse", "the_same", "concerns_pain", "With Medication", "shortness_of_breath_BP", "concerns_swallowing", "concerns_Nausea", "concerns_bowels", "concerns_urination", "concerns_fatigue", "depressedN", "glasses", "Trach", "concerns_others", "Medication_Too Much", "too_big", "whole_crushed_liquid_New", "Medication_Side_Effects", "Medication_Information", "Medication_Too_Little", "timing", "don_t_want_pp", "Coughing", "Chest Pain", "Choking", "Squeezing", "Burning_ur", "achingDull", "Sharp", "Numb", "spasming", "pins_and_needlesN", "Cramping", "heavy__pain_", "shooting_pain", "throbbingN", "itchingN", "Food Sticking", "Heartburn", "Vomiting", "With Food", "Constipation", "with_food_and_drinks", "Diarrhea", "double-vision", "blurry", "left_vision", "right_vision", "whole", "crushed", "Liquid", "toilet", "bed", "With Drinks", "scheduled", "different_time", "spread_out", "anxious", "scared_icon", "angryN", "Frustrated", "exhaustedN", "sadN", "mucous", "suction", "Cap", "SpeakingValve", "Remove", "Wake up Tired", "Trouble Sleeping", "More Than Usual", "With Activity", "All Day", "gasBloating", "Blood", "bedpan", "Frequent", "urinary_incontinence", "Very_Little_Infrequent", "Blood_ur", "With Food Swallowing", "With Pills", "Morning", "Evening", "just_started", "when_standing", "when_sitting","addtoWite","be_quiet","beforeIwent","boughtSoup","brain_injury","can'tFind","helloAds","liftAlittle","materials","reading_problems","screenshotDont","seeing","threePeople","waterPain","whatU","writing_drawing","writing_problems1","pain0", "pain1", "pain2", "pain3", "pain4", "pain5", "body_1", "body_3", "body_2", "Mouth", "Neck", "Chest-Breasts", "Shoulder", "Upper-Arm", "Elbow", "Abdomen", "hip", "Lower-Leg", "Pelvis-Genitals", "Hand", "Fingers", "Foot-Toes", "Ankle", "Wrist", "Thigh", "knee", "Forearm"]

//, "Forehead_F", "body_3", "Eye_F", "body_2", "Ear_F", "Mouth", "Mouth_F", "Neck", "Neck_F", "Chest-Breasts", "Chest-Breasts_F", "Shoulder", "Shoulder_F", "Upper-Arm", "Upper-Arm_F", "Elbow", "Elbow_F", "Abdomen", "Abdomen_F", "hip", "hip_F", "Lower-Leg", "Lower-Leg_F", "Pelvis-Genitals", "Pelvis-Genitals_F", "Hand", "Hand_F", "Fingers", "Fingers_F", "Foot-Toes", "Foot-Toes_F", "Ankle", "Ankle_F", "Wrist", "Wrist_F", "Thigh", "Thigh_F", "knee", "knee_F", "Forearm", "Forearm_F"

class SaveBoard: UIViewController {
    
    @IBOutlet weak var txtFileName: UITextField!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var titleLbl: UILabel!
    
    var imageData = Data()
    var enterTest = String()
    var comeFromSummry = false
    
    var backVC : White_bord!

    override func viewDidLoad() {
        super.viewDidLoad()
        let filt = allConcernImages.removingDuplicates()
        print(allConcernImages.count)
        print(filt.count)
        titleLbl.text = comeFromSummry ? "Summary".localized() : "Whiteboard".localized()
    }

    @IBAction func btnBackAction(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func btnSaveAction(_ sender: UIButton) {
        
        if txtFileName.text?.trimmingCharacters(in: .whitespaces).count ?? 0 > 0 {
            
            if !NetworkUtils.isNetworkReachable() {
                
                Toastnew.show(message: "no internet connection", controller: self)
                return
            }
            let parm = ["type": comeFromSummry ? "summary" : "whiteboard","user_id" : UIDevice.current.identifierForVendor!.uuidString
                       ,"image_text" : enterTest,"api" : "uploadFile","image_title" : txtFileName.text ?? ""
                        ]
            
            print("parm : \(parm)")

            let urlRequest: Alamofire.URLRequestConvertible = URLRequest(url: URL(string: baseUrl)!)
            upload(image: imageData, to: baseUrl, params: parm)
            
//            upload(file_name : txtFileName.text ?? "")
        }else{
//            self.showToast(message: "Please enter save as name.", font: UIFont(name: "Navigo-Regular", size: 16) ?? UIFont.systemFont(ofSize: 17))
            Toastnew.show(message: "Can't empty", controller: self)

        }
    }
}


extension SaveBoard {
    
    func upload(image: Data, to url: String, params: [String: Any]) {
               
        let headers = HTTPHeaders(["content-type": "multipart/form-data",
                                   "Authorization":"Bearer"])
        
        
        Util.showHud(view: self.view)
        AF.upload(multipartFormData: { multiPart in
            for (key, value) in params {
                if let temp = value as? String {
                    multiPart.append(temp.data(using: .utf8)!, withName: key)
                }
                if let temp = value as? Int {
                    multiPart.append("\(temp)".data(using: .utf8)!, withName: key)
                }
//                if let temp = value as? NSArray {
//                    temp.forEach({ element in
//                        let keyObj = key + "[]"
//                        if let string = element as? String {
//                            multiPart.append(string.data(using: .utf8)!, withName: keyObj)
//                        } else
//                            if let num = element as? Int {
//                                let value = "\(num)"
//                                multiPart.append(value.data(using: .utf8)!, withName: keyObj)
//                        }
//                    })
//                }
            }
            multiPart.append(image, withName: "multi_user_image[0]", fileName: "file.png", mimeType: "image/png")
            print(multiPart)
        }, to: baseUrl, method: .post, headers: headers)
            .uploadProgress(queue: .main, closure: { progress in
                //Current upload progress of file
                print("Upload Progress: \(progress.fractionCompleted)")
            })
            .responseJSON(completionHandler: { response in
                //Do what ever you want to do with response
                
                Util.hideHud(view: self.view)
                switch(response.result)
                {
                
                            case .success(_):
                                if let data = response.value
                                {
                                    print(response.value as Any)
                                    print(data)
                                    let statusCode = response.response?.statusCode ?? 0
                                    if statusCode == 401{
                                        //Handel session expired work
                                    }else{
                                        DispatchQueue.main.async {
                                            //success(data)
                                            if self.comeFromSummry {
                                                self.navigationController?.popViewController(animated: true)
                                            }else{
                                                self.backVC.clearAllDrawlines()
                                                self.navigationController?.popToRootViewController(animated: false)
                                                TabbarObj.selectedIndex = 4
                                            }
                                            
                                        }
                                    }
                                }
                                break
                            case .failure(_):
                                if let data = response.error
                                {
                                    print(response.value as Any)
                                    print(data)
                                    let statusCode = response.response?.statusCode ?? 0
                                    //failure("serverError", statusCode)
                                }
                                break
                            }
                
                            
                
            })
    }
}
extension Array where Element: Hashable {
    func removingDuplicates() -> [Element] {
        var addedDict = [Element: Bool]()

        return filter {
            addedDict.updateValue(true, forKey: $0) == nil
        }
    }

    mutating func removeDuplicates() {
        self = self.removingDuplicates()
    }
}
