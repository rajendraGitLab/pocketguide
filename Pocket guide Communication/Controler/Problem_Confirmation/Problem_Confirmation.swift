//
//  problem_Confirmation.swift
//  Pocket guide Communication
//
//  Created by Deepak on 09/11/21.
//

import UIKit

class Problem_Confirmation: UIViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view3: UIView!
    
    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var btn2: UIButton!
    @IBOutlet weak var btn3: UIButton!
    
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var nextBtnView: UIView!
    
    var allButton = [UIButton()]
    var allView = [UIView()]
    
    var titleVC = "Is this a new problem?"
    
    var selectIndex = 1
    
    var concerns_Type: Concerns_Type = .All_Concerns

    override func viewDidLoad() {
        super.viewDidLoad()
        nextBtnView.isHidden = true
        lblTitle.text = titleVC.localized()
        
        allButton = [btn1,btn2,btn3]
        allView = [view1,view2,view3]
        
        for i in 0 ... allButton.count - 1 {
            
            allButton[i].tag = i
            allButton[i].addTarget(self, action: #selector(btnSelectBtnall(_:)), for: .touchUpInside)
        }
        
        for i in 0 ... allView.count - 1 {
            allView[i].tag = i
        }
    }
    
    @IBAction func btnback(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @objc func btnSelectBtnall(_ sender: UIButton) {
                
        SummeryInfo.shared.removeWithTitle(imgtitle: "New Problem".localized())
        SummeryInfo.shared.removeWithTitle(imgtitle: "Not a New Problem".localized())
        SummeryInfo.shared.removeWithTitle(imgtitle: "I Don’t Know".localized())
        
        if sender.tag == 0 {
            if IS_MALE{
                playSound(fileName:  SELECTED_LANG == "en" ? "Yes" : "yes_si".localized(), file_Extension: "wav")
            }else{
                playSound(fileName: SELECTED_LANG == "en" ? "Yes_HRU_En" : "si (yes)_HRU_Sp", file_Extension: "mp3")
            }
            SummeryInfo.shared.appendSummeryInfo(imgName: UIImage(named: "checked")!, imgTitle: "New Problem".localized(), timeStamp: NSDate().timeIntervalSince1970)
        }else if sender.tag == 1 {
            if IS_MALE{
                playSound(fileName:  SELECTED_LANG == "en" ? "No" : "No_no", file_Extension: "wav")
            }else{
                playSound(fileName: SELECTED_LANG == "en" ? "No_HRU_En" : "No (no)_HRU_Sp", file_Extension: "mp3")
            }
            SummeryInfo.shared.appendSummeryInfo(imgName: UIImage(named: "close")!, imgTitle: "Not a New Problem".localized(), timeStamp: NSDate().timeIntervalSince1970)
        }else{
            if IS_MALE{
                playSound(fileName:  SELECTED_LANG == "en" ? "I_Do_not_Know" : "No se(i don't know)_male_Sp", file_Extension: "wav")
            }else{
                playSound(fileName:  SELECTED_LANG == "en" ? "I dont know_Female_en" : "no se(i don't know)_Female_Sp", file_Extension: "wav")
            }
            SummeryInfo.shared.appendSummeryInfo(imgName: UIImage(named: "concerns_others")!, imgTitle: "I Don’t Know".localized(), timeStamp: NSDate().timeIntervalSince1970)
        }
        selectindex(index: sender.tag)
        nextBtnView.isHidden = false
        //self.navigationController?.popToRootViewController(animated: false)
    }
    
    
    func selectindex(index : Int){
        
        for i in allView{
            if i.tag == index{
                i.borderWidth = isIpad ? 8 : 4
                i.borderColor = hexStringToUIColor(hex: "#0F9AF0")
            }else{
                i.borderWidth = 0
                i.borderColor = .clear
            }
        }
        if index == 0 {
            selectIndex = 2
            
            if concerns_Type == .Swallowing {
                
                if let Concerns_vc = SB_Calender.instantiateViewController(withIdentifier: "Calender") as? Calender{
                    
//                    Concerns_vc.titleVC = getTitle
                    Concerns_vc.concerns_Type = .Swallowing
                        self.navigationController?.pushViewController(Concerns_vc, animated: false)
                    }
            }
        }else if index == 1{
            
            if concerns_Type == .Swallowing {
            //self.navigationController?.popViewController(animated: false)
            }
            selectIndex = 3
            
        }else if index == 2 {
            selectIndex = 1
//            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
//                TabbarObj.selectedIndex = 2
//                self.navigationController?.popToRootViewController(animated: false)
//            }
        }
    }
    
    @IBAction func btnNextAction(_ sender: UIButton) {
        if let Concerns_vc = SB_Summery.instantiateViewController(withIdentifier: "SummeryVC") as? SummeryVC{
//               Concerns_vc.titleVC = getTitle
            
                self.navigationController?.pushViewController(Concerns_vc, animated: false)
            }
    }
}
