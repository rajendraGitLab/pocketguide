//
//  Health_type_Cell.swift
//  Pocket guide Communication
//
//  Created by Deepak on 29/10/21.
//

import UIKit

class Health_type_Cell: UICollectionViewCell {
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var image: UIImageView!
    
}
