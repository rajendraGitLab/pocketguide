//
//  Home_page.swift
//  Pocket guide Communication
//
//  Created by Deepak on 28/10/21.
//

import UIKit

var howRU = summeryDetails()
var SummeryDict = [String: summeryDetails]()

class Home_page: UIViewController,UIScrollViewDelegate {
    
    var healthTypeList = [(UIImage(named: "health_good"),"Good".localized()),(UIImage(named: "health_okay"),"Okay".localized()),(UIImage(named: "health_bad"),"Bad".localized()),(UIImage(named: "Comes and Goes"),"Up and Down".localized()),(UIImage(named: "getting_better"),"Getting Better".localized()),(UIImage(named: "getting_worse"),"Getting Worse".localized()),(UIImage(named: "the_same"),"The Same".localized())]

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collectionViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var sidemenubtn : UIButton!
    @IBOutlet weak var zoomView : ZoomableView!
    @IBOutlet weak var sclView : UIScrollView!
    
    @IBOutlet weak var indicatorView: UIView!
    @IBOutlet weak var indicatorViewTopConstraint: NSLayoutConstraint!
    
    fileprivate (set) var scrollBar : VerticalScrollBar!
    
    var selectIndex = -1
    var currentCellFrame = Int()
    var comeFromIntroduction = false
    var isZooming = false

    override func viewDidLoad() {
        super.viewDidLoad()
        sclView.minimumZoomScale = 1
        sclView.maximumZoomScale = 10.0
        sclView.delegate = self
//        zoomView.sourceView = collectionView
//        zoomView.isZoomable = true
        
        if IS_MALE{
            if SELECTED_LANG == "en" {
                playSound(fileName: "How are you_Male_en", file_Extension: "mp3")
            }else{
                playSound(fileName: "How are you_Como estas_Male_Sp", file_Extension: "wav")
            }
        }else{
            playSound(fileName: SELECTED_LANG == "en" ? "How are you_HRU_En" : "como estas_ (how are you_)_HRU_Sp", file_Extension: "mp3")
        }
        collectionView.reloadData()
        self.scrollBar = VerticalScrollBar(frame: CGRect.zero, targetScrollView: self.collectionView)
                self.view.addSubview(self.scrollBar!)
        
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.scrollBar.frame = CGRect(x: UIScreen.main.bounds.width - (isIpad ? 30 : 35) - (UIApplication.shared.windows[0].safeAreaInsets.left), y: self.view.frame.height - self.collectionView.frame.height - (isIpad ? 90 : (UIDevice.current.hasNotch ? (comeFromIntroduction ? 30 : 65) : (comeFromIntroduction ? 10 : 50))) - (isIpad ? 40 : -10), width: self.indicatorView.frame.width * 1.6, height: self.collectionView.frame.height)
        self.scrollBar.backgroundColor = .clear
    }
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        isZooming = true
    }
    func scrollViewDidEndZooming(_ scrollView: UIScrollView, with view: UIView?, atScale scale: CGFloat) {
        isZooming = false
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if !isZooming{
            let totalScroll = scrollView.contentSize.height - self.collectionView.frame.height
            let ratio = totalScroll/(self.collectionView.frame.height - self.indicatorView.frame.height)
            self.indicatorViewTopConstraint.constant = (scrollView.contentOffset.y)/(ratio)
        }
        
    }
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
            return zoomView
        }
    @IBAction func btnMenuAction(_ sender: UIButton) {
        
//        if let vc = SB_Tab.instantiateViewController(withIdentifier: "Tab_bar") as? Tab_bar {
//
//            let nav = UINavigationController(rootViewController: vc)
//            nav.interactivePopGestureRecognizer?.delegate = nil
//            nav.setNavigationBarHidden(true, animated: false)
//            UIApplication.shared.keyWindow?.rootViewController = nav
//            UIApplication.shared.keyWindow?.makeKeyAndVisible()
//        }
        Util.showSideMenu(navC: self)
        //Util.showSideMenu(navC: self)
    }
    
}

extension Home_page : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        healthTypeList.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Health_type_Cell", for: indexPath) as! Health_type_Cell
        cell.image.image = healthTypeList[indexPath.row].0
        cell.title.text = healthTypeList[indexPath.row].1.localized()
        cell.title.font = iconTitleFont
//        if healthTypeList[indexPath.row].1 == "health_good" {
//            cell.image.contentMode = .center
//        }
        
        cell.image.contentMode = .scaleAspectFit
        if selectIndex != -1{
            if indexPath.row == selectIndex{
                cell.bgView.borderColor =  hexStringToUIColor(hex: "#0F9AF0")
                cell.bgView.borderWidth = isIpad ? 8 : 4
            }else{
                cell.bgView.borderColor = .clear
            }
        }else{
                cell.bgView.borderColor = .clear
        }
        cell.bgView.dropShadow()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let collectionHeight = collectionView.bounds.height

        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
//        flowLayout.collectionView?.isPagingEnabled = true
        let totalSpace = flowLayout.sectionInset.left
        + flowLayout.sectionInset.right
        + ( (noOfCellsInRow == 2 ? (collectionView.frame.height/1.4) : flowLayout.minimumLineSpacing) * CGFloat((noOfCellsInRow == 6 ? 3 : noOfCellsInRow) - 1))

        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat((noOfCellsInRow == 6 ? 3 : noOfCellsInRow)))
        currentCellFrame = noOfCellsInRow == 1 ? Int(collectionHeight - (isIpad ? 40 : 20)) : size - (isIpad ? 25 : 12)
        
//        self.GuideView_Category2WidthConstraint?.constant = CGFloat(currentCellFrame)
//        self.GuideView_Category2TopConstraint.constant = collectionView.bounds.height/2 - CGFloat(currentCellFrame/2)
        return CGSize(width: noOfCellsInRow == 6 ? Int(collectionHeight/(comeFromIntroduction ? 2.1 : 1.8) - (4 * flowLayout.minimumInteritemSpacing)) : currentCellFrame, height: noOfCellsInRow == 6 ? Int(collectionHeight/(comeFromIntroduction ? 2.4 : 2.1) - (4 * flowLayout.minimumInteritemSpacing)) : currentCellFrame)
        
        
//                if (vcMode == "normal"){
//                    if concerns_Type == .pain_feel || concerns_Type == .Swallowing || concerns_Type == .Nausea || concerns_Type == .Fatigue || concerns_Type == .Bowels || concerns_Type == .Urination{
//                        return CGSize(width: (collectionView.bounds.width / 3 - 20), height: 175)
//                    }
//                    return isIpad == true ? CGSize(width: collectionWidth/3, height: collectionWidth/3) : CGSize(width: collectionWidth - 9, height: collectionWidth - 9)
//                }else{
//                    return CGSize(width: (collectionView.bounds.width / 3 - 20), height: 175)
//                }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        if noOfCellsInRow <= 2 {
            return isIpad ? 40 : 20
        }
        return 1
    }
   
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if noOfCellsInRow == 6 {
            return isIpad ? (comeFromIntroduction ? 60 : 50) : 25
        }
        return collectionView.bounds.height/2 - CGFloat(currentCellFrame/2)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
//
//        KtFlowlayout.minimumInteritemSpacing = 10
////        layout.minimumLineSpacing = 401
//        KtFlowlayout.minimumLineSpacing = collectionView.bounds.height/2 - CGFloat(currentCellFrame/2)
////        flowLayout.collectionView?.isPagingEnabled = true
        if noOfCellsInRow <= 2 {
            let totalCellWidth = Int(currentCellFrame) * noOfCellsInRow
            let totalSpacingWidth = Int(isIpad ? 60 : 30) * (noOfCellsInRow - 1)

            let leftInset = (collectionView.bounds.width - CGFloat(totalCellWidth + totalSpacingWidth)) / 2
            let rightInset = leftInset
            return UIEdgeInsets(top: collectionView.bounds.height/2 - CGFloat(currentCellFrame/2), left: rightInset, bottom: collectionView.bounds.height/2 - CGFloat(currentCellFrame/2), right: rightInset)
        }else{
            if noOfCellsInRow == 6 {
                return UIEdgeInsets(top: comeFromIntroduction ? CGFloat(Double(currentCellFrame)/7) : 0, left: isIpad ? 25 : hasTopNotch ? screenHeight/3.9 : screenHeight/4.8, bottom: 0, right: isIpad ? 15 : hasTopNotch ? screenHeight/3.9 : screenHeight/4.8)
            }
            return UIEdgeInsets(top: collectionView.bounds.height/2 - CGFloat(currentCellFrame/2), left: 0, bottom: collectionView.bounds.height/2 - CGFloat(currentCellFrame/2), right: 0)
      }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        selectIndex = indexPath.row
        let title = healthTypeList[indexPath.row].1
        if title == "Getting Better".localized(){
            if IS_MALE{
                if SELECTED_LANG == "en" {
                    playSound(fileName: "getting better_male_en", file_Extension: "mp3")
                }else{
                    playSound(fileName: "mejorando", file_Extension: "wav")
                }
            }else{
                playSound(fileName: SELECTED_LANG == "en" ? "Getting Better_HRU_En" : "mejorando (getting better)_HRU_Sp", file_Extension: "mp3")
            }
        }else if title == "Getting Worse".localized(){
            
            if IS_MALE{
                if SELECTED_LANG == "en" {
                    playSound(fileName: "getting worse_male_en", file_Extension: "mp3")
                }else{
                    playSound(fileName: "empeorando", file_Extension: "wav")
                }
            }else{
                playSound(fileName: SELECTED_LANG == "en" ? "Getting Worse_HRU_En" : "empeorando (getting worse)_HRU_Sp", file_Extension: "mp3")
            }
        }else if title == "Good".localized(){
          
            if IS_MALE{
                playSound(fileName: SELECTED_LANG == "en" ? "Good" : "Bueno (Good)_male_Sp", file_Extension: "wav")
            }else{
                playSound(fileName: SELECTED_LANG == "en" ? "good_HRU_En" : "bueno (good)_HRU_Sp", file_Extension: "mp3")
            }
        }
        else if title == "Up and Down".localized(){
           
            if IS_MALE{
                playSound(fileName: SELECTED_LANG == "en" ? "Up and down" : "up and down_arriba y abajo", file_Extension: "wav")
            }else{
                playSound(fileName: SELECTED_LANG == "en" ? "Up and Down_HRU_En" : "arriba y abajo (up and down)_HRU_Sp", file_Extension: "mp3")
            }
        }else if title == "Bad".localized(){
            
            if IS_MALE{
                playSound(fileName: SELECTED_LANG == "en" ? "Bad" : "Malo(bad)_Male_Sp", file_Extension: "wav")
            }else{
                playSound(fileName: SELECTED_LANG == "en" ? "bad_HRU_En" : "malo (bad)_HRU_Sp", file_Extension: "mp3")
            }
        }else if title == "Okay".localized(){
            
            if IS_MALE{
                playSound(fileName: SELECTED_LANG == "en" ? "Okay" : "Esta bien(okay)_Male_Sp", file_Extension: "wav")
            }else{
                playSound(fileName: SELECTED_LANG == "en" ? "Okay_HRU_En" : "esta bien (okay)_HRU_Sp", file_Extension: "mp3")
            }
        }else if title == "The Same".localized(){
            
            if IS_MALE{
                if SELECTED_LANG == "en" {
                    playSound(fileName: "the same_Male_en", file_Extension: "mp3")
                }else{
                    playSound(fileName: "Lo mismo(the same)_Male_Sp", file_Extension: "wav")
                }
            }else{
                playSound(fileName: SELECTED_LANG == "en" ? "the same_HRU_En" : "lo mismo (the same)_HRU_Sp", file_Extension: "mp3")
            }
        }else{
            playSound(fileName: title, file_Extension: "wav")
        }
        
        
        if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
            
            vc.concerns_Type = .All_Concerns
            vc.logoImage = healthTypeList[indexPath.row].0!
            vc.typeTitle = healthTypeList[indexPath.row].1
            howRU = summeryDetails(title: healthTypeList[indexPath.row].1, img: healthTypeList[indexPath.row].0!)
            self.navigationController?.pushViewController(vc, animated: false)
        }
        
//        if let vc = SB_Concerns.instantiateViewController(withIdentifier: "Concerns") as? Concerns {
//            self.navigationController?.pushViewController(vc, animated: false)
//        }
        
//        if indexPath.row == 3 {
//            if let vc = SB_Concerns.instantiateViewController(withIdentifier: "Concerns") as? Concerns {
//                self.navigationController?.pushViewController(vc, animated: false)
//            }
//        }

        collectionView.reloadData()
    }
    
}
struct summeryDetails {
    var title: String?
    var img: UIImage?
}
