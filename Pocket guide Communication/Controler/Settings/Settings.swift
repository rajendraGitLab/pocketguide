//
//  Settings.swift
//  Pocket guide Communication
//
//  Created by Admin on 25/08/22.
//

import UIKit

class Settings: UIViewController {
    
    @IBOutlet weak var iconView1: UIView!
    @IBOutlet weak var iconView2: UIView!
    @IBOutlet weak var iconView3: UIView!
    @IBOutlet weak var iconView4: UIView!
    @IBOutlet weak var iconView5: UIView!
    @IBOutlet weak var iconView6: UIView!
    
    @IBOutlet weak var iconBtn1: UIView!
    @IBOutlet weak var iconBtn2: UIView!
    @IBOutlet weak var iconBtn3: UIView!
    @IBOutlet weak var iconBtn4: UIView!
    @IBOutlet weak var iconBtn5: UIView!
    @IBOutlet weak var iconBtn6: UIView!
    
    @IBOutlet weak var iconLbl1: UILabel!
    @IBOutlet weak var iconLbl2: UILabel!
    @IBOutlet weak var iconLbl3: UILabel!
    @IBOutlet weak var iconLbl4: UILabel!
    @IBOutlet weak var iconLbl5: UILabel!
    @IBOutlet weak var iconLbl6: UILabel!
    
    @IBOutlet weak var maleImg: UIImageView!
    @IBOutlet weak var femaleImg: UIImageView!
    @IBOutlet weak var englishImg: UIImageView!
    @IBOutlet weak var spanishImg: UIImageView!
    
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var nextBtn: UIButton!
    
    var allViews = [UIView()]
    var allLbls = [UILabel()]
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
    }
    
    func setUpUI(){
        
        backBtn.isHidden = isNew ? true : false
        nextBtn.isHidden = isNew ? false : true
        
        allViews = [iconView1,iconView2,iconView3,iconView4,iconView5,iconView6]
        allLbls = [iconLbl1,iconLbl2,iconLbl3,iconLbl4,iconLbl5,iconLbl6]
        for i in 0 ... allViews.count - 1 {
            allViews[i].layer.cornerRadius = isIpad ? 22 : 11
            if i == noOfCellsInRow - 1{
                allViews[i].backgroundColor = hexStringToUIColor(hex: "#0F9AF0")
                allLbls[i].textColor = .white
            }
            
            allViews[i].layer.shadowColor = UIColor.gray.cgColor
            allViews[i].layer.shadowOffset = .zero
            allViews[i].layer.shadowRadius = 10
            allViews[i].layer.shadowOpacity = 0.35
            allViews[i].layer.masksToBounds = false
            //dropShadow(color: .gray, opacity: 0.3, offSet: .zero, radius: 1, scale: false)
        }
        if IS_MALE == true {
            maleImg.image = UIImage(named: "genderSelected")
            femaleImg.image = UIImage(named: "genderUnSelected")
        }else{
            maleImg.image = UIImage(named: "genderUnSelected")
            femaleImg.image = UIImage(named: "genderSelected")
        }
        if SELECTED_LANG == "en" {
            englishImg.image = UIImage(named: "genderSelected")
            spanishImg.image = UIImage(named: "genderUnSelected")
        }else{
            englishImg.image = UIImage(named: "genderUnSelected")
            spanishImg.image = UIImage(named: "genderSelected")
        }
    }
    @IBAction func btnback(_ sender: UIButton) {
//        self.navigationController?.popViewController(animated: false)
        methodMakeRootTabbar(index: 1)
    }
    @IBAction func btnNextAction(_ sender: UIButton) {
        
        if let vc = SB_Fill_Details.instantiateViewController(withIdentifier: "Fill_Details") as? Fill_Details {
            self.navigationController?.pushViewController(vc, animated: false)
        }
    }
    @IBAction func chooseCategoryAction(_ sender: UIButton){
        switch (sender.tag)
        {
        case 5:
            maleImg.image = UIImage(named: "genderSelected")
            femaleImg.image = UIImage(named: "genderUnSelected")
            
            setGenderInUserDefault(isMale: true)
        case 6:
            femaleImg.image = UIImage(named: "genderSelected")
            maleImg.image = UIImage(named: "genderUnSelected")
            
            setGenderInUserDefault(isMale: false)
        case 7:
            englishImg.image = UIImage(named: "genderSelected")
            spanishImg.image = UIImage(named: "genderUnSelected")
            setlanguageInUserDefault(isEnglish: "en")
            if !comeFromHowRuVC {
                methodMakeRootTabbar(index: 1)
            }
        case 8:
            spanishImg.image = UIImage(named: "genderSelected")
            englishImg.image = UIImage(named: "genderUnSelected")
            setlanguageInUserDefault(isEnglish: "es")
            if !comeFromHowRuVC {
                methodMakeRootTabbar(index: 1)
            }
        case 11:
            highlightIcon(enableView: iconView1, disableView: [iconView2,iconView3,iconView4,iconView5,iconView6],whiteLbl: iconLbl1,blackLbls: [iconLbl2,iconLbl3,iconLbl4,iconLbl6])
            setItemsPerRowInUserDefault(itemCount: 1)
        case 12:
            highlightIcon(enableView: iconView2, disableView: [iconView1,iconView3,iconView4,iconView5,iconView6],whiteLbl: iconLbl2,blackLbls: [iconLbl1,iconLbl3,iconLbl4,iconLbl6])
            setItemsPerRowInUserDefault(itemCount: 2)
        case 13:
            highlightIcon(enableView: iconView3, disableView: [iconView1,iconView2,iconView4,iconView5,iconView6],whiteLbl: iconLbl3,blackLbls: [iconLbl1,iconLbl2,iconLbl4,iconLbl6])
            setItemsPerRowInUserDefault(itemCount: 3)
        case 14:
            highlightIcon(enableView: iconView4, disableView: [iconView1,iconView2,iconView3,iconView5,iconView6],whiteLbl: iconLbl4,blackLbls: [iconLbl1,iconLbl2,iconLbl3,iconLbl6])
            setItemsPerRowInUserDefault(itemCount: 4)
        case 15:
            highlightIcon(enableView: iconView5, disableView: [iconView1,iconView2,iconView3,iconView4,iconView6],whiteLbl: iconLbl5,blackLbls: [iconLbl1,iconLbl2,iconLbl3,iconLbl6])
            setItemsPerRowInUserDefault(itemCount: 5)
        case 16:
            highlightIcon(enableView: iconView6, disableView: [iconView1,iconView2,iconView3,iconView4,iconView5],whiteLbl: iconLbl6,blackLbls: [iconLbl1,iconLbl2,iconLbl3,iconLbl4])
            setItemsPerRowInUserDefault(itemCount: 6)
        default:
            print("Switch default")
        }
    }
    
    private func highlightIcon(enableView: UIView,disableView: [UIView],whiteLbl: UILabel,blackLbls: [UILabel]){
        enableView.backgroundColor = hexStringToUIColor(hex: "#0F9AF0")
        for i in 0 ... disableView.count - 1 {
            disableView[i].backgroundColor = .white
        }
        
        whiteLbl.textColor = .white
        for i in 0 ... blackLbls.count - 1 {
            blackLbls[i].textColor = .black
        }
    }
}
