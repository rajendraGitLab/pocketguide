//
//  Healthcare_Providers_Aphasia.swift
//  Pocket guide Communication
//
//  Created by Deepak on 29/10/21.
//

import UIKit

class Healthcare_Providers_Aphasia: UIViewController {
    
    @IBOutlet weak var btnback: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    
    @IBOutlet weak var lbl1: UILabel!
    @IBOutlet weak var lbl2: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnback.isHidden = !isNew

        
//        btnback.isHidden = isNew
//        btnNext.isHidden = !isNew
        
//        lbl1.text = "Communication problems between Healthcare Providers (HCP) and People with Aphasia (PWA) impedes both diagnosis and therapy, with considerable implications for healthcare quality (Rijssen, Veldkamp, et al., 2021).\n\n Aphasia is a communication impairment that affects language: speaking, reading, writing, and understanding.\n\n How do PWA communicate medical concerns and symptoms experienced if they have difficulty speaking, reading, writing, and understanding?"
        
        lbl1.text = "Aphasia is a communication impairment that affects language: speaking, reading, writing, and understanding.\n\nCommunication problems between Healthcare Providers (HCP) and People with Aphasia (PWA) impedes both diagnosis and therapy, with considerable implications for healthcare quality (Rijssen, Veldkamp, et al., 2021).\n\nHow do PWA communicate medical concerns and symptoms experienced if they have difficulty speaking, reading, writing, and understanding?".localized()
        
        
//        lbl2.text = "Communication problems between Healthcare Providers (HCP) and People with Aphasia (PWA) impedes both diagnosis and therapy, with considerable implications for healthcare quality (Rijssen, Veldkamp, et al., 2021).\n\n Aphasia is a communication impairment that affects language: speaking, reading, writing, and understanding.\n\n How do PWA communicate medical concerns and symptoms experienced if they have difficulty speaking, reading, writing, and understanding?"
       
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func btnNextAction(_ sender: UIButton) {
        
        if let Concerns_vc = SB_Tips.instantiateViewController(withIdentifier: "HowDoesWorkVC") as? HowDoesWorkVC{
            self.navigationController?.pushViewController(Concerns_vc, animated: false)

        }
    }
}
