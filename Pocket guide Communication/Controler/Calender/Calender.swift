//
//  Calender.swift
//  Pocket guide Communication
//
//  Created by Deepak on 02/11/21.
//

import UIKit
import FSCalendar

var isDaySelected = Bool()
class Calender: UIViewController, FSCalendarDataSource, FSCalendarDelegate {

    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var clView: FSCalendar!
    @IBOutlet weak var weekView: UIView!
    @IBOutlet weak var todayView: UIView!
    @IBOutlet weak var allView: UIView!
    @IBOutlet weak var allViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var segment: UISegmentedControl!
    
    
    @IBOutlet weak var todayRightTickImg1: UIImageView!
    @IBOutlet weak var todayRightTickImg2: UIImageView!
    @IBOutlet weak var todayRightTickImg3: UIImageView!
    
    @IBOutlet weak var weekRightTickImg1: UIImageView!
    @IBOutlet weak var weekRightTickImg2: UIImageView!
    @IBOutlet weak var weekRightTickImg3: UIImageView!
    @IBOutlet weak var weekRightTickImg4: UIImageView!
    @IBOutlet weak var weekRightTickImg5: UIImageView!
    @IBOutlet weak var weekRightTickImg6: UIImageView!
    @IBOutlet weak var weekRightTickImg7: UIImageView!
    
    @IBOutlet weak var saturdayLbl: UILabel!
    @IBOutlet weak var sundayLbl: UILabel!
    
    @IBOutlet weak var segMentView1: UIView!
    @IBOutlet weak var segMentView2: UIView!
    @IBOutlet weak var segMentView3: UIView!
    
    @IBOutlet weak var segMentLbl1: UILabel!
    @IBOutlet weak var segMentLbl2: UILabel!
    @IBOutlet weak var segMentLbl3: UILabel!
    
    @IBOutlet weak var scrollV: UIScrollView!
  
    var titleVC = "When did it start?"
    var concerns_Type: Concerns_Type = .none
    var allImgView = [UIView()]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        allImgView = [todayRightTickImg1,todayRightTickImg2,todayRightTickImg3,weekRightTickImg1,weekRightTickImg2,weekRightTickImg3,weekRightTickImg4,weekRightTickImg5,weekRightTickImg6,weekRightTickImg7]

        sundayLbl.text = SELECTED_LANG == "en" ? "S" : "D"
        saturdayLbl.text = SELECTED_LANG == "en" ? "S" : "S"
        lblTitle.text = titleVC.localized()
        self.title = "FSCalendar"
        clView.locale = NSLocale(localeIdentifier: SELECTED_LANG) as Locale
        clView.appearance.headerTitleFont = UIFont(name: "Navigo-Regular", size: isIpad ? 25 : 16)
        clView.appearance.weekdayFont = UIFont(name: "Navigo-Regular", size: isIpad ? 20 : 13)
        // In loadView or viewDidLoad

//        calendar.appearance.headerTitleFont = isIpad == true ? UIFont.init(name: "Navigo-Bold", size: 36) : UIFont.init(name: "Navigo-Bold", size: 12)
//        calendar.appearance.weekdayFont = isIpad == true ? UIFont.init(name: "Navigo-Regular", size: 32) : UIFont.init(name: "Navigo-Regular", size: 11)
//        calendar.appearance.titleFont = isIpad == true ? UIFont.init(name: "Navigo-Regular", size: 28) : UIFont.init(name: "Navigo-Regular", size: 10)
//        calendar.headerHeight = isIpad == true ? 100 : 50
        if isDaySelected{
            todayView.isHidden = true
            weekView.isHidden = false
            clView.isHidden = true
            
            self.segMentLbl2.textColor = .white
            self.segMentView2.backgroundColor = hexStringToUIColor(hex: "#0F9AF0")
            
            self.segMentLbl1.textColor = .gray
            self.segMentView1.backgroundColor = .white
            self.segMentLbl3.textColor = .gray
            self.segMentView3.backgroundColor = .white
        }else{
            weekView.isHidden = true
            clView.isHidden = true
            todayView.isHidden = false
        }
        
        
//        let titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
//        let titleTextFont = [NSAttributedString.Key.font: UIFont(name: "Navigo-Regular", size: isIpad == true ? 35 : 17)]
//        segment.setTitleTextAttributes(titleTextFont as [NSAttributedString.Key : Any], for: .normal)
//        segment.setTitleTextAttributes(titleTextAttributes, for: .selected)
        
//        calendar.m = Date()
        
    }
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        
        print("select date = \(date)")
        var getTitle = "Is this a new problem?"
        if concerns_Type == .Pain{
            getTitle = "Is this pain problem new?"
        }
        if concerns_Type == .Nausea{
            getTitle = "Is this nausea problem new?"
        }else if concerns_Type == .Breathing_Problems{
            getTitle = "Is this breathing issue a new problem?"
        }else if concerns_Type == .Fatigue{
            getTitle = "Is fatigue a new problem?"
        }else if concerns_Type == .Bowels{
            getTitle = "Is this bowel problem new?"
        }else if concerns_Type == .Bladder{
            getTitle = "Is this bladder problem new?"
        }
        
//        if concerns_Type == .Swallowing {
//            
//            if let Concerns_vc = SB_How_often.instantiateViewController(withIdentifier: "How_often") as? How_often{
//                       
//                Concerns_vc.concerns_Type = .Swallowing
//                self.navigationController?.pushViewController(Concerns_vc, animated: false)
//                
//                return
//                              
//            }
//        }
            
            
        gotToVC(title: getTitle.localized())
        
        
//        if let Concerns_vc = SB_Problem_Confirmation.instantiateViewController(withIdentifier: "Problem_Confirmation") as? Problem_Confirmation{
//
//            self.navigationController?.pushViewController(Concerns_vc, animated: false)
//
//        }
//
        if monthPosition == .previous || monthPosition == .next {
            calendar.setCurrentPage(date, animated: false)
        }
    }
    private func gotToVC(title: String = "Is this a new problem?".localized()){
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            if let Concerns_vc = SB_Problem_Confirmation.instantiateViewController(withIdentifier: "Problem_Confirmation") as? Problem_Confirmation{
                
                Concerns_vc.titleVC = title
                self.navigationController?.pushViewController(Concerns_vc, animated: false)

            }
        }
    }
    @IBAction func segMentBtnAction(_ sender: UIButton) {
        switch (sender.tag){
        case 80:
            self.segMentLbl1.textColor = .white
            self.segMentView1.backgroundColor = hexStringToUIColor(hex: "#0F9AF0")
            
            self.segMentLbl2.textColor = .gray
            self.segMentView2.backgroundColor = .white
            self.segMentLbl3.textColor = .gray
            self.segMentView3.backgroundColor = .white
            
            todayView.isHidden = false
            weekView.isHidden = true
            clView.isHidden = true
            
            if IS_MALE{
                playSound(fileName: SELECTED_LANG == "en" ? "Today_male_en" : "Hoy - today_male_Sp", file_Extension: "wav")
            }else{
                playSound(fileName: SELECTED_LANG == "en" ? "Today_Calender_en" : "hoy (today)_Calender_Sp", file_Extension: "mp3")
            }
        case 81:
            self.segMentLbl2.textColor = .white
            self.segMentView2.backgroundColor = hexStringToUIColor(hex: "#0F9AF0")
            
            self.segMentLbl1.textColor = .gray
            self.segMentView1.backgroundColor = .white
            self.segMentLbl3.textColor = .gray
            self.segMentView3.backgroundColor = .white
            
            todayView.isHidden = true
            weekView.isHidden = false
            clView.isHidden = true
            if IS_MALE{
                playSound(fileName: SELECTED_LANG == "en" ? "Week_male_en" : "Semana - week_male_Sp", file_Extension: "wav")
            }else{
                playSound(fileName: SELECTED_LANG == "en" ? "Week_Calender_en" : "semana (week)_Calender_Sp", file_Extension: "mp3")
            }
        case 82:
            self.segMentLbl3.textColor = .white
            self.segMentView3.backgroundColor = hexStringToUIColor(hex: "#0F9AF0")
            
            self.segMentLbl1.textColor = .gray
            self.segMentView1.backgroundColor = .white
            self.segMentLbl2.textColor = .gray
            self.segMentView2.backgroundColor = .white
            
            todayView.isHidden = true
            weekView.isHidden = true
            clView.isHidden = false
            if IS_MALE{
                playSound(fileName: SELECTED_LANG == "en" ? "Month_male_en" : "Mes - month_male_Sp", file_Extension: "wav")
            }else{
                playSound(fileName: SELECTED_LANG == "en" ? "Month_Calender_en" : "Mes (month)_Calender_Sp", file_Extension: "mp3")
            }
        default:
            print("")
        }
    }
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    @IBAction func didTapOnRightClick(_ sender: UIButton) {
        switch (sender.tag)
        {
        case 2,22:
            highlightRightTick(enableTick: todayRightTickImg1, disableTick: [todayRightTickImg2,todayRightTickImg3])
            if IS_MALE{
                playSound(fileName: SELECTED_LANG == "en" ? "Morning" : "Mañana - morning_male_Sp", file_Extension: "wav")
            }else{
                playSound(fileName: SELECTED_LANG == "en" ? "Morning_Calender_en" : "manana (morning)_Calender_Sp", file_Extension: "mp3")
            }
            gotToVC()
        case 3,23:
            highlightRightTick(enableTick: todayRightTickImg2, disableTick: [todayRightTickImg1,todayRightTickImg3])
            if IS_MALE{
                playSound(fileName: SELECTED_LANG == "en" ? "Afternoon_male_en" : "Tarde - afternoon_male_Sp", file_Extension: "wav")
            }else{
                playSound(fileName: SELECTED_LANG == "en" ? "afternoon_Calender_en" : "tarde (afternoon)_Calender_Sp", file_Extension: "mp3")
            }
            gotToVC()
        case 4,24:
            highlightRightTick(enableTick: todayRightTickImg3, disableTick: [todayRightTickImg1,todayRightTickImg2])
            if IS_MALE{
                playSound(fileName: SELECTED_LANG == "en" ? "Evening" : "Noche Temprano - evening_male_Sp", file_Extension: "wav")
            }else{
                playSound(fileName: SELECTED_LANG == "en" ? "Evening_Calender_en" : "noche temprano (evening - early evening)_Calender_Sp", file_Extension: "mp3")
            }
            gotToVC()
        case 5,55:
            highlightRightTick(enableTick: weekRightTickImg1, disableTick: [weekRightTickImg2,weekRightTickImg3,weekRightTickImg4,weekRightTickImg5,weekRightTickImg6,weekRightTickImg7])
            if IS_MALE{
                playSound(fileName: SELECTED_LANG == "en" ? "Sunday_male_en" : "Domingo_male_Sp", file_Extension: "wav")
            }else{
                playSound(fileName: SELECTED_LANG == "en" ? "Sunday_Calender_en" : "Domingo (Sunday)_Calender_Sp", file_Extension: "mp3")
            }
            gotToVC()
        case 6,56:
            highlightRightTick(enableTick: weekRightTickImg2, disableTick: [weekRightTickImg1,weekRightTickImg3,weekRightTickImg4,weekRightTickImg5,weekRightTickImg6,weekRightTickImg7])
            if IS_MALE{
                playSound(fileName: SELECTED_LANG == "en" ? "Monday_male_en" : "Lunes_male_Sp", file_Extension: "wav")
            }else{
                playSound(fileName: SELECTED_LANG == "en" ? "Monday_Calender_en" : "Lunes (monday)_Calender_Sp", file_Extension: "mp3")
            }
            gotToVC()
        case 7,57:
            highlightRightTick(enableTick: weekRightTickImg3, disableTick: [weekRightTickImg1,weekRightTickImg2,weekRightTickImg4,weekRightTickImg5,weekRightTickImg6,weekRightTickImg7])
            if IS_MALE{
                playSound(fileName: SELECTED_LANG == "en" ? "Tuesday_male_en" : "Martes_male_Sp", file_Extension: "wav")
            }else{
                playSound(fileName: SELECTED_LANG == "en" ? "Tuesday_Calender_en" : "Martes (Tuesday)_Calender_Sp", file_Extension: "mp3")
            }
            gotToVC()
        case 8,58:
            highlightRightTick(enableTick: weekRightTickImg4, disableTick: [weekRightTickImg1,weekRightTickImg2,weekRightTickImg3,weekRightTickImg5,weekRightTickImg6,weekRightTickImg7])
            if IS_MALE{
                playSound(fileName: SELECTED_LANG == "en" ? "Wednesday_male_en" : "Miercoles_male_Sp", file_Extension: "wav")
            }else{
                playSound(fileName: SELECTED_LANG == "en" ? "Wednesday_Calender_en" : "Miercoles(Wednesday)_Calender_Sp", file_Extension: "mp3")
            }
            gotToVC()
        case 9,59:
            highlightRightTick(enableTick: weekRightTickImg5, disableTick: [weekRightTickImg1,weekRightTickImg2,weekRightTickImg3,weekRightTickImg4,weekRightTickImg6,weekRightTickImg7])
            if IS_MALE{
                playSound(fileName: SELECTED_LANG == "en" ? "Thursday_male_en" : "Jueves_male_Sp", file_Extension: "wav")
            }else{
                playSound(fileName: SELECTED_LANG == "en" ? "Thursday_Calender_en" : "Jueves (Thursday)_Calender_Sp", file_Extension: "mp3")
            }
            gotToVC()
        case 10,60:
            highlightRightTick(enableTick: weekRightTickImg6, disableTick: [weekRightTickImg1,weekRightTickImg2,weekRightTickImg3,weekRightTickImg4,weekRightTickImg5,weekRightTickImg7])
            if IS_MALE{
                playSound(fileName: SELECTED_LANG == "en" ? "Friday_male_en" : "Viernes_male_Sp", file_Extension: "wav")
            }else{
                playSound(fileName: SELECTED_LANG == "en" ? "Friday_Calender_en" : "Viernes (Friday)_Calender_Sp", file_Extension: "mp3")
            }
            gotToVC()
        case 11,61:
            highlightRightTick(enableTick: weekRightTickImg7, disableTick: [weekRightTickImg1,weekRightTickImg2,weekRightTickImg3,weekRightTickImg4,weekRightTickImg5,weekRightTickImg6])
            if IS_MALE{
                playSound(fileName: SELECTED_LANG == "en" ? "Saturday_male_en" : "Sabado_male_Sp", file_Extension: "wav")
            }else{
                playSound(fileName: SELECTED_LANG == "en" ? "Saturday_Calender_en" : "Sabado (Saturday)_Calender_Sp", file_Extension: "mp3")
            }
            gotToVC()
        default:
            print("")
        }
    }

    private func highlightRightTick(enableTick: UIImageView,disableTick: [UIImageView]){
        enableTick.image = UIImage(named: "checked")
        for i in 0 ... disableTick.count - 1 {
            disableTick[i].image = UIImage(named: "")
        }
    }
}



class OYSegmentControl: UISegmentedControl {
  
  override func layoutSubviews(){
    super.layoutSubviews()
    
    let segmentStringSelected: [NSAttributedString.Key : Any] = [
      NSAttributedString.Key.font : UIFont(name: "Navigo-Regular", size: isIpad == true ? 23 : 9),
      NSAttributedString.Key.foregroundColor : UIColor.white
    ]
    
    let segmentStringHighlited: [NSAttributedString.Key : Any] = [
      NSAttributedString.Key.font : UIFont(name: "Navigo-Regular", size: isIpad == true ? 23 : 9),
      NSAttributedString.Key.foregroundColor : UIColor.black
    ]
    
    setTitleTextAttributes(segmentStringHighlited, for: .normal)
    setTitleTextAttributes(segmentStringSelected, for: .selected)
    setTitleTextAttributes(segmentStringHighlited, for: .highlighted)
    
    layer.masksToBounds = true
      
      setTitle("Today".localized(), forSegmentAt: 0)
      setTitle("Week".localized(), forSegmentAt: 1)
      setTitle("Months".localized(), forSegmentAt: 2)
    
    if #available(iOS 13.0, *) {
      selectedSegmentTintColor = hexStringToUIColor(hex: "#0F9AF0")
    } else {
      tintColor = hexStringToUIColor(hex: "#0F9AF0")
    }
    
      backgroundColor = .white
    
    //corner radius
    let cornerRadius = bounds.height / 2
    let maskedCorners: CACornerMask = [.layerMinXMinYCorner, .layerMinXMaxYCorner, .layerMaxXMinYCorner, .layerMaxXMaxYCorner]
    //background
    clipsToBounds = true
    layer.cornerRadius = cornerRadius
    layer.maskedCorners = maskedCorners

    let foregroundIndex = numberOfSegments
    if subviews.indices.contains(foregroundIndex),
      let foregroundImageView = subviews[foregroundIndex] as? UIImageView {
      foregroundImageView.image = UIImage()
      foregroundImageView.clipsToBounds = true
      foregroundImageView.layer.masksToBounds = true
      foregroundImageView.backgroundColor = hexStringToUIColor(hex: "#0F9AF0")
      
      foregroundImageView.layer.cornerRadius = bounds.height / 2 + 5
      foregroundImageView.layer.maskedCorners = maskedCorners
    }
//      dropShadow()
  }
  
  override func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
    return false
  }
  
}
extension NSLayoutConstraint {

    override public var description: String {
        let id = identifier ?? ""
        return "id: \(id), constant: \(constant)" //you may print whatever you want here
    }
}
