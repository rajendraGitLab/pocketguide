//
//  JustChooseVC.swift
//  Pocket guide Communication
//
//  Created by Admin on 31/08/22.
//
import UIKit

class JustChooseVC: UIViewController {

    //MARK: - Outlets
    
    @IBOutlet weak var bgView: UIView!
    
    @IBOutlet weak var Title_lbl: UILabel!
    @IBOutlet weak var choose_lbl: UILabel!
    @IBOutlet weak var confirm_lbl: UILabel!
    @IBOutlet weak var repeat_lbl: UILabel!
    @IBOutlet weak var done_lbl: UILabel!
    
    @IBOutlet weak var choose_btn: UIButton!
    @IBOutlet weak var confirm_btn: UIButton!
    @IBOutlet weak var repeat_btn: UIButton!
    @IBOutlet weak var done_btn: UIButton!
    
    @IBOutlet weak var next_btn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
    }
    //MARK: - Funcitons
    
    func setUpUI(){
//        bgView.applyGradient(isVertical: false, colorArray: [UIColor.white,UIColor(red: 219/255, green: 233/255, blue: 247/255, alpha: 1)])
//        next_btn.titleLabel?.font = UIFont(name: "Navigo-Regular", size: 40)
    }
    //MARK: - IBAction
    
    @IBAction func chooseAction(_ sender: UIButton){
        
    }
    @IBAction func confirmAction(_ sender: UIButton){
        
    }
    @IBAction func repeatAction(_ sender: UIButton){
        
    }
    @IBAction func doneAction(_ sender: UIButton){
        
    }
    
    @IBAction func btnNextAction(_ sender: UIButton) {
        
        if let vc = SB_Settings.instantiateViewController(withIdentifier: "Settings") as? Settings {
            self.navigationController?.pushViewController(vc, animated: false)
        }
    }
}
