//
//  SignInVC.swift
//  Pocket guide Communication
//
//  Created by Admin on 15/11/22.
//

import UIKit
import AuthenticationServices
import Alamofire

class SignInVC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userDefault.setValue(true, forKey: "isNew")
        isNew = true
    }
    @IBAction func appleLogin_Action(_ sender: UIButton) {

        if #available(iOS 13.0, *) {
            let appleIDProvider = ASAuthorizationAppleIDProvider()
            let request = appleIDProvider.createRequest()
            request.requestedScopes = [.fullName, .email]
            let authorizationController = ASAuthorizationController(authorizationRequests: [request])
            authorizationController.delegate = self
            authorizationController.presentationContextProvider = self
            authorizationController.performRequests()
        } else {
//             Fallback on earlier versions
        }
    }
}
@available(iOS 13.0, *)
extension SignInVC: ASAuthorizationControllerDelegate {

     // ASAuthorizationControllerDelegate function for authorization failed

    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        print(error.localizedDescription)
    }

       // ASAuthorizationControllerDelegate function for successful authorization

    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {

        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {

            // Create an account as per your requirement
            let appleId = appleIDCredential.user
            let appleUserFirstName = appleIDCredential.fullName?.givenName
            let appleUserLastName = appleIDCredential.fullName?.familyName
            let appleUserEmail = appleIDCredential.email
            UserDefaults.standard.setValue(appleId, forKey: "RappleId")
            if appleUserFirstName != nil {
                let fullName = "\(appleUserFirstName!) \(appleUserLastName!)"
                UserDefaults.standard.setValue(appleUserEmail, forKey: "RappleEmail")
                UserDefaults.standard.setValue(fullName, forKey: "RappleName")
                UserDefaults.standard.setValue(appleId, forKey: "RappleId")
            }
            
            let email = UserDefaults.standard.value(forKey: "RappleEmail") as? String ?? ""
            let name = UserDefaults.standard.value(forKey: "RappleName") as? String ?? ""
            let appleId1 = UserDefaults.standard.value(forKey: "RappleId") as? String ?? ""
            print("Apple Login ID - ",appleId)
            print("Apple Login Email - ",email)
            print("Apple Login Name - ",name)
            
            APPLE_NAME = name
            APPLE_ID = appleId
            APPLE_EMAIL = email
            LoginAPI()
//            self.socialLogIn(provider: "apple", providerId: appleId, email: email, userName: name, userImage: "")
            
        } else if let passwordCredential = authorization.credential as? ASPasswordCredential {
        }
    }
    
    func LoginAPI(){
        
        if !NetworkUtils.isNetworkReachable() {
            Toastnew.show(message: "No internet connection", controller: self)
            return
        }
        let parm = ["user_id" : APPLE_ID,"email": APPLE_EMAIL]
        Util.showHud(view: self.view)
        print("parm : \(parm)")
        AF.request(signUp, method: .post, parameters: parm , encoding: URLEncoding.httpBody,headers: nil).responseJSON {
                        response in
                        Util.hideHud(view: self.view)
                        switch(response.result) {
                        case .success(_):
                            if let data = response.value
                            {
                                DispatchQueue.main.async {
                                    print(data)
                                    if let getDataresponce = data as? NSDictionary{
                                            userDefault.setValue(true, forKey: "isAppleLogin")
//                                        if let vc = SB_Main.instantiateViewController(withIdentifier: "SubscriptionVC") as? SubscriptionVC{
//                                            self.navigationController?.pushViewController(vc, animated: false)
//                                        }
                                        self.methodMakeRootTabbar(index: 1)
                                        comeFromHowRuVC = true
                                           
                                        
                                    }else{
                                    }
                                }
                            }
                            break
                        case .failure(_):
                            if response.error != nil
                            {
                                let statusCode = response.response?.statusCode ?? 0
                            }
                            break
                        }
        
                    }
    }
}

@available(iOS 13.0, *)
extension SignInVC: ASAuthorizationControllerPresentationContextProviding {

    //For present window

    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {

        return self.view.window!
        
       
    }

}
