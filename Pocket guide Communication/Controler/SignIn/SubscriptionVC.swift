//
//  SubscriptionVC.swift
//  Pocket guide Communication
//
//  Created by Admin on 15/11/22.
//

import UIKit
import SwiftyStoreKit
import Alamofire

class SubscriptionVC: UIViewController {

    var timer: Timer!
    var nextDate: Date!
    var isExpired = false
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnActiveNow: UIButton!
    @IBOutlet weak var lblActiveNow: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var activeNowView: UIView!
    @IBOutlet weak var lblPrice: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if SUBSCRIPTION_STATUS{
            self.btnMenu.isHidden = false
            self.btnNext.isHidden = true
        }else{
            self.btnNext.isHidden = true
            self.btnMenu.isHidden = true
        }
       
//        let now = Date()
//        let calendar = Calendar.current
//        let components = DateComponents(calendar: calendar,timeZone: <#T##TimeZone?#>)  // <- 17:00 = 5pm
//        nextDate = calendar.nextDate(after: now, matching: components, matchingPolicy: .nextTime)!
//        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(UpdateTime), userInfo: nil, repeats: true)
        getSubscriptionStatus()
    }
    
    @objc func UpdateTime() {
           let diff = Calendar.current.dateComponents([.hour, .minute, .second], from: Date(), to: nextDate)
           print(diff)
       }
    @IBAction func btnNextAction(_ sender: UIButton){
        if isNew{
            self.methodMakeRootTabbar(index: 1)
            comeFromHowRuVC = true
        }else{
            self.gotoRootHome()
        }
    }
    @IBAction func btnMenuAction(_ sender: UIButton){
        Util.showSideMenu(navC: self)
    }
    @IBAction func buyMembership(_ sender: UIButton){
        purchase()
    }
    private func gotoRootHome(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            if let vc = SB_Fill_Details.instantiateViewController(withIdentifier: "Fill_Details") as? Fill_Details {
                let nav = UINavigationController(rootViewController: vc)
                nav.interactivePopGestureRecognizer?.delegate = nil
                nav.setNavigationBarHidden(true, animated: false)
                self.window?.rootViewController = nav
                self.window?.makeKeyAndVisible()
            }
        }
    }
}
extension SubscriptionVC{
    
    //MARK: Product Information Get
    func productInformation() {
        DispatchQueue.main.async {
            Util.showHud(view: self.view)
        }
        SwiftyStoreKit.retrieveProductsInfo(["MedProductID"]) { result in
            if let product = result.retrievedProducts.first {
                let priceString = product.localizedPrice!
                print("Product: \(product.localizedDescription), price: \(priceString)")
            }
            else if let invalidProductId = result.invalidProductIDs.first {
                print("Invalid product identifier: \(invalidProductId)")
            }
            else {
                print("Error: \(result.error)")
            }
        }
    }
    
    //MARK: Product Purchased function
    func purchase() {
        DispatchQueue.main.async {
            Util.showHud(view: self.view)
        }
        let productId = "MedProductID"
        SwiftyStoreKit.purchaseProduct("\(productId)", atomically: true) { result in
            print("result-\(result)-")
            if case .success(let purchase) = result {
                switch purchase.transaction.transactionState {
                case .purchased:
                    let downloads = purchase.transaction.downloads
                    print("51HCG-\(purchase.transaction.transactionState.debugDescription): \(purchase.productId)")
                    if !downloads.isEmpty {
                        print("51HCG-\(purchase.transaction.transactionState.debugDescription): \(purchase.productId)")
                        SwiftyStoreKit.start(downloads)
                        print("call api")
                        
                    } else if purchase.needsFinishTransaction {
                        print("52HCG-\(purchase.transaction.transactionState.debugDescription): \(purchase.productId)")
                        SwiftyStoreKit.finishTransaction(purchase.transaction)
                        print("call api1")
                    } else {
                        let transactionIdentifier = purchase.transaction.transactionIdentifier ?? ""
                        print(transactionIdentifier)
                        let transactionDate = purchase.transaction.transactionDate?.timeIntervalSinceReferenceDate
                        print(transactionDate)
                        print("53HCG-\(purchase.transaction.transactionState.debugDescription): \(purchase.productId)")
                        print("call api2")
                    }
                    if purchase.originalTransaction?.transactionIdentifier ?? "" == ""{
                        self.updateStatusAtBackendAPI(transactionID: purchase.transaction.transactionIdentifier ?? "")
                    }else{
                        self.updateStatusAtBackendAPI(transactionID: purchase.originalTransaction?.transactionIdentifier ?? "")
                    }
                    UserDefaults.standard.setValue(true, forKey: "SUBSCRIPTION_STATUS")
                    self.btnActiveNow.isUserInteractionEnabled = false
                    self.lblActiveNow.text = "ACTIVATED"
                    self.btnNext.isHidden = false
                    Util.hideHud(view: self.view)
                case .restored:
                    let downloads = purchase.transaction.downloads
                    if !downloads.isEmpty {
                        print("511HCG-\(purchase.transaction.transactionState.debugDescription): \(purchase.productId)")
                        SwiftyStoreKit.start(downloads)
                    } else if purchase.needsFinishTransaction {
                        print("521HCG-\(purchase.transaction.transactionState.debugDescription): \(purchase.productId)")
                        SwiftyStoreKit.finishTransaction(purchase.transaction)
                    } else {
                        print("531HCG-\(purchase.transaction.transactionState.debugDescription): \(purchase.productId)")
                    }
                    self.dismiss(animated: true) {
                        Util.hideHud(view: self.view)
                    }
                case .failed, .purchasing, .deferred:
                    print("Cancel")
                    self.dismiss(animated: true) {
                        Util.hideHud(view: self.view)
                    }
                    break
                @unknown default:
                    print("Cancel")
                    self.dismiss(animated: true) {
                        Util.hideHud(view: self.view)
                    }
                    break
                }
                let downloads = purchase.transaction.downloads
                if !downloads.isEmpty {
                    SwiftyStoreKit.start(downloads)
                }
                if purchase.needsFinishTransaction {
                    SwiftyStoreKit.finishTransaction(purchase.transaction)
                }
            }else{
                print("Cancel")
                self.dismiss(animated: true) {
                    Util.hideHud(view: self.view)
                }
            }
        }
    }
    func getSubscriptionStatus(){
        
        if !NetworkUtils.isNetworkReachable() {
            Toastnew.show(message: "No internet connection", controller: self)
            return
        }
        let parm = ["user_id" : APPLE_ID]
        Util.showHud(view: self.view)
        print("parm : \(parm)")
        AF.request(getTransactionIAP, method: .post, parameters: parm , encoding: URLEncoding.httpBody,headers: nil).responseJSON {
                        response in 
                        Util.hideHud(view: self.view)
                        switch(response.result) {
                        case .success(_):
                            if let data = response.value
                            {
                                DispatchQueue.main.async {
                                    print(data)
                                    if let getDataresponse = data as? NSDictionary{
                                        let data = getDataresponse.value(forKey: "data") as? NSArray
                                        let inAppPurchaseStatus = data?.value(forKey: "status") as? NSArray
                                        SUBSCRIPTION_STATUS = inAppPurchaseStatus?.firstObject as? Bool ?? false
                                        if SUBSCRIPTION_STATUS {
                                            UserDefaults.standard.setValue(true, forKey: "SUBSCRIPTION_STATUS")
                                            self.btnActiveNow.isUserInteractionEnabled = false
                                            self.lblActiveNow.text = "ACTIVATED"
                                            if comefromSideMenu{
                                                self.btnNext.isHidden = true
                                                self.btnMenu.isHidden = false
                                            }else{
                                                self.btnNext.isHidden = false
                                                self.btnMenu.isHidden = true
                                            }
                                            
                                        }else{
                                            UserDefaults.standard.setValue(false, forKey: "SUBSCRIPTION_STATUS")
                                            self.btnActiveNow.isUserInteractionEnabled = true
                                            self.lblActiveNow.text = "Active Now"
                                            self.btnNext.isHidden = true
                                            if data?.count == 0{
                                                self.activeNowView.backgroundColor = hexStringToUIColor(hex: "0F9AF0")
                                                self.lblTitle.text = "Yearly Subscription"
                                                self.lblTitle.textColor = .black
                                            }else{
                                                self.activeNowView.backgroundColor = .red
                                                self.lblTitle.text = "Your Yearly Subscription is Expired"
                                                self.lblTitle.textColor = .red
                                            }
                                        }
                                        
                                    }else{
                                    }
                                }
                            }
                            break
                        case .failure(_):
                            if response.error != nil
                            {
                                let statusCode = response.response?.statusCode ?? 0
                            }
                            break
                        }
        }
    }
    func updateStatusAtBackendAPI(transactionID: String){
        
        if !NetworkUtils.isNetworkReachable() {
            Toastnew.show(message: "No internet connection", controller: self)
            return
        }
        let parm = ["user_id" : APPLE_ID,"txn_id": transactionID,"txn_date": "2021-10-12","email": APPLE_EMAIL,"txn_name": "1 year"]
        Util.showHud(view: self.view)
        print("parm : \(parm)")
        AF.request(backendIAP, method: .post, parameters: parm , encoding: URLEncoding.httpBody,headers: nil).responseJSON {
                        response in
                        Util.hideHud(view: self.view)
                        switch(response.result) {
                        case .success(_):
                            if let data = response.value
                            {
                                DispatchQueue.main.async {
                                    print(data)
                                    if let getDataresponce = data as? NSDictionary{
                                    }else{
                                    }
                                }
                            }
                            break
                        case .failure(_):
                            if response.error != nil
                            {
                                let statusCode = response.response?.statusCode ?? 0
                            }
                            break
                        }
        
                    }
    }
}
