//
//  AphasiaVC.swift
//  Pocket guide Communication
//
//  Created by Admin on 18/10/22.
//

import UIKit

class AphasiaVC: UIViewController,UIScrollViewDelegate {

    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var menuBtn: UIButton!
    
    @IBOutlet weak var img1: UIImageView!
    @IBOutlet weak var img2: UIImageView!
    @IBOutlet weak var img3: UIImageView!
    @IBOutlet weak var img4: UIImageView!
    
    @IBOutlet weak var sclView: UIScrollView!
    @IBOutlet weak var zView: UIView!
    
    @IBOutlet weak var zoomView : ZoomableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        sclView.minimumZoomScale = 1
        sclView.maximumZoomScale = 10.0
        sclView.delegate = self
//        zoomView.sourceView = sclView
//        zoomView.isZoomable = true
        if SELECTED_LANG == "en" {
            img1.image = UIImage(named: "Aphasia1")
            img2.image = UIImage(named: "Aphasia2")
            img3.image = UIImage(named: "Aphasia3")
            img4.image = UIImage(named: "Aphasia4")
        }else{
            img1.image = UIImage(named: "Aphasia1_Sp")
            img2.image = UIImage(named: "Aphasia2_Sp")
            img3.image = UIImage(named: "Aphasia3_Sp")
            img4.image = UIImage(named: "Aphasia4_Sp")
        }
    }
    @IBAction func btnBackAction(_ sender: UIButton) {
        Util.showSideMenu(navC: self)
    }
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
            return zView
        }
    @IBAction func btnNextAction(_ sender: UIButton) {
        
        if let vc = SB_Tips.instantiateViewController(withIdentifier: "ApraxiaVC") as? ApraxiaVC{
            self.navigationController?.pushViewController(vc, animated: false)
        }
    }

}
