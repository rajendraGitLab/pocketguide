//
//  Tips.swift
//  Pocket guide Communication
//
//  Created by Deepak on 29/10/21.
//

import UIKit

class Tips: UIViewController {
    
    @IBOutlet weak var lbl1: UILabel!
    @IBOutlet weak var lbl2: UILabel!
    @IBOutlet weak var lbl3: UILabel!
    @IBOutlet weak var lbl4: UILabel!
    @IBOutlet weak var lbl5: UILabel!
    @IBOutlet weak var lbl6: UILabel!
    
    
    @IBOutlet weak var btnback: UIButton!
    @IBOutlet weak var btnNext: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        
//        btnNext.isHidden = !isNew
        
        let string1 = "Tag the end of your question with a yes/no gesture to promote comprehension for PWA, for example, “Do you have pain?” “yes?” (nod head for yes) “or no?” (shake head for no)."
        let attrStri = NSMutableAttributedString.init(string:string1)
        let nsRange = NSString(string: string1).range(of: "Tag", options: String.CompareOptions.caseInsensitive)
        attrStri.addAttributes([NSAttributedString.Key.font: UIFont.init(name: "Navigo-Bold", size: 15.0) as Any], range: nsRange)
        self.lbl1.attributedText = attrStri
        
        let string2 = "Use slow and simplified statements and questions."
        let attrStri2 = NSMutableAttributedString.init(string:string2)
        let nsRange2 = NSString(string: string2).range(of: "slow")
        let nsRange21 = NSString(string: string2).range(of: "simplified")
        attrStri2.addAttributes([NSAttributedString.Key.font: UIFont.init(name: "Navigo-Bold", size: 15.0) as Any], range: nsRange2)
        attrStri2.addAttributes([NSAttributedString.Key.font: UIFont.init(name: "Navigo-Bold", size: 15.0) as Any], range: nsRange21)
        self.lbl2.attributedText = attrStri2
        
        let string3 = "Incorporate facial expressions, gestures, and body language."
        let attrStri3 = NSMutableAttributedString.init(string:string3)
        let nsRange3 = NSString(string: string3).range(of: "facial expressions, gestures,")
        let nsRange31 = NSString(string: string3).range(of: "body language.")
        attrStri3.addAttributes([NSAttributedString.Key.font: UIFont.init(name: "Navigo-Bold", size: 15.0) as Any], range: nsRange3)
        attrStri3.addAttributes([NSAttributedString.Key.font: UIFont.init(name: "Navigo-Bold", size: 15.0) as Any], range: nsRange31)
        self.lbl3.attributedText = attrStri3
        
        let string4 = "Wait for a response."
        let attrStri4 = NSMutableAttributedString.init(string:string4)
        let nsRange4 = NSString(string: string4).range(of: "Wait")
        attrStri4.addAttributes([NSAttributedString.Key.font: UIFont.init(name: "Navigo-Bold", size: 15.0) as Any], range: nsRange4)
        self.lbl4.attributedText = attrStri4
        
        let string5 = "Point to each multiple choice option as you read it."
        let attrStri5 = NSMutableAttributedString.init(string:string5)
        let nsRange5 = NSString(string: string5).range(of: "Point")
        attrStri5.addAttributes([NSAttributedString.Key.font: UIFont.init(name: "Navigo-Bold", size: 15.0) as Any], range: nsRange5)
        self.lbl5.attributedText = attrStri5
        
        let string6 = "Remember that yes/no may not be reliable. Pay attention to facial expressions and body language."
        let attrStri6 = NSMutableAttributedString.init(string:string6)
        let nsRange6 = NSString(string: string6).range(of: "may not be reliable.")
        attrStri6.addAttributes([NSAttributedString.Key.font: UIFont.init(name: "Navigo-Bold", size: 15.0) as Any], range: nsRange6)
        self.lbl6.attributedText = attrStri6
        
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func btnNextAction(_ sender: UIButton) {
        
        if let vc = SB_Yes_no_clarify.instantiateViewController(withIdentifier: "Yes_no_clarify") as? Yes_no_clarify {
            self.navigationController?.pushViewController(vc, animated: false)
        }
    }
}
