//
//  Tab_bar.swift
//  Pocket guide Communication
//
//  Created by Deepak on 28/10/21.
//

import UIKit

var TabbarObj  : Tab_bar!
var hasTopNotch: Bool {
    if #available(iOS 11.0, tvOS 11.0, *) {
        return UIApplication.shared.windows[0].safeAreaInsets.left > 20
    }
    return false
}

class Tab_bar: UITabBarController {

    @IBInspectable var height: CGFloat = 0.0

    override func viewDidLoad() {
        super.viewDidLoad()
        TabbarObj = self
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    func setUpTabBarUI(){
//        var tabFrame            = tabBar.frame
//        tabFrame.size.height    = isIpad ? 120 : 55
//        tabFrame.origin.y       = view.frame.size.height - (isIpad ? 120 : 55)
//        tabBar.frame            = tabFrame
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        let aarayImages = [SELECTED_LANG == "en" ? "idea" : "idea_Sp","house","yes-no"]
        if let count = self.tabBar.items?.count {
                for i in 0...(count-1) {
                    let image: UIImage? = UIImage(named:aarayImages[i])?.withRenderingMode(.alwaysOriginal)
                    self.tabBar.items?[i].image = image
                    let templateImage = image?.withRenderingMode(.alwaysOriginal)
                    let img = UIImageView(image: templateImage)
                    img.tintColor = .black
                    self.tabBar.items?[i].selectedImage = img.image
            }
        }
    }
    
    override func viewWillLayoutSubviews() {
        
        let appearance = UITabBarItem.appearance()
        let attributes = [NSAttributedString.Key.font:UIFont(name: "Navigo-Regular", size: isIpad ? 22 : 11)]
        appearance.setTitleTextAttributes(attributes as [NSAttributedString.Key : Any], for: .normal)
        appearance.titlePositionAdjustment.vertical = isIpad ? -18 : (UIDevice.current.hasNotch ? 7 : -3)
        
//        let items = tabBar.items
//        items?[0].title = "Yes/No".localized()
//        items?[1].title = "Home".localized()
//        items?[2].title = "Whiteboard".localized()
       
        if #available(iOS 13.0, *) {
            let appearance1 = UITabBarAppearance()
            appearance1.configureWithOpaqueBackground()
            appearance1.backgroundColor = hexStringToUIColor(hex: "#DBE9F7")
            tabBar.standardAppearance = appearance1
        } else {
            // Fallback on earlier versions
            UITabBar.appearance().isTranslucent = false
            UITabBar.appearance().backgroundColor = .red
            UITabBar.appearance().tintColor = .red
            let Tcontroller = self.window?.rootViewController as? UITabBarController
            Tcontroller?.tabBar.backgroundColor = UIColor.red // your color

        }
    }
}

//extension UITabBar {
//    override open func sizeThatFits(_ size: CGSize) -> CGSize {
//        var sizeThatFits = super.sizeThatFits(size)
//        sizeThatFits.height = isIpad ? 120 : 55 // adjust your size here
//        return sizeThatFits
//    }
//}
class CustomTabBar: UITabBar {

    override func sizeThatFits(_ size: CGSize) -> CGSize {
          var size = super.sizeThatFits(size)

        size.height = isIpad ? 90 : (UIDevice.current.hasNotch ? 60 : 42)
          return size
     }
}

extension UIDevice {
    /// Returns `true` if the device has a notch
    var hasNotch: Bool {
        guard #available(iOS 11.0, *), let window = UIApplication.shared.windows.filter({$0.isKeyWindow}).first else { return false }
        if UIDevice.current.orientation.isPortrait {
            return window.safeAreaInsets.top >= 44
        } else {
            return window.safeAreaInsets.left > 0 || window.safeAreaInsets.right > 0
        }
    }
}
