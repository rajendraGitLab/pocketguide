//
//  Swallowing_More.swift
//  Pocket guide Communication
//
//  Created by Deepak on 12/11/21.
//

import UIKit

class Swallowing_More: UIViewController,UIScrollViewDelegate {
    
    
    var swallowing_ProblemsList = [(UIImage(named: "With Food Swallowing"),"With Food".localized()),
                                   (UIImage(named: "With Drinks"),"With Drinks".localized()),
//                                   (UIImage(named: "With Pills"),"With Pills".localized()),
                                   (UIImage(named: "With Medication"),"With Medication".localized()),
                                   (UIImage(named: "with_food_and_drinks"),"With Food and Drinks".localized())]
    
    @IBOutlet weak var btnMenuOutlet: UIButton!
    @IBOutlet weak var btnBackOutlet: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var indicatorView: UIView!
    @IBOutlet weak var indicatorViewTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var zoomView : ZoomableView!
    @IBOutlet weak var sclView : UIScrollView!
    
    fileprivate (set) var scrollBar : VerticalScrollBar!
    var comeFromBreathingProblems = false
    var currentCellFrame = Int()
    var concerns_Type: Concerns_Type = .All_Concerns
    var selectIndex = -1
    var isZooming = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sclView.minimumZoomScale = 1
        sclView.maximumZoomScale = 10.0
        sclView.delegate = self
//        zoomView.sourceView = collectionView
//        zoomView.isZoomable = true
        lblTitle.text = "Swallowing".localized()
//        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
//            layout.scrollDirection = isIpad == true ? .vertical : .horizontal
//        }
        if comeFromBreathingProblems{
            swallowing_ProblemsList = [(UIImage(named: "With Food Swallowing"),"With Food".localized()),
                                           (UIImage(named: "With Drinks"),"With Drinks".localized()),
                                           (UIImage(named: "With Pills"),"With Pills".localized()),
//                                           (UIImage(named: "With Medication"),"With Medication".localized()),
                                           (UIImage(named: "with_food_and_drinks"),"With Food and Drinks".localized())]
        }else{
            swallowing_ProblemsList = [(UIImage(named: "With Food Swallowing"),"With Food".localized()),
                                           (UIImage(named: "With Drinks"),"With Drinks".localized()),
        //                                   (UIImage(named: "With Pills"),"With Pills".localized()),
                                           (UIImage(named: "With Medication"),"With Medication".localized()),
                                           (UIImage(named: "with_food_and_drinks"),"With Food and Drinks".localized())]
        }
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
        self.scrollBar = VerticalScrollBar(frame: CGRect.zero, targetScrollView: self.collectionView)
                self.view.addSubview(self.scrollBar!)
        
        
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.scrollBar.frame = CGRect(x: UIScreen.main.bounds.width - (isIpad ? 40 : 20) - (UIApplication.shared.windows[0].safeAreaInsets.left), y: self.view.frame.height - self.collectionView.frame.height - (isIpad ? 90 : (UIDevice.current.hasNotch ? 60 : 42)) - (isIpad ? 40 : 0), width: self.indicatorView.frame.width * 1.6, height: self.collectionView.frame.height)
        self.scrollBar.backgroundColor = .clear
    }
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        isZooming = true
    }
    func scrollViewDidEndZooming(_ scrollView: UIScrollView, with view: UIView?, atScale scale: CGFloat) {
        isZooming = false
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if !isZooming{
            let totalScroll = scrollView.contentSize.height - self.collectionView.frame.height
            let ratio = totalScroll/(self.collectionView.frame.height - self.indicatorView.frame.height)
            self.indicatorViewTopConstraint.constant = (scrollView.contentOffset.y)/(ratio)
        }
        
    }
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
            return zoomView
        }
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }

}

extension Swallowing_More : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return swallowing_ProblemsList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Health_type_Cell", for: indexPath) as! Health_type_Cell
        
        let getData = swallowing_ProblemsList
        cell.image.image = getData[indexPath.row].0
        cell.title.text = getData[indexPath.row].1
        cell.title.font = iconTitleFont
        cell.image.contentMode = .scaleAspectFit
        if selectIndex != -1{
            if indexPath.row == selectIndex{
                cell.bgView.borderColor =  hexStringToUIColor(hex: "#0F9AF0")
                cell.bgView.borderWidth = isIpad ? 8 : 4
            }else{
                cell.bgView.borderColor = .clear
            }
        }else{
            cell.bgView.borderColor = .clear
        }
        
        cell.bgView.dropShadow()
        
            return cell
        }
       
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let collectionHeight = collectionView.bounds.height

        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
//        flowLayout.collectionView?.isPagingEnabled = true
        let totalSpace = flowLayout.sectionInset.left
        + flowLayout.sectionInset.right
        + ( (noOfCellsInRow == 2 ? (isIpad ? collectionView.frame.height/5 : collectionView.frame.height/1.5) : flowLayout.minimumLineSpacing) * CGFloat((noOfCellsInRow == 6 ? 3 : noOfCellsInRow) - 1))

        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat((noOfCellsInRow == 6 ? 3 : noOfCellsInRow)))
        currentCellFrame = noOfCellsInRow == 1 ? Int(collectionHeight - (isIpad ? 40 : 20)) : size - (isIpad ? 25 : 12)
        
//        self.GuideView_Category2WidthConstraint?.constant = CGFloat(currentCellFrame)
//        self.GuideView_Category2TopConstraint.constant = collectionView.bounds.height/2 - CGFloat(currentCellFrame/2)
        return CGSize(width: noOfCellsInRow == 6 ? Int(collectionHeight/1.9 - (4 * flowLayout.minimumInteritemSpacing)) : currentCellFrame, height: noOfCellsInRow == 6 ? Int(collectionHeight/2.05 - (4 * flowLayout.minimumInteritemSpacing)) : currentCellFrame)
        
        //        if (vcMode == "normal"){
        //            if concerns_Type == .pain_feel || concerns_Type == .Swallowing || concerns_Type == .Nausea || concerns_Type == .Fatigue || concerns_Type == .Bowels || concerns_Type == .Urination{
        //                return CGSize(width: (collectionView.bounds.width / 3 - 20), height: 175)
        //            }
        //            return isIpad == true ? CGSize(width: collectionWidth/3, height: collectionWidth/3) : CGSize(width: collectionWidth - 9, height: collectionWidth - 9)
        //        }else{
        //            return CGSize(width: (collectionView.bounds.width / 3 - 20), height: 175)
        //        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        if noOfCellsInRow <= 2 {
            return isIpad ? 40 : 20
        }
        return 1
    }
   
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if noOfCellsInRow == 6 {
            return isIpad ? 20 : 8
        }
        return collectionView.bounds.height/2 - CGFloat(currentCellFrame/2)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if noOfCellsInRow <= 2 {
            let totalCellWidth = Int(currentCellFrame) * noOfCellsInRow
            let totalSpacingWidth = Int(isIpad ? 60 : 30) * (noOfCellsInRow - 1)

            let leftInset = (collectionView.bounds.width - CGFloat(totalCellWidth + totalSpacingWidth)) / 2
            let rightInset = leftInset
            return UIEdgeInsets(top: collectionView.bounds.height/2 - CGFloat(currentCellFrame/2), left: rightInset, bottom: collectionView.bounds.height/2 - CGFloat(currentCellFrame/2), right: rightInset)
        }else{
            if noOfCellsInRow == 6 {
                return UIEdgeInsets(top: 0, left: isIpad ? 25 : hasTopNotch ? screenHeight/3 : screenHeight/4, bottom: 0, right: isIpad ? 15 : hasTopNotch ? screenHeight/3 : screenHeight/4)
            }
            return UIEdgeInsets(top: collectionView.bounds.height/2 - CGFloat(currentCellFrame/2), left: 0, bottom: collectionView.bounds.height/2 - CGFloat(currentCellFrame/2), right: 0)
      }

    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        selectIndex = indexPath.row
        collectionView.reloadData()
        
        let title = swallowing_ProblemsList[indexPath.row].1

        if title == "With Food".localized(){
            if IS_MALE{
                playSound(fileName:  SELECTED_LANG == "en" ? "With Food" : "con comida_with food", file_Extension: "wav")
            }else{
                playSound(fileName:  SELECTED_LANG == "en" ? "With Food_Swallowing_En" : "con comida (with food)_Swallowing_Sp", file_Extension: "mp3")
            }
        }else if title == "With Drinks".localized(){
            
            if IS_MALE{
                playSound(fileName:  SELECTED_LANG == "en" ? "With Drinks" : "con bebidas_with drinks", file_Extension: "wav")
            }else{
                playSound(fileName:  SELECTED_LANG == "en" ? "With Drinks_Swallowing_En" : "Con bebidas (with drinks)_Swallowing_Sp", file_Extension: "mp3")
            }
        }
        else if title == "With Pills".localized(){
            if IS_MALE{
                playSound(fileName:  SELECTED_LANG == "en" ? "With Pills" : "con pastillas_with pills", file_Extension: "wav")
            }else{
                playSound(fileName:  SELECTED_LANG == "en" ? "With Pills_Swallowing_En" : "con pastillas (with pills)_Swallowing_Sp", file_Extension: "mp3")
            }
        }
        else if title == "With Medication".localized(){
            if IS_MALE{
                playSound(fileName: SELECTED_LANG == "en" ? "With Medication(Fatigue)_com" : "con medicacion", file_Extension: "wav")
            }else{
                playSound(fileName: SELECTED_LANG == "en" ? "With medication_ Nausea_En" : "con medicacion (with medication)_ Nausea_Sp", file_Extension: "mp3")
            }
            
        }else if title == "With Food and Drinks".localized(){
           
            if IS_MALE{
                playSound(fileName:  SELECTED_LANG == "en" ? "With Food and Drinks" : "con alimentos o bebidas_with food or drinks", file_Extension: "wav")
            }else{
                playSound(fileName:  SELECTED_LANG == "en" ? "With food or drinks_Swallowing_En" : "Con comida y bebida (with food and drink)_ Nausea_Sp", file_Extension: "mp3")
            }
        }
                
        if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
            
            vc.concerns_Type = .Swallowing_More
            vc.logoImage = swallowing_ProblemsList[indexPath.row].0!
            vc.typeTitle = swallowing_ProblemsList[indexPath.row].1
            self.navigationController?.pushViewController(vc, animated: false)
            
        }
    }
    
}
