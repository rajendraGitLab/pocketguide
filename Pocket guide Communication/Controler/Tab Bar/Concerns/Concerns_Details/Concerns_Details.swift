//
//  Concerns_Details.swift
//  Pocket guide Communication
//
//  Created by Deepak on 01/11/21.
//

import UIKit


class Concerns_Details: UIViewController {
    
    @IBOutlet weak var imageLogo: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var btnYesOutlet: UIButton!
    @IBOutlet weak var btnNoOutlet: UIButton!
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var yesView: UIView!
    @IBOutlet weak var noView: UIView!
    
    @IBOutlet weak var topicView: UIView!
    @IBOutlet weak var topicTitle: UILabel!
    @IBOutlet weak var topicSubtitle: UILabel!
    
    @IBOutlet weak var summryBtnView: UIView!
    
    var logoImage = UIImage()
    var typeTitle = ""
    var concerns_Type: Concerns_Type = .All_Concerns
    
    var goToPage = ""
    var isFrome = ""
    var fromPain = ""
    
    var topicTitleText = ""
    var topicSubTitleText = ""
    
    var isShowSummryBtn = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.summryBtnView.isHidden = true
        topicTitle.text = topicTitleText
        topicSubtitle.text = topicSubTitleText
        if concerns_Type == .Topic_Board {
            imageLogo.isHidden = true
            topicView.isHidden = false
        }else{
            topicView.isHidden = true
            imageLogo.isHidden = false
        }
        if concerns_Type == .pain_feel || typeTitle == "Information".localized() || typeTitle == "Schedule Pain Meds".localized() || typeTitle == "Different Times".localized() || typeTitle == "Good".localized(){
            imageLogo.contentMode = .scaleAspectFit
        }else{
            imageLogo.contentMode = .scaleToFill
        }
        if typeTitle.localized() == "No Concerns".localized(){
            imageLogo.contentMode = .scaleAspectFit
        }
        if fromPain != "" {
            imageLogo.contentMode = .scaleAspectFit
            if fromPain == "No Pain".localized(){
                if IS_MALE{
                    playSound(fileName:  SELECTED_LANG == "en" ? "No Pain" : "No Pain_S", file_Extension: "wav")
                }else{
                    playSound(fileName:  SELECTED_LANG == "en" ? "No Pain_PainScale_En" : "Sin dolor (No pain)_PainScale_Sp", file_Extension: "mp3")
                }
                
            }else if fromPain == "Mild".localized(){
                if IS_MALE{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Mild" : "Mild_S", file_Extension: "wav")
                }else{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Mild_PainScale_En" : "leve (mild)_PainScale_Sp", file_Extension: "mp3")
                }
            }else if fromPain == "Severe".localized(){
                if IS_MALE{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Severe" : "Severe_S", file_Extension: "wav")
                }else{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Severe_PainScale_En" : "grave (severe)_PainScale_Sp", file_Extension: "mp3")
                }
            }
            else if fromPain == "Very Severe".localized(){
               
                if IS_MALE{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Very severe" : "Very severe_S", file_Extension: "wav")
                }else{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Very Severe_PainScale_En" : "muy grave (very severe)_PainScale_Sp", file_Extension: "mp3")
                }
            }
            else if fromPain == "Worst Pain Imaginable".localized(){
                
                if IS_MALE{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Worst Pain Imaginable" : "El peor dolor imaginable(WrostPainImaginabe)_male_Sp", file_Extension: "wav")
                }else{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Worst Pain Imaginable_PainScale_En" : "Peor dolor imaginable (worst pain imaginable)_PainScale_Sp", file_Extension: "mp3")
                }
            }
            else if fromPain == "Moderate".localized(){
             
                if IS_MALE{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Moderate" : "Moderate_S", file_Extension: "wav")
                }else{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Moderate_PainScale_En" : "moderado (moderate)_PainScale_Sp", file_Extension: "mp3")
                }
            }
        }
        
        imageLogo.image = logoImage
        
        lblTitle.text = typeTitle.localized()
        
        bgView.dropShadow()
        yesView.dropShadow()
        noView.dropShadow()
        
    }
    @IBAction func summryBtnAction(_ sender: UIButton) {
        if let Concerns_vc = SB_Summery.instantiateViewController(withIdentifier: "SummeryVC") as? SummeryVC{
                self.navigationController?.pushViewController(Concerns_vc, animated: false)
            }
    }
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    @IBAction func sidemenuTapped(_ sender: UIButton) {
        Util.showSideMenu(navC: self)
    }
    
    @IBAction func btnYesNoConfirmAction(_ sender: UIButton) {
        
        
        if sender.tag == 1 {
            
            SummeryInfo.shared.appendSummeryInfo(imgName: logoImage, imgTitle: typeTitle, timeStamp: NSDate().timeIntervalSince1970)
            btnYesOutlet.borderWidth = 6
            btnYesOutlet.borderColor = hexStringToUIColor(hex: "#0F9AF0")
            btnNoOutlet.borderColor = .clear
            
            if IS_MALE{
                playSound(fileName:  SELECTED_LANG == "en" ? "Yes" : "yes_si", file_Extension: "wav")
            }else{
                playSound(fileName: SELECTED_LANG == "en" ? "Yes_HRU_En" : "si (yes)_HRU_Sp", file_Extension: "mp3")
            }
            
            if isShowSummryBtn{
                if let Concerns_vc = SB_Summery.instantiateViewController(withIdentifier: "SummeryVC") as? SummeryVC{
                        self.navigationController?.pushViewController(Concerns_vc, animated: false)
                    }
                return
            }
            
            
            if typeTitle == "Something Else".localized() || concerns_Type == .Topic_Board {
                if let whiteBoard_vc = SB_White_bord.instantiateViewController(withIdentifier: "White_bord") as? White_bord{
                    whiteBoard_vc.comeFromNavigation = true
                    self.navigationController?.pushViewController(whiteBoard_vc, animated: false)
                }
                return
            }
            
            if isFrome == "How_often" {
                if concerns_Type == .Breathing_Problems{
                    if typeTitle == "Choking".localized() || typeTitle == "Swallowing".localized(){
                        if let Concerns_vc = SB_Concerns.instantiateViewController(withIdentifier: "Swallowing_More") as? Swallowing_More{
                            //                Concerns_vc.titleVC = "How often do you have problems with your bowels?"
                            Concerns_vc.comeFromBreathingProblems = true
                            Concerns_vc.concerns_Type = .Swallowing
                            
                            self.navigationController?.pushViewController(Concerns_vc, animated: false)
                            
                        }
                        return
                    }
                }
                if let Concerns_vc = SB_Calender.instantiateViewController(withIdentifier: "Calender") as? Calender{
                    
                    Concerns_vc.titleVC = goToPage
                    Concerns_vc.concerns_Type = concerns_Type
                    self.navigationController?.pushViewController(Concerns_vc, animated: false)
                }
                
                return
                
            }
            
            if concerns_Type == .All_Concerns {
                methodMakeRootTabbar(index: 1)
                //                if let vc = SB_Tab.instantiateViewController(withIdentifier: "Tab_bar") as? Tab_bar {
                //                    //vc.concerns_Type = .All_Concerns
                //                    let nav = UINavigationController(rootViewController: vc)
                //                    nav.interactivePopGestureRecognizer?.delegate = nil
                //                    nav.setNavigationBarHidden(true, animated: false)
                //                    UIApplication.shared.keyWindow?.rootViewController = nav
                //                    UIApplication.shared.keyWindow?.makeKeyAndVisible()
                //                }
                
                //
                //                if let Concerns_vc = SB_Concerns.instantiateViewController(withIdentifier: "Concerns") as? Concerns{
                //
                //                    self.navigationController?.pushViewController(Concerns_vc, animated: false)
                //
                //                }
                
            }
            if concerns_Type == .Pain {
                if let Concerns_vc = SB_HumanBody.instantiateViewController(withIdentifier: "HumanBody") as? HumanBody{
                    
                    self.navigationController?.pushViewController(Concerns_vc, animated: false)
                }
            }
            if concerns_Type == .Medication {
                if typeTitle == "Don't Want".localized() || typeTitle == "Information".localized()
                {
                    if let whiteBoard_vc = SB_White_bord.instantiateViewController(withIdentifier: "White_bord") as? White_bord{
                        whiteBoard_vc.comeFromNavigation = true
                        self.navigationController?.pushViewController(whiteBoard_vc, animated: false)
                    }
                    return
                }
//                else if typeTitle == "Whole".localized() || typeTitle == "Crushed".localized() || typeTitle == "Liquid".localized()
//                {
//                    return
//                }
                else{
                    if goToPage == "Concerns 2"{
                        if lblTitle.text == "Hard to Swallow".localized(){
                            if let Concerns_vc = SB_Concerns.instantiateViewController(withIdentifier: "Concerns") as? Concerns{
                                Concerns_vc.vcMode = "more"
                                Concerns_vc.comeFromhardToSwallow = true
                                Concerns_vc.topLbl = typeTitle.localized()
                                Concerns_vc.concerns_Type = .All_Concerns
                                self.navigationController?.pushViewController(Concerns_vc, animated: false)
                            }
                        }else if lblTitle.text == "Don't Want".localized(){
                        }
                        else{
                            if let Concerns_vc = SB_Concerns.instantiateViewController(withIdentifier: "Concerns") as? Concerns{
                                Concerns_vc.vcMode = "more"
                                Concerns_vc.topLbl = typeTitle.localized()
                                Concerns_vc.concerns_Type = .All_Concerns
                                self.navigationController?.pushViewController(Concerns_vc, animated: false)
                            }
                        }
                        
                    }else if goToPage == "White Board"{
                        
    //                    methodMakeRootTabbar(index: 2)
                        if let whiteBoard_vc = SB_White_bord.instantiateViewController(withIdentifier: "White_bord") as? White_bord{
                            whiteBoard_vc.comeFromNavigation = true
                            self.navigationController?.pushViewController(whiteBoard_vc, animated: false)
                        }
                        
                        
                    }else if goToPage == "Whole /Crushed /Liquid".localized(){
                        
                        if let Concerns_vc = SB_Concerns.instantiateViewController(withIdentifier: "Concerns") as? Concerns{
                            Concerns_vc.fromWholeCrushLiquid = "Whole /Crushed /Liquid".localized()
                            Concerns_vc.topLbl = typeTitle.localized()
                            Concerns_vc.concerns_Type = .Medication
                            self.navigationController?.pushViewController(Concerns_vc, animated: false)
                        }
                        
                    }else if goToPage == "Timing".localized(){
                        
                        if let Concerns_vc = SB_Concerns.instantiateViewController(withIdentifier: "Concerns") as? Concerns{
                            Concerns_vc.fromTiming = "timing".localized()
                            Concerns_vc.topLbl = typeTitle.localized()
                            Concerns_vc.concerns_Type = .Medication
                            self.navigationController?.pushViewController(Concerns_vc, animated: false)
                        }
                        
                    }else if goToPage == "How_often"{
                        if let Concerns_vc = SB_How_often.instantiateViewController(withIdentifier: "How_often") as? How_often{
                            Concerns_vc.titleVC = "How Often?"
                            
                            Concerns_vc.concerns_Type = .Breathing_Problems
                            
                            self.navigationController?.pushViewController(Concerns_vc, animated: false)
                        }
                    }
                    else{
                        if let Concerns_vc = SB_Concerns.instantiateViewController(withIdentifier: "Concerns") as? Concerns{
                            Concerns_vc.concerns_Type = .Medication
                            self.navigationController?.pushViewController(Concerns_vc, animated: false)
                        }
                    }
                    return
                }
            }
            if concerns_Type == .Breathing_Problems {
                if typeTitle == "Trach".localized(){
                    if let Concerns_vc = SB_Concerns.instantiateViewController(withIdentifier: "Concerns") as? Concerns{
                        Concerns_vc.concerns_Type = .Trach
                        self.navigationController?.pushViewController(Concerns_vc, animated: false)
                    }
                }else if goToPage == "How_often"{
                    if let Concerns_vc = SB_How_often.instantiateViewController(withIdentifier: "How_often") as? How_often{
                        Concerns_vc.titleVC = "How often do you have breathing problems?".localized()
                        Concerns_vc.addMoreIcon = true
                        Concerns_vc.concerns_Type = .Breathing_Problems
                        self.navigationController?.pushViewController(Concerns_vc, animated: false)
                        
                    }
                }else{
                    if let Concerns_vc = SB_Concerns.instantiateViewController(withIdentifier: "Concerns") as? Concerns{
                        Concerns_vc.concerns_Type = .Breathing_Problems
                        self.navigationController?.pushViewController(Concerns_vc, animated: false)
                        
                    }
                }
                
                return
                
            }else if concerns_Type == .pain_feel {
                
                if let Concerns_vc = SB_Concerns.instantiateViewController(withIdentifier: "Concerns") as? Concerns{
                    Concerns_vc.concerns_Type = .pain_feel
                    self.navigationController?.pushViewController(Concerns_vc, animated: false)
                }
                return
                
            }else if concerns_Type == .Swallowing {
                if typeTitle == "Trach".localized(){
                    if let Concerns_vc = SB_Concerns.instantiateViewController(withIdentifier: "Concerns") as? Concerns{
                        Concerns_vc.concerns_Type = .Trach
                        self.navigationController?.pushViewController(Concerns_vc, animated: false)
                    }
                }else{
                    if goToPage == "How_often"{
                        if let Concerns_vc = SB_Concerns.instantiateViewController(withIdentifier: "Swallowing_More") as? Swallowing_More{
                            //                Concerns_vc.titleVC = "How often do you have problems with your bowels?"
                            
                            Concerns_vc.concerns_Type = .Swallowing
                            
                            self.navigationController?.pushViewController(Concerns_vc, animated: false)
                            
                        }
                    }else{
                        if let Concerns_vc = SB_Concerns.instantiateViewController(withIdentifier: "Concerns") as? Concerns{
                            Concerns_vc.concerns_Type = .Swallowing
                            self.navigationController?.pushViewController(Concerns_vc, animated: false)
                        }
                    }
                }
                    
               
                
                
                
                return
                
            }else if concerns_Type == .Nausea {
                
                
                if goToPage == "How_often"{
                    
                    var gettitle = "How often do you have nausea?"
                    if typeTitle == "Pain" {
                        gettitle = "How often do you have pain?"
                    }
                    
                    if let Concerns_vc = SB_How_often.instantiateViewController(withIdentifier: "How_often") as? How_often{
                        Concerns_vc.titleVC = gettitle
                        Concerns_vc.concerns_Type = .Nausea
                        self.navigationController?.pushViewController(Concerns_vc, animated: false)
                    }
                    
                }else{
                    if let Concerns_vc = SB_Concerns.instantiateViewController(withIdentifier: "Concerns") as? Concerns{
                        Concerns_vc.concerns_Type = .Nausea
                        self.navigationController?.pushViewController(Concerns_vc, animated: false)
                    }
                }
                return
                
            }else if concerns_Type == .Trach {
                
                if typeTitle == "Breathing / Coughing".localized(){
                    if let Concerns_vc = SB_Concerns.instantiateViewController(withIdentifier: "Concerns") as? Concerns{
                        Concerns_vc.concerns_Type = .Breathing_Problems
                        self.navigationController?.pushViewController(Concerns_vc, animated: false)
                        
                    }
                }else if typeTitle == "Swallowing".localized(){
                    if let Concerns_vc = SB_Concerns.instantiateViewController(withIdentifier: "Concerns") as? Concerns{
                        Concerns_vc.concerns_Type = .Swallowing
                        self.navigationController?.pushViewController(Concerns_vc, animated: false)
                    }
                }else{
                    if goToPage == "How_often"{

                        
                        let gettitle = "How Often?"
    //                    if typeTitle == "Pain" {
    //                        gettitle = "How often do you have pain?"
    //                    }

                        if let Concerns_vc = SB_How_often.instantiateViewController(withIdentifier: "How_often") as? How_often{
                            Concerns_vc.titleVC = gettitle
                            Concerns_vc.concerns_Type = .Trach
                            self.navigationController?.pushViewController(Concerns_vc, animated: false)
                        }

                    }else{
                        if let Concerns_vc = SB_Concerns.instantiateViewController(withIdentifier: "Concerns") as? Concerns{
                            Concerns_vc.concerns_Type = .Trach
                            self.navigationController?.pushViewController(Concerns_vc, animated: false)
                        }
                    }
                }
                
              
                return
                
            }else if concerns_Type == .vision_problem {
                
                if goToPage == "How_often"{
                    
                    let gettitle = "How often do you have Vision problems?"
//                    if typeTitle == "Pain" {
//                        gettitle = "How often do you have pain?"
//                    }

                    if let Concerns_vc = SB_How_often.instantiateViewController(withIdentifier: "How_often") as? How_often{
                        Concerns_vc.titleVC = gettitle
                        Concerns_vc.concerns_Type = .vision_problem
                        self.navigationController?.pushViewController(Concerns_vc, animated: false)
                    }

                }else if goToPage == "whiteboard"{
                    if let whiteBoard_vc = SB_White_bord.instantiateViewController(withIdentifier: "White_bord") as? White_bord{
                        whiteBoard_vc.comeFromNavigation = true
                        self.navigationController?.pushViewController(whiteBoard_vc, animated: false)
                    }
                }
                else{
                    if let Concerns_vc = SB_Concerns.instantiateViewController(withIdentifier: "Concerns") as? Concerns{
                        Concerns_vc.concerns_Type = .vision_problem
                        self.navigationController?.pushViewController(Concerns_vc, animated: false)
                        
                    }
                    
                }
                
                
                
                return
                
            }else if concerns_Type == .Emotions_Feelings {
                
                
                if goToPage == "How_often"{

                    var gettitle = "How often do you feel this way?"
//                    if typeTitle == "Pain" {
//                        gettitle = "How often do you have pain?"
//                    }

                    if let Concerns_vc = SB_How_often.instantiateViewController(withIdentifier: "How_often") as? How_often{
                        Concerns_vc.titleVC = gettitle
                        Concerns_vc.concerns_Type = .Emotions_Feelings
                        self.navigationController?.pushViewController(Concerns_vc, animated: false)
                    }

                }else{
                    if let Concerns_vc = SB_Concerns.instantiateViewController(withIdentifier: "Concerns") as? Concerns{
                        Concerns_vc.concerns_Type = .Emotions_Feelings
                        self.navigationController?.pushViewController(Concerns_vc, animated: false)
                        
                    }
                    
                }
                
                
                
                return
                
            }else if concerns_Type == .Fatigue {
                
                if lblTitle.text == "Trouble Sleeping".localized() {
                    if let Concerns_vc = SB_Calender.instantiateViewController(withIdentifier: "Calender") as? Calender{
                        
                        Concerns_vc.titleVC = "When did this start?".localized()
                            self.navigationController?.pushViewController(Concerns_vc, animated: false)
                        }
                }else if goToPage == "How_often"{
                    
                    if let Concerns_vc = SB_How_often.instantiateViewController(withIdentifier: "How_often") as? How_often{
                        Concerns_vc.titleVC = "How often do you have fatigue?"
                        Concerns_vc.concerns_Type = .Fatigue
                        
                        self.navigationController?.pushViewController(Concerns_vc, animated: false)
                        
                    }
                }else{
                    
                    if let Concerns_vc = SB_Concerns.instantiateViewController(withIdentifier: "Concerns") as? Concerns{
                        Concerns_vc.concerns_Type = .Fatigue
                        self.navigationController?.pushViewController(Concerns_vc, animated: false)
                        
                    }
                    
                }
                
                
                
                return
                
            }else if concerns_Type == .Bowels {
                
                if goToPage == "How_often"{
                    
                    
                    if let Concerns_vc = SB_How_often.instantiateViewController(withIdentifier: "How_often") as? How_often{
                        Concerns_vc.titleVC = "How often do you have problems with your bowels?"
                        Concerns_vc.concerns_Type = .Bowels
                        
                        self.navigationController?.pushViewController(Concerns_vc, animated: false)
                        
                    }
                    
                }else{
                    if let Concerns_vc = SB_Concerns.instantiateViewController(withIdentifier: "Concerns") as? Concerns{
                        Concerns_vc.concerns_Type = .Bowels
                        self.navigationController?.pushViewController(Concerns_vc, animated: false)
                        
                    }
                    
                    
                }
                
                
                return
                
            }else if concerns_Type == .Urination {
                
                if goToPage == "How_often"{
                    
                    if let Concerns_vc = SB_How_often.instantiateViewController(withIdentifier: "How_often") as? How_often{
                        //                Concerns_vc.titleVC = "How often do you have problems with your bowels?"
                        
                        Concerns_vc.concerns_Type = .Urination
                        
                        self.navigationController?.pushViewController(Concerns_vc, animated: false)
                        
                    }
                }else{
                    if let Concerns_vc = SB_Concerns.instantiateViewController(withIdentifier: "Concerns") as? Concerns{
                        Concerns_vc.concerns_Type = .Urination
                        self.navigationController?.pushViewController(Concerns_vc, animated: false)
                        
                    }
                }
                
                return
                
            }else if concerns_Type == .Pain_meter {
                
                if goToPage == "Pain_meter" {
                    
                    if let vc = SB_Concerns.instantiateViewController(withIdentifier: "Pain_meter") as? Pain_meter {
                        
                        self.navigationController?.pushViewController(vc, animated: false)
                        
                    }
                    
                    return
                }else{
                    if let Concerns_vc = SB_How_often.instantiateViewController(withIdentifier: "How_often") as? How_often{
                        //                Concerns_vc.titleVC = "How often do you have problems with your bowels?"
                        
                        Concerns_vc.concerns_Type = .Pain_meter
                        
                        self.navigationController?.pushViewController(Concerns_vc, animated: false)
                        
                    }
                }
                
                //                if let Concerns_vc = SB_Calender.instantiateViewController(withIdentifier: "Calender") as? Calender{
                //                 let vc = (TabbarObj.viewControllers?[0] as! UINavigationController).viewControllers.first as? Concerns
                //                    Concerns_vc.concerns_Type = .Pain
                //                    vc?.navigationController?.pushViewController(Concerns_vc, animated: false)
                //
                //
                //            }
                
            }else if concerns_Type == .Swallowing_More {
                
//                 if let Concerns_vc = SB_Problem_Confirmation.instantiateViewController(withIdentifier: "Problem_Confirmation") as? Problem_Confirmation{
//
//                    Concerns_vc.concerns_Type = .Swallowing
//
//                    self.navigationController?.pushViewController(Concerns_vc, animated: false)
//
//                }
                if let Concerns_vc = SB_Calender.instantiateViewController(withIdentifier: "Calender") as? Calender{

                    Concerns_vc.titleVC = "When did it start?".localized()
                    Concerns_vc.concerns_Type = concerns_Type
                    self.navigationController?.pushViewController(Concerns_vc, animated: false)
                }
                
            }else if concerns_Type == .Other{
                
                if let whiteBoard_vc = SB_White_bord.instantiateViewController(withIdentifier: "White_bord") as? White_bord{
                    whiteBoard_vc.comeFromNavigation = true
                    self.navigationController?.pushViewController(whiteBoard_vc, animated: false)
                }
            }
            
            //            if typeTitle == "Medication"{
            //
            //                if let Concerns_vc = SB_Concerns.instantiateViewController(withIdentifier: "Concerns") as? Concerns{
            //                    Concerns_vc.isFrome = "Medication"
            //                    self.navigationController?.pushViewController(Concerns_vc, animated: false)
            //
            //                }
            //                return
            //            }
            
            //            if typeTitle == "Pain" {
            //                if let Concerns_vc = SB_HumanBody.instantiateViewController(withIdentifier: "HumanBody") as? HumanBody{
            //
            //                    self.navigationController?.pushViewController(Concerns_vc, animated: false)
            //                }
            //            }else{
            //
            //                if let vc = SB_Concerns.instantiateViewController(withIdentifier: "Pain_meter") as? Pain_meter {
            //                           self.navigationController?.pushViewController(vc, animated: false)
            //                       }
            //
            //            }
            
        }else{
            
            SummeryInfo.shared.removeWithTitle(imgtitle: typeTitle)
            if IS_MALE{
                playSound(fileName:  SELECTED_LANG == "en" ? "No" : "No_no", file_Extension: "wav")
            }else{
                playSound(fileName: SELECTED_LANG == "en" ? "No_HRU_En" : "No (no)_HRU_Sp", file_Extension: "mp3")
            }
            
            btnNoOutlet.borderWidth = 6
            btnNoOutlet.borderColor = hexStringToUIColor(hex: "#0F9AF0")
            btnYesOutlet.borderColor = .clear
            
            self.navigationController?.popViewController(animated: false)
        }
    }
    
    
}
