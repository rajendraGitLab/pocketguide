//
//  Concerns.swift
//  Pocket guide Communication
//
//  Created by Deepak on 29/10/21.
//

import UIKit

enum Concerns_Type {
    case Breathing_Problems,Pain,Medication,Swallowing,All_Concerns,Topic_Board
    case Nausea,Fatigue,Bowels,Urination,Other,No_Concerns
    case pain_feel
    case none
    case Bladder
    case Pain_meter
    case Swallowing_More
    case How_often
    case Settings
    case vision_problem,Emotions_Feelings,Trach,Needs_Board
    //How does your pain feel?
}


class Concerns: UIViewController,UIScrollViewDelegate {
    
    // Main //With Medication //concerns_medication
    var healthTypeList = [(UIImage(named: "concerns_pain"),"Pain".localized()),(UIImage(named: "With Medication"),"Medication".localized()),(UIImage(named: "shortness_of_breath_BP"),"Breathing / Coughing".localized()),(UIImage(named: "concerns_swallowing"),"Swallowing".localized()),(UIImage(named: "concerns_Nausea"),"Nausea".localized()),(UIImage(named: "concerns_bowels"),"Bowels".localized()),(UIImage(named: "concerns_urination"),"Urination".localized()),(UIImage(named: "concerns_fatigue"),"Fatigue".localized()),
                          
                          (UIImage(named: "depressedN"),"Emotions / Feelings".localized()),(UIImage(named: "glasses"),"Vision Problems".localized()),(UIImage(named: "Trach"),"Trach".localized())
                          
                          ,(UIImage(named: "concerns_others"),"Something Else".localized()),(UIImage(named: "health_good"),"No Concerns".localized())] //(UIImage(named: "concerns_swallowing"),"Swallowing")
    
    var healthTypeListMore = [(UIImage(named: "concerns_Nausea"),"Nausea".localized()),(UIImage(named: "concerns_fatigue"),"Fatigue".localized()),(UIImage(named: "concerns_bowels"),"Bowels".localized()),(UIImage(named: "concerns_urination"),"Urination".localized()),(UIImage(named: "concerns_swallowing"),"Swallowing".localized()),(UIImage(named: "concerns_others"),"Something Else".localized()),(UIImage(named: "health_good"),"No Concerns".localized())]  // //Concerns_close // concerns_swallowing //Other Concerns_close
    
    var hardToSwallow_List = [(UIImage(named: "Medication_Too Much"),"Too Many".localized()),(UIImage(named: "too_big"),"Too Big".localized()),(UIImage(named: "whole_crushed_liquid_New"),"Whole /Crushed /Liquid".localized())]
    
    // 1
    var healthTypeList_Medication = [(UIImage(named: "Medication_Side_Effects"),"Side Effects".localized()),(UIImage(named: "concerns_swallowing"),"Hard to Swallow".localized()),(UIImage(named: "Medication_Information"),"Information".localized()),(UIImage(named: "Medication_Too Much"),"Too Much".localized()),(UIImage(named: "Medication_Too_Little"),"Too Little".localized()),(UIImage(named: "timing"),"Timing".localized()),(UIImage(named: "whole_crushed_liquid_New"),"Whole /Crushed /Liquid".localized()),(UIImage(named: "don_t_want_pp"),"Don't Want".localized()),(UIImage(named: "concerns_others"),"Something Else".localized())]
    
    //Breathing Problems
    var breathing_ProblemsList = [(UIImage(named: "shortness_of_breath_BP"),"Shortness of Breath".localized()),(UIImage(named: "Coughing"),"Coughing".localized()),(UIImage(named: "Chest Pain"),"Chest Pain".localized()),(UIImage(named: "Choking"),"Choking".localized().localized()),(UIImage(named: "Trach"),"Trach".localized()),(UIImage(named: "concerns_others"),"Something Else".localized())]
    
    //How does your pain feel? Types
    var healthPainFellList = [(UIImage(named: "Squeezing"),"Squeezing / Tight".localized()),(UIImage(named: "Burning_ur"),"Burning".localized()),(UIImage(named: "achingDull"),"Aching / Dull".localized()),(UIImage(named: "Sharp"),"Sharp / Stabbing".localized()),(UIImage(named: "Numb"),"Numb / Tingling".localized()),(UIImage(named: "spasming"),"Spasming".localized())
                              
                              ,(UIImage(named: "pins_and_needlesN"),"Pins and Needles".localized().localized())
                              ,(UIImage(named: "Cramping"),"Cramping".localized())
                              ,(UIImage(named: "heavy__pain_"),"Heavy".localized())
                              ,(UIImage(named: "shooting_pain"),"Shooting".localized())
                              ,(UIImage(named: "throbbingN"),"Throbbing".localized())
                              ,(UIImage(named: "itchingN"),"Itching".localized()),(UIImage(named: "concerns_others"),"Something Else".localized())
    ]
    
    //Swallowing
    var swallowing_ProblemsList = [(UIImage(named: "Choking"),"Choking".localized()),(UIImage(named: "Food Sticking"),"Food Sticking".localized()),(UIImage(named: "Heartburn"),"Heartburn".localized()),(UIImage(named: "Coughing"),"Coughing".localized())
                                   ,(UIImage(named: "Trach"),"Trach".localized())
                                   //                                   ,(UIImage(named: "applogo2"),"Something else".localized())
                                   ,(UIImage(named: "shortness_of_breath_BP"),"Breathing / Coughing".localized()),(UIImage(named: "concerns_others"),"Something Else".localized())
    ]
    
    //Nausea
    var nausea_ProblemsList = [
        //        (UIImage(named: "Pain"),"Pain"),
        (UIImage(named: "Vomiting"),"Vomiting".localized()),(UIImage(named: "With Food"),"With Food".localized()),(UIImage(named: "Constipation"),"Constipation".localized()),(UIImage(named: "With Medication"),"With Medication".localized()),(UIImage(named: "with_food_and_drinks"),"With Food/Drink".localized()),
        //        (UIImage(named: "applogo2"),"Gas/Bloating"),
        //        (UIImage(named: "applogo2"),"Bed Pan"),
        (UIImage(named: "Diarrhea"),"Diarrhea".localized()),(UIImage(named: "Cramping"),"Cramping".localized()),(UIImage(named: "concerns_others"),"Something Else".localized())]
    
    //vision
    var vision_ProblemsList = [(UIImage(named: "double-vision"),"Double Vision".localized()),(UIImage(named: "blurry"),"Blurry".localized()),(UIImage(named: "left_vision"),"Left Vision".localized()),(UIImage(named: "right_vision"),"Right Vision".localized()),(UIImage(named: "glasses"),"Glasses".localized()),(UIImage(named: "concerns_others"),"Something Else".localized())]
    
    //whole/crushed/liquid
    var wholeCrushedLiquied_ProblemsList = [(UIImage(named: "whole"),"Whole".localized()),(UIImage(named: "crushed"),"Crushed".localized()),(UIImage(named: "Liquid"),"Liquid".localized())]
    
    //NeedsBoard
    var Needs_board_List = [(UIImage(named: "toilet"),"Bathroom".localized()),(UIImage(named: "bed"),"Bed".localized()),(UIImage(named: "With Food"),"Food".localized()),(UIImage(named: "With Drinks"),"Drink".localized()),(UIImage(named: "glasses"),"Glasses".localized()),(UIImage(named: "With Medication"),"Medication".localized())]
    
    //timingList
    var timing_List = [(UIImage(named: "scheduled"),"Schedule Pain Meds".localized()),(UIImage(named: "different_time"),"Different Times".localized()),(UIImage(named: "spread_out"),"Spread Out".localized())]
    
    //emotionsFeeling
    var emotions_feelings_list = [(UIImage(named: "anxious"),"Anxious".localized()),(UIImage(named: "depressedN"),"Depressed".localized()),(UIImage(named: "scared_icon"),"Scared".localized()),(UIImage(named: "angryN"),"Angry".localized()),(UIImage(named: "Frustrated"),"Frustrated".localized()),(UIImage(named: "exhaustedN"),"Exhausted".localized()),(UIImage(named: "sadN"),"Sad".localized()),(UIImage(named: "concerns_others"),"Something Else".localized())]
    
    //Trash
    var trash_list = [(UIImage(named: "mucous"),"Mucus / Secretion".localized()),(UIImage(named: "suction"),"Suction".localized()),(UIImage(named: "Cap"),"Cap".localized()),(UIImage(named: "SpeakingValve"),"Speaking Valve".localized()),
                      //                      (UIImage(named: "applogo2"),"Difficulty Breathing"),
                      (UIImage(named: "Remove"),"Remove".localized()),(UIImage(named: "shortness_of_breath_BP"),"Breathing / Coughing".localized()),(UIImage(named: "concerns_swallowing"),"Swallowing".localized()),(UIImage(named: "concerns_others"),"Something Else".localized())
                      //                      ,(UIImage(named: "applogo2"),"Speaking")
    ]
    
    //Fatigue
    var fatigue_ProblemsList = [(UIImage(named: "Wake up Tired"),"Wake up Tired".localized()),(UIImage(named: "Trouble Sleeping"),"Trouble Sleeping".localized()),(UIImage(named: "More Than Usual"),"More Than Usual".localized()),(UIImage(named: "With Activity"),"With Activity".localized()),(UIImage(named: "All Day"),"All Day".localized()),(UIImage(named: "With Medication"),"With Medication".localized()),(UIImage(named: "concerns_others"),"Something Else".localized())]
    
    //Bowels
    var bowels_ProblemsList = [(UIImage(named: "Constipation"),"Constipation".localized()),(UIImage(named: "Diarrhea"),"Diarrhea".localized()),(UIImage(named: "gasBloating"),"Gas/Bloating".localized()),(UIImage(named: "Cramping"),"Cramping".localized())
                               ,(UIImage(named: "Blood"),"Blood".localized()),(UIImage(named: "bedpan".localized()),"Bed Pan".localized()),(UIImage(named: "concerns_others"),"Something Else".localized())]
    
    //Urination
    var urination_ProblemsList = [(UIImage(named: "Burning_ur"),"Burning".localized()),(UIImage(named: "Frequent"),"Frequent".localized()),(UIImage(named: "urinary_incontinence"),"Urgency".localized()),(UIImage(named: "Very_Little_Infrequent"),"Very Little/ Infrequent".localized())
                                  //                                  ,(UIImage(named: "Pain_ur"),"Pain")
                                  ,(UIImage(named: "Blood_ur"),"Blood".localized()),(UIImage(named: "concerns_others"),"Something Else".localized())]
    
    var topicBoard_List = [(UIImage(named: "applogo2"),"Discharge".localized(),"Discharge"),(UIImage(named: "applogo2"),"Will I Get Better?".localized(),"Will I Get Better?"),(UIImage(named: "applogo2"),"Therapy".localized(),"Therapy"),(UIImage(named: "applogo2"),"Communication".localized(),"Communication"),(UIImage(named: "applogo2"),"Driving".localized(),"Driving"),(UIImage(named: "applogo2"),"Home Set-Up".localized(),"Home Set-Up"),(UIImage(named: "applogo2"),"Stroke Prevention".localized(),"Stroke Prevention"),(UIImage(named: "applogo2"),"Family Support".localized(),"Family Support"),(UIImage(named: "applogo2"),"Feeding Tube".localized(),"Feeding Tube"),(UIImage(named: "applogo2"),"Trach".localized(),"Trach"),(UIImage(named: "applogo2"),"Money".localized(),"Money"),(UIImage(named: "applogo2"),"Follow Up Appointments".localized(),"Follow Up Appointments")
                           ,(UIImage(named: "applogo2"),"Household Activity".localized(),"Household Activity")
                           ,(UIImage(named: "applogo2"),"Community Activity".localized(),"Community Activity")
                           ,(UIImage(named: "applogo2"),"Work".localized(),"Work")
                           ,(UIImage(named: "applogo2"),"Sex".localized(),"Sex")
                           ,(UIImage(named: "applogo2"),"Family".localized(),"Family")
                           ,(UIImage(named: "applogo2"),"Raising Children".localized(),"Raising Children")
                           ,(UIImage(named: "applogo2"),"Food".localized(),"Food")
                           ,(UIImage(named: "applogo2"),"Alcohol".localized(),"Alcohol")
    ]
    
    
    @IBOutlet weak var btnMenuOutlet: UIButton!
    @IBOutlet weak var btnBackOutlet: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var GuideView: UIView!
    
    @IBOutlet weak var indicatorView: UIView!
    @IBOutlet weak var indicatorBgView: UIView!
    @IBOutlet weak var indicatorViewTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var GuideView_1: UIView!
    @IBOutlet weak var GuideView_2: UIView!
    @IBOutlet weak var GuideView_3: UIView!
    @IBOutlet weak var GuideView_4: UIView!
    @IBOutlet weak var GuideView_5: UIView!
    @IBOutlet weak var GuideView_6: UIView!
    
    @IBOutlet weak var dashedView_1: UIView!
    @IBOutlet weak var dashedView_2: UIView!
    @IBOutlet weak var dashedView_3: UIView!
    
    @IBOutlet weak var GuideView_Category2WidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var GuideView_Category2TopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var zoomView : ZoomableView!
    @IBOutlet weak var sclView : UIScrollView!
    
    var selectIndex = -1
    var vcMode = "normal"
    var isFrome = "" //Medication
    var offSet = CGPoint()
    var topLbl = String()
    
    var currentCellFrame = Int()
    var concerns_Type: Concerns_Type = .All_Concerns
    
    var fromWholeCrushLiquid = String()
    var fromTiming = String()
    var lblScrollBar = UILabel()
    var comeFromhardToSwallow = false
    
    var isMedicationLastIcon = false
    var isZooming = false
    
    fileprivate (set) var scrollBar : VerticalScrollBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        sclView.minimumZoomScale = 1
        sclView.maximumZoomScale = 10.0
        sclView.delegate = self
//        zoomView.sourceView = collectionView
//        zoomView.isZoomable = true
        
        if concerns_Type == .All_Concerns {
            lblTitle.text = "Concerns".localized()
            if vcMode == "more" {
                lblTitle.text = topLbl.localized()
            }
            btnBackOutlet.isHidden = true
        }else if concerns_Type == .Medication {
            lblTitle.text = "Medication".localized()
            if fromWholeCrushLiquid == "Whole /Crushed /Liquid".localized() {
                lblTitle.text = "Whole /Crushed /Liquid".localized()
            }else if fromTiming == "timing".localized(){
                lblTitle.text = "Timing".localized()
            }
            btnBackOutlet.isHidden = true
        }else if concerns_Type == .pain_feel {
            lblTitle.text = "How does your pain feel?".localized()
            btnBackOutlet.isHidden = true
            
        }else if concerns_Type == .Breathing_Problems {
            lblTitle.text = "Breathing Problems".localized()
            btnBackOutlet.isHidden = true
            
        }else if concerns_Type == .Swallowing {
            lblTitle.text = "Swallowing".localized()
            btnBackOutlet.isHidden = true
            
        }else if concerns_Type == .Nausea {
            lblTitle.text = "Nausea".localized()
            btnBackOutlet.isHidden = true
            
        }else if concerns_Type == .Fatigue {
            lblTitle.text = "Fatigue".localized()
            btnBackOutlet.isHidden = true
            
        }else if concerns_Type == .Bowels {
            lblTitle.text = "Bowels".localized()
            btnBackOutlet.isHidden = true
            
        }else if concerns_Type == .Urination {
            lblTitle.text = "Urination".localized()
            btnBackOutlet.isHidden = true
        }else if concerns_Type == .vision_problem {
            lblTitle.text = "Vision Problems".localized()
            btnBackOutlet.isHidden = true
        }
        else if concerns_Type == .Trach {
            lblTitle.text = "Trach".localized()
            btnBackOutlet.isHidden = true
        }else if concerns_Type == .Emotions_Feelings {
            lblTitle.text = "Emotions / Feelings".localized()
            btnBackOutlet.isHidden = true
        }else if concerns_Type == .Needs_Board {
            lblTitle.text = "Needs Board".localized()
            btnBackOutlet.isHidden = true
        }else if concerns_Type == .Topic_Board {
            lblTitle.text = "Topic Board".localized()
            btnBackOutlet.isHidden = true
        }else {
            lblTitle.text = "Concerns".localized()
            btnBackOutlet.isHidden = false
        }
        self.collectionView.contentInsetAdjustmentBehavior = .always
        //        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
        //            layout.scrollDirection = isIpad == true ? .vertical : .horizontal
        //        }
        hideGuideNextButton(show: [GuideView_1,dashedView_1], hide: [GuideView_2,GuideView_3,GuideView_4,GuideView_5,GuideView_6,dashedView_2,dashedView_3])
        
        //        if isShowGuideScreen {
        //            GuideView.isHidden = false
        if comeFromHowRuVC {
            let guiDView = GuideTabView.intitiateFromNib(tabHeight: Int(self.tabBarController?.tabBar.fs_height ?? 0))
            guiDView.tag = 56
            guiDView.stackViewHeight.constant = CGFloat(Int(self.tabBarController?.tabBar.fs_height ?? 0))
            self.tabBarController?.view.addSubview(guiDView)
            
        }else{
            GuideView.isHidden = true
        }
        
        //        }else{
        //            GuideView.isHidden = true
        //        }
        
        
        self.uiUpdate(vcMode: self.vcMode)
        self.scrollBar = VerticalScrollBar(frame: CGRect.zero, targetScrollView: self.collectionView)
                self.view.addSubview(self.scrollBar!)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.scrollBar.frame = CGRect(x: UIScreen.main.bounds.width - (isIpad ? 40 : 20) - (UIApplication.shared.windows[0].safeAreaInsets.left), y: self.view.frame.height - self.collectionView.frame.height - (isIpad ? 90 : (UIDevice.current.hasNotch ? 60 : 42)) - (isIpad ? 40 : 0), width: self.indicatorView.frame.width * 1.6, height: self.collectionView.frame.height)
        self.scrollBar.backgroundColor = .clear
    }
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        isZooming = true
    }
    func scrollViewDidEndZooming(_ scrollView: UIScrollView, with view: UIView?, atScale scale: CGFloat) {
        isZooming = false
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if !isZooming{
            let totalScroll = scrollView.contentSize.height - self.collectionView.frame.height
            let ratio = totalScroll/(self.collectionView.frame.height - self.indicatorView.frame.height)
            self.indicatorViewTopConstraint.constant = (scrollView.contentOffset.y)/(ratio)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        isMedicationLastIcon = false
    }
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
            return zoomView
        }
    @IBAction func guideNextButtonAction(_ sender: UIButton) {
        switch sender.tag {
        case 5:
            hideGuideNextButton(show: [GuideView_6,dashedView_3], hide: [GuideView_1,GuideView_2,GuideView_3,GuideView_4,GuideView_5,dashedView_1,dashedView_2])
        case 6:
            hideGuideNextButton(show: [GuideView_3], hide: [GuideView_1,GuideView_2,GuideView_4,GuideView_5,GuideView_6,dashedView_1,dashedView_2,dashedView_3])
            NotificationCenter.default.post(name: Notification.Name("showDash"), object: nil, userInfo: ["show": "1"])
        case 7:
            hideGuideNextButton(show: [GuideView_4], hide: [GuideView_1,GuideView_2,GuideView_3,GuideView_5,GuideView_6,dashedView_1,dashedView_2,dashedView_3])
            NotificationCenter.default.post(name: Notification.Name("showDash"), object: nil, userInfo: ["show": "2"])
        case 8:
            hideGuideNextButton(show: [GuideView_5], hide: [GuideView_1,GuideView_2,GuideView_3,GuideView_4,GuideView_6,dashedView_1,dashedView_2,dashedView_3])
            NotificationCenter.default.post(name: Notification.Name("showDash"), object: nil, userInfo: ["show": "3"])
        case 11:
            hideGuideNextButton(show: [GuideView_2,dashedView_2], hide: [GuideView_1,GuideView_3,GuideView_4,GuideView_5,GuideView_6,dashedView_1,dashedView_3])
            
        case 9:
            GuideView.removeFromSuperview()
            lblScrollBar.removeFromSuperview()
            self.tabBarController?.view.viewWithTag(56)?.removeFromSuperview()
            if let vc = SB_JustChoose.instantiateViewController(withIdentifier: "JustChooseVC") as? JustChooseVC {
                let nav = UINavigationController(rootViewController: vc)
                nav.interactivePopGestureRecognizer?.delegate = nil
                nav.setNavigationBarHidden(true, animated: false)
                window?.rootViewController = nav
                window?.makeKeyAndVisible()
            }
        case 10:
            GuideView.removeFromSuperview()
            lblScrollBar.removeFromSuperview()
            self.tabBarController?.view.viewWithTag(56)?.removeFromSuperview()
            if let vc = SB_JustChoose.instantiateViewController(withIdentifier: "JustChooseVC") as? JustChooseVC {
                let nav = UINavigationController(rootViewController: vc)
                nav.interactivePopGestureRecognizer?.delegate = nil
                nav.setNavigationBarHidden(true, animated: false)
                window?.rootViewController = nav
                window?.makeKeyAndVisible()
            }
        default:
            print("")
        }
    }
    
    func hideGuideNextButton(show: [UIView],hide: [UIView]){
        for i in 0..<show.count{
            show[i].isHidden = false
        }
        for i in 0..<hide.count{
            hide[i].isHidden = true
            print("hide ",i)
        }
    }
    
    @IBAction func btnMenuAction(_ sender: UIButton) {
        
        if concerns_Type == .All_Concerns {
            Util.showSideMenu(navC: self)
        }else{
            self.navigationController?.popViewController(animated: false)
        }
        
        //        if vcMode == "normal"{
        //            Util.showSideMenu(navC: self)
        //        }else{
        //            vcMode = "normal"
        //            uiUpdate(vcMode: vcMode)
        //        }
        
    }
    
    @IBAction func btnNextAction(_ sender: UIButton) {
        
        vcMode = "more"
        selectIndex = -1
        uiUpdate(vcMode: vcMode)
        
    }
    private func playSound_SomthingElse(){
        if IS_MALE {
            if SELECTED_LANG == "en"{
                playSound(fileName: "Something Else_male_en", file_Extension: "wav")
            }else{
                playSound(fileName: "algo mas_something else", file_Extension: "wav")
            }
        }else{
            playSound(fileName:  SELECTED_LANG == "en" ? "Something else_Swallowing_En" : "algo mas (something else)_Swallowing_Sp", file_Extension: "mp3")
        }
    }
    func uiUpdate(vcMode : String){
        
        let layout = UICollectionViewFlowLayout()
        
        //  if concerns_Type == .All_Concerns {
        for cell in collectionView.visibleCells {
            let indexPath = collectionView.indexPath(for: cell)
            print(indexPath)
        }
        
        //        layout.scrollDirection = .vertical
        //        collectionView.collectionViewLayout = layout
        btnMenuOutlet.setImage(UIImage(named: "BACK"), for: .normal)
        btnBackOutlet.isHidden = true
        if concerns_Type == .All_Concerns {
            if vcMode == "more" {
                btnMenuOutlet.setImage(UIImage(named: "menu"), for: .normal)
                btnBackOutlet.isHidden = true
            }else{
                btnMenuOutlet.setImage(UIImage(named: "menu"), for: .normal)
                btnBackOutlet.isHidden = true
            }
        }
        
        
        // }
        
        //        }else{
        //
        //            layout.scrollDirection = .horizontal
        //            collectionView.collectionViewLayout = layout
        //
        //            btnBackOutlet.isHidden = true
        //            btnMenuOutlet.setImage(UIImage(named: "BACK"), for: .normal)
        //
        //        }
        
        
        //        if vcMode == "more"{
        //
        //            btnBackOutlet.isHidden = true
        //            btnMenuOutlet.setImage(UIImage(named: "BACK"), for: .normal)
        //            layout.scrollDirection = .vertical
        //            collectionView.collectionViewLayout = layout
        //
        //        }else{
        //
        //            btnBackOutlet.isHidden = false
        //            btnMenuOutlet.setImage(UIImage(named: "menu"), for: .normal)
        //            layout.scrollDirection = .horizontal
        //
        //            if vcMode == "normal" {
        //                if concerns_Type == .pain_feel || concerns_Type == .Swallowing || concerns_Type == .Nausea || concerns_Type == .Fatigue || concerns_Type == .Bowels || concerns_Type == .Urination{
        //                    layout.scrollDirection = .vertical
        //
        //                }
        //            }
        //
        //            collectionView.collectionViewLayout = layout
        //
        //        }
            self.collectionView.reloadData()
    }
}

extension Concerns : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if vcMode == "normal"{
            if concerns_Type == .Medication {
                if fromWholeCrushLiquid == "Whole /Crushed /Liquid".localized() {
                    return wholeCrushedLiquied_ProblemsList.count
                }else if fromTiming == "timing".localized(){
                    return timing_List.count
                }
                return healthTypeList_Medication.count
            }else if concerns_Type == .All_Concerns {
                return healthTypeList.count
            }else if concerns_Type == .Breathing_Problems {
                return breathing_ProblemsList.count
            }else if concerns_Type == .pain_feel {
                return healthPainFellList.count
            }else if concerns_Type == .Swallowing {
                return swallowing_ProblemsList.count
            }else if concerns_Type == .Nausea{
                return nausea_ProblemsList.count
            }else if concerns_Type == .Fatigue{
                return fatigue_ProblemsList.count
            }else if concerns_Type == .Bowels{
                return bowels_ProblemsList.count
            }else if concerns_Type == .Urination{
                return urination_ProblemsList.count
            }else if concerns_Type == .vision_problem {
                return vision_ProblemsList.count
            }else if concerns_Type == .Trach {
                return trash_list.count
            }else if concerns_Type == .Emotions_Feelings {
                return emotions_feelings_list.count
            }else if concerns_Type == .Topic_Board {
                return topicBoard_List.count
            }else if concerns_Type == .Needs_Board {
                return Needs_board_List.count
            }
            
            return healthTypeList.count
            //healthTypeList_Medication.count
        }else{
            //            return healthTypeListMore.count
            if comeFromhardToSwallow {
                return hardToSwallow_List.count
            }
            return healthTypeListMore.count
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        //        let getData = (vcMode == "normal") ? (isFrome == "Medication") ?  healthTypeList_Medication : healthTypeList : healthTypeListMore
        
        let getIdentifier = (vcMode == "normal") ? "Health_type_Cell" : "Health_type_Cell_More"
        
        if (vcMode == "normal") {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: getIdentifier, for: indexPath) as! Health_type_Cell
            
            cell.image.contentMode = .scaleToFill
            
            if concerns_Type == .pain_feel || concerns_Type == .Swallowing || concerns_Type == .Nausea || concerns_Type == .Fatigue || concerns_Type == .Bowels || concerns_Type == .Urination || concerns_Type == .vision_problem || concerns_Type == .Emotions_Feelings || concerns_Type == .Trach || concerns_Type == .Needs_Board || concerns_Type == .Topic_Board  {
                
                cell.image.contentMode = .scaleToFill
                if concerns_Type == .Topic_Board{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Topic_Board_Cell", for: indexPath) as! Topic_Board_Cell
                    let getData = topicBoard_List
                    cell.title.text = getData[indexPath.row].1
                    cell.title.font = UIFont.init(name: "Navigo-Regular", size: CGFloat(isIpad ? 30 : noOfCellsInRow == 6 ? 14 : 22))
                    if selectIndex != -1{
                        if indexPath.row == selectIndex{
                            cell.bgView.borderColor =  hexStringToUIColor(hex: "#0F9AF0")
                            cell.bgView.borderWidth = isIpad ? 8 : 4
                        }else{
                            cell.bgView.borderColor = .clear
                        }
                    }else{
                        cell.bgView.borderColor = .clear
                    }
                    
                    cell.bgView.dropShadow()
                    return cell
                }else{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Health_type_Cell", for: indexPath) as! Health_type_Cell
                    var getData = healthPainFellList
                    if concerns_Type == .All_Concerns {
                        if "No Concerns".localized() == healthTypeList[indexPath.row].1  {
                            cell.image.contentMode = .scaleAspectFit
                        }
                    }else if concerns_Type == .pain_feel {
                        getData = healthPainFellList
                    }else if concerns_Type == .Nausea{
                        getData = nausea_ProblemsList
                        //                    if "Constipation" == nausea_ProblemsList[indexPath.row].1 {
                        //                        cell.image.contentMode = .scaleToFill
                        //                    }
                        cell.image.contentMode = .scaleToFill
                    }else if concerns_Type == .Fatigue{
                        getData = fatigue_ProblemsList
                    }else if concerns_Type == .Bowels{
                        getData = bowels_ProblemsList
                    }else if concerns_Type == .Urination{
                        getData = urination_ProblemsList
                    }else if concerns_Type == .vision_problem{
                        getData = vision_ProblemsList
                        cell.image.contentMode = .scaleAspectFit
                        if getData[indexPath.row].1 == "Right Vision" || getData[indexPath.row].1 == "Left Vision" {
                            cell.image.contentMode = .scaleToFill
                        }
                    }
                    else if concerns_Type == .Trach{
                        getData = trash_list
                    }
                    else if concerns_Type == .Emotions_Feelings{
                        getData = emotions_feelings_list
                    }else if concerns_Type == .Needs_Board{
                        getData = Needs_board_List
                    }else {
                        getData = swallowing_ProblemsList
                        cell.image.contentMode = .scaleAspectFit
                    }
                    
                    cell.image.image = getData[indexPath.row].0
                    cell.title.text = getData[indexPath.row].1
                    cell.title.font = iconTitleFont
                    if selectIndex != -1{
                        if indexPath.row == selectIndex{
                            cell.bgView.borderColor =  hexStringToUIColor(hex: "#0F9AF0")
                            cell.bgView.borderWidth = isIpad ? 8 : 4
                        }else{
                            cell.bgView.borderColor = .clear
                        }
                    }else{
                        cell.bgView.borderColor = .clear
                    }
                    cell.bgView.dropShadow()
                    //cell.backgroundColor = .red
                    return cell
                }
                
            }
            
            if concerns_Type == .Medication {
                if fromWholeCrushLiquid == "Whole /Crushed /Liquid".localized() {
                    cell.image.image = wholeCrushedLiquied_ProblemsList[indexPath.row].0
                    cell.title.text = wholeCrushedLiquied_ProblemsList[indexPath.row].1
                    cell.title.font = iconTitleFont
                    cell.image.contentMode = .scaleToFill
                    
                }else if fromTiming == "timing".localized() {
                    cell.image.image = timing_List[indexPath.row].0
                    cell.title.text = timing_List[indexPath.row].1
                    cell.title.font = iconTitleFont
                    cell.image.contentMode = .scaleAspectFit
                }
                else{
                    cell.image.image = healthTypeList_Medication[indexPath.row].0
                    cell.title.text = healthTypeList_Medication[indexPath.row].1
                    cell.title.font = iconTitleFont
                    cell.image.contentMode = .scaleToFill
                    if "Information".localized() == healthTypeList_Medication[indexPath.row].1 {
                        cell.image.contentMode = .center
                    }
                }
                
            }else if concerns_Type == .All_Concerns {
                cell.image.image = healthTypeList[indexPath.row].0
                cell.title.text = healthTypeList[indexPath.row].1
                cell.title.font = iconTitleFont
                
                if "Other".localized() == healthTypeList[indexPath.row].1  {
                    cell.image.contentMode = .scaleAspectFit
                }
                if "No Concerns".localized() == healthTypeList[indexPath.row].1  {
                    cell.image.contentMode = .scaleAspectFit
                }
                if "Breathing / Coughing".localized() == healthTypeList[indexPath.row].1  {
                    cell.image.contentMode = .scaleAspectFit
                }
                
            }else if concerns_Type == .Breathing_Problems {
                cell.image.image = breathing_ProblemsList[indexPath.row].0
                cell.title.text = breathing_ProblemsList[indexPath.row].1
                cell.title.font = iconTitleFont
            }else if concerns_Type == .pain_feel {
                cell.image.image = healthPainFellList[indexPath.row].0
                cell.title.text = healthPainFellList[indexPath.row].1
                cell.title.font = iconTitleFont
            }else if concerns_Type == .Swallowing {
                cell.image.image = swallowing_ProblemsList[indexPath.row].0
                cell.title.text = swallowing_ProblemsList[indexPath.row].1
                cell.title.font = iconTitleFont
                if "Others".localized() == swallowing_ProblemsList[indexPath.row].1 {
                    cell.image.contentMode = .center
                }
            }else if concerns_Type == .vision_problem {
                cell.image.image = vision_ProblemsList[indexPath.row].0
                cell.title.text = vision_ProblemsList[indexPath.row].1
                cell.title.font = iconTitleFont
                cell.image.contentMode = .scaleAspectFit
                //                if "Others" == vision_ProblemsList[indexPath.row].1 {
                //                    cell.image.contentMode = .center
                //                }
            }else if concerns_Type == .Trach {
                cell.image.image = trash_list[indexPath.row].0
                cell.title.text = trash_list[indexPath.row].1
                cell.title.font = iconTitleFont
                //                if "Others" == vision_ProblemsList[indexPath.row].1 {
                //                    cell.image.contentMode = .center
                //                }
            }else if concerns_Type == .Emotions_Feelings {
                cell.image.image = emotions_feelings_list[indexPath.row].0
                cell.title.text = emotions_feelings_list[indexPath.row].1
                cell.title.font = iconTitleFont
                //                if "Others" == vision_ProblemsList[indexPath.row].1 {
                //                    cell.image.contentMode = .center
                //                }
            }else if concerns_Type == .Needs_Board {
                cell.image.image = Needs_board_List[indexPath.row].0
                cell.title.text = Needs_board_List[indexPath.row].1
                cell.title.font = iconTitleFont
                //                if "Others" == vision_ProblemsList[indexPath.row].1 {
                //                    cell.image.contentMode = .center
                //                }
            }
            
            
            if selectIndex != -1{
                if indexPath.row == selectIndex{
                    cell.bgView.borderColor =  hexStringToUIColor(hex: "#0F9AF0")
                    cell.bgView.borderWidth = isIpad ? 8 : 4
                }else{
                    cell.bgView.borderColor = .clear
                }
            }else{
                cell.bgView.borderColor = .clear
            }
            cell.bgView.dropShadow()
            return cell
        }else{
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: getIdentifier, for: indexPath) as! Health_type_Cell_More
            if comeFromhardToSwallow {
                cell.image.image = hardToSwallow_List[indexPath.row].0
                cell.title.text = hardToSwallow_List[indexPath.row].1
                cell.title.font = iconTitleFont
            }else{
                cell.image.image = healthTypeListMore[indexPath.row].0
                cell.title.text = healthTypeListMore[indexPath.row].1
                cell.title.font = iconTitleFont
                
                let image = healthTypeListMore[indexPath.row].1
                
                //|| "Other" == image
                if "No Concerns".localized() == image {
                    cell.image.contentMode = .scaleAspectFit
                }else{
                    cell.image.contentMode = .scaleToFill
                }
//                if healthTypeListMore[indexPath.row].1 == "Other" {
//                    cell.image.contentMode = .scaleAspectFit
//                }else{
//                    cell.image.contentMode = .scaleToFill
//                }
                if selectIndex != -1{
                    if indexPath.row == selectIndex{
                        cell.bgView.borderColor =  hexStringToUIColor(hex: "#0F9AF0")
                        cell.bgView.borderWidth = isIpad ? 8 : 4
                    }else{
                        cell.bgView.borderColor = .clear
                    }
                }else{
                    cell.bgView.borderColor = .clear
                }
                cell.bgView.dropShadow()
            }
            //cell.backgroundColor = .red
            cell.bgView.dropShadow()
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let collectionHeight = collectionView.bounds.height
        
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        //        flowLayout.collectionView?.isPagingEnabled = true
        let totalSpace = flowLayout.sectionInset.left
        + flowLayout.sectionInset.right
        + ( (noOfCellsInRow == 2 ? (isIpad ? collectionView.frame.height/5 : collectionView.frame.height/1.5) : flowLayout.minimumLineSpacing) * CGFloat((noOfCellsInRow == 6 ? 3 : noOfCellsInRow) - 1))
        
        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat((noOfCellsInRow == 6 ? 3 : noOfCellsInRow)))
        currentCellFrame = noOfCellsInRow == 1 ? Int(collectionHeight - (isIpad ? 40 : 20)) : size - (isIpad ? 25 : 12)
        
        self.GuideView_Category2WidthConstraint?.constant = CGFloat(currentCellFrame)
        self.GuideView_Category2TopConstraint.constant = collectionView.bounds.height/2 - CGFloat(currentCellFrame/2)
        return CGSize(width: noOfCellsInRow == 6 ? Int(collectionHeight/1.9 - (4 * flowLayout.minimumInteritemSpacing)) : currentCellFrame, height: noOfCellsInRow == 6 ? Int(collectionHeight/2.05 - (4 * flowLayout.minimumInteritemSpacing)) : currentCellFrame)
        
        
        //                if (vcMode == "normal"){
        //                    if concerns_Type == .pain_feel || concerns_Type == .Swallowing || concerns_Type == .Nausea || concerns_Type == .Fatigue || concerns_Type == .Bowels || concerns_Type == .Urination{
        //                        return CGSize(width: (collectionView.bounds.width / 3 - 20), height: 175)
        //                    }
        //                    return isIpad == true ? CGSize(width: collectionWidth/3, height: collectionWidth/3) : CGSize(width: collectionWidth - 9, height: collectionWidth - 9)
        //                }else{
        //                    return CGSize(width: (collectionView.bounds.width / 3 - 20), height: 175)
        //                }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        if noOfCellsInRow <= 2 {
            return isIpad ? 40 : 20
        }
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if noOfCellsInRow == 6 {
            return isIpad ? 20 : 8
        }
        return collectionView.bounds.height/2 - CGFloat(currentCellFrame/2)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        //
        //        KtFlowlayout.minimumInteritemSpacing = 10
        ////        layout.minimumLineSpacing = 401
        //        KtFlowlayout.minimumLineSpacing = collectionView.bounds.height/2 - CGFloat(currentCellFrame/2)
        ////        flowLayout.collectionView?.isPagingEnabled = true
        DispatchQueue.main.async {
            if abs(self.collectionView.frame.height - collectionViewLayout.collectionViewContentSize.height) <= 20{
                   self.indicatorBgView.isHidden = true
            }
        }
       
        if noOfCellsInRow <= 2 {
            let totalCellWidth = Int(currentCellFrame) * noOfCellsInRow
            let totalSpacingWidth = Int(isIpad ? 60 : 30) * (noOfCellsInRow - 1)
            
            let leftInset = (collectionView.bounds.width - CGFloat(totalCellWidth + totalSpacingWidth)) / 2
            let rightInset = leftInset
            return UIEdgeInsets(top: collectionView.bounds.height/2 - CGFloat(currentCellFrame/2), left: rightInset, bottom: collectionView.bounds.height/2 - CGFloat(currentCellFrame/2), right: rightInset)
        }else{
            if noOfCellsInRow == 6 {
                return UIEdgeInsets(top: 0, left: isIpad ? 25 : hasTopNotch ? screenHeight/3 : screenHeight/4, bottom: 0, right: isIpad ? 15 : hasTopNotch ? screenHeight/3 : screenHeight/4)
            }
            return UIEdgeInsets(top: collectionView.bounds.height/2 - CGFloat(currentCellFrame/2), left: 0, bottom: collectionView.bounds.height/2 - CGFloat(currentCellFrame/2), right: 0)
        }
       
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        self.zoomView.reset()
        selectIndex = indexPath.row
        collectionView.reloadData()
        
        let getData = (vcMode == "normal") ? (isFrome == "Medication") ?  healthTypeList_Medication : healthTypeList : (comeFromhardToSwallow == true ? hardToSwallow_List : healthTypeListMore)
        if concerns_Type == .All_Concerns {
            if vcMode != "more" {
                SummeryDict.removeAll()
                SummeryInfo.shared.removeAll()
            }
            let getTitle = getData[indexPath.row].1
            if getTitle == "Something Else".localized() && vcMode == "normal"{
                playSound_SomthingElse()
            }
            if getTitle == "Pain".localized() {
                if IS_MALE {
                    playSound(fileName:  SELECTED_LANG == "en" ? "Pain" : "pain_dolor", file_Extension: "wav")
                }else{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Pain_PainDis_En" : "Dolor (pain)_PainDis_Sp", file_Extension: "mp3")
                }
                
                
                if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                    
                    vc.concerns_Type = .Pain
                    vc.logoImage = getData[indexPath.row].0!
                    vc.typeTitle = getData[indexPath.row].1
                    self.navigationController?.pushViewController(vc, animated: false)
                }
                
            }else if getTitle == "Medication".localized() {
                
                if IS_MALE{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Medication" : "Medicación_Medication", file_Extension: "wav")
                }else{
                    playSound(fileName:  SELECTED_LANG == "en" ? "medication_Female_en" : "medicacion (medication)_Medication_Sp", file_Extension: "mp3")
                }
                
                if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                    
                    vc.concerns_Type = .Medication
                    vc.logoImage = getData[indexPath.row].0!
                    vc.typeTitle = getData[indexPath.row].1
                    if fromWholeCrushLiquid == "Whole /Crushed /Liquid".localized() {
                        vc.logoImage = wholeCrushedLiquied_ProblemsList[indexPath.row].0!
                        vc.typeTitle = wholeCrushedLiquied_ProblemsList[indexPath.row].1
                    }else if fromTiming == "timing".localized(){
                        vc.logoImage = timing_List[indexPath.row].0!
                        vc.typeTitle = timing_List[indexPath.row].1
                    }
                    self.navigationController?.pushViewController(vc, animated: false)
                }
            }else if getTitle == "Breathing / Coughing".localized() {
                if IS_MALE{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Breathing or Coughing" : "Respiración o tos_breathing or coughing", file_Extension: "wav")
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "Breathing or Coughing _B&C_En" : "respiracion o tos (breathing or coughing)_B&C_Sp" , file_Extension: "mp3")
                }
                if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                    
                    vc.concerns_Type = .Breathing_Problems
                    vc.logoImage = getData[indexPath.row].0!
                    vc.typeTitle = getData[indexPath.row].1
                    self.navigationController?.pushViewController(vc, animated: false)
                }
            }else if getTitle == "Swallowing".localized() {
                if IS_MALE{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Swallowing" : "tragar_swallowing", file_Extension: "wav")
                }else{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Swallowing_Swallowing_En" : "tragar (swallow)_female_Sp", file_Extension: "mp3")
                }
                
                
                if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                    
                    vc.concerns_Type = .Swallowing
                    vc.logoImage = getData[indexPath.row].0!
                    vc.typeTitle = getData[indexPath.row].1
                   
                    self.navigationController?.pushViewController(vc, animated: false)
                }
            }else if getTitle == "Nausea".localized() {
                
                if IS_MALE{
                    playSound(fileName: SELECTED_LANG == "en" ? "Nausea(Concerns page 2)" : "nausea_Nauseas", file_Extension: "wav")
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "Nausea_ Nausea_En" : "nauseas (nausea)_ Nausea_Sp", file_Extension: "mp3")
                }
                
                if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                    
                    vc.concerns_Type = .Nausea
                    vc.logoImage = getData[indexPath.row].0!
                    vc.typeTitle = getData[indexPath.row].1
                    
                    self.navigationController?.pushViewController(vc, animated: false)
                }
            }else if getTitle == "Fatigue".localized() {
                
                if IS_MALE{
                    playSound(fileName: SELECTED_LANG == "en" ? "Fatigue" : "Fatiga", file_Extension: "wav")
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "fatigue_Fatigue_En" : "fatiga (fatigue)_Fatigue_Sp", file_Extension: "mp3")
                }
                
                if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                    
                    vc.concerns_Type = .Fatigue
                    vc.logoImage = getData[indexPath.row].0!
                    vc.typeTitle = getData[indexPath.row].1
                    self.navigationController?.pushViewController(vc, animated: false)
                }
            }else if getTitle == "Bowels".localized() {
                
                if IS_MALE{
                    playSound(fileName: SELECTED_LANG == "en" ? "Bowels" : "Bowels_Intestinos (Joaquin Vignoli)" , file_Extension: "wav")
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "Bowels_Bowels_En" : "intestinos(bowels)_Bowels_Sp" , file_Extension: "mp3")
                }
                
                
                if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                    
                    vc.concerns_Type = .Bowels
                    vc.logoImage = getData[indexPath.row].0!
                    vc.typeTitle = getData[indexPath.row].1
                    self.navigationController?.pushViewController(vc, animated: false)
                }
            }else if getTitle == "Urination".localized() {
                
                if IS_MALE{
                    playSound(fileName: SELECTED_LANG == "en" ? "Urination" : "Micción_Urination", file_Extension: "wav")
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "Urination_Urination_En" : "miccion (urination)_Urination_Sp", file_Extension: "mp3")
                }
                
                
                if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                    
                    vc.concerns_Type = .Urination
                    vc.logoImage = getData[indexPath.row].0!
                    vc.typeTitle = getData[indexPath.row].1
                   
                    self.navigationController?.pushViewController(vc, animated: false)
                }
            }else if getTitle == "Emotions / Feelings".localized() {
                
                if IS_MALE{
                    if SELECTED_LANG == "en" {
                        playSound(fileName:  "Emotions or feelings_Male_en", file_Extension: "mp3")
                    }else{
                        playSound(fileName:  "Emotions_Feelings Emociones_sentimientos", file_Extension: "wav")
                    }
                    
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "Emotions or Feelings_E&F_En" : "Emociones_sentimientos_Sp_F" , file_Extension: "mp3")
                }
                
                if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                    
                    vc.concerns_Type = .Emotions_Feelings
                    vc.logoImage = getData[indexPath.row].0!
                    vc.typeTitle = getData[indexPath.row].1
                   
                    self.navigationController?.pushViewController(vc, animated: false)
                }
            }else if getTitle == "Needs Board".localized() {
                
                playSound(fileName: "Needs Board".localized(), file_Extension: "wav")
                
                //                if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                //
                //                    vc.concerns_Type = .Needs_Board
                //                    vc.logoImage = getData[indexPath.row].0!
                //                    vc.typeTitle = getData[indexPath.row].1
                //                    if vcMode == "more" {
                //                        SummeryDict["typeMore_2"] = summeryDetails(title: getData[indexPath.row].1, img: getData[indexPath.row].0!)
                //                    }else{
                //                        SummeryDict["type_1"] = summeryDetails(title: getData[indexPath.row].1, img: getData[indexPath.row].0!)
                //                    }
                //                    self.navigationController?.pushViewController(vc, animated: false)
                //                }
            }
            else if getTitle == "Vision Problems".localized() {
                if IS_MALE{
                    if SELECTED_LANG == "en" {
                        playSound(fileName: "Vision problems_male_en",file_Extension: "mp3")
                    }else{
                        playSound(fileName: "Vision problems_Problemas de la vista", file_Extension: "wav")
                    }
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "Vision Problems_VP_En" : "Problemas de la vista (vision problems)_VP_Sp", file_Extension: "mp3")
                }
                
                if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                    
                    vc.concerns_Type = .vision_problem
                    vc.logoImage = getData[indexPath.row].0!
                    vc.typeTitle = getData[indexPath.row].1
            
                    self.navigationController?.pushViewController(vc, animated: false)
                }
            }else if getTitle == "Trach".localized() {
                if IS_MALE{
                    if SELECTED_LANG == "en" {
                        playSound(fileName: "trach_male_en", file_Extension: "mp3")
                    }else{
                        playSound(fileName: "traqueotomia_trach", file_Extension: "wav")
                    }
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "trach_Trach_En" : "traqueotomia (trach)_Trach_Sp", file_Extension: "mp3")
                }
                
                if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                    
                    vc.concerns_Type = .Trach
                    vc.logoImage = getData[indexPath.row].0!
                    vc.typeTitle = getData[indexPath.row].1
                 
                    
                    self.navigationController?.pushViewController(vc, animated: false)
                }
            }else{
                
                if let title = healthTypeList[indexPath.row].1 as? String {
                    if title == "Something Else".localized() || title == "No Concerns".localized() {
                        
                        if title == "Something Else".localized() {
                            //                            if IS_MALE {
                            //                                playSound(fileName:  SELECTED_LANG == "en" ? "Other" : "otro_other", file_Extension: "wav")
                            //                            }else{
                            //                                playSound(fileName:  SELECTED_LANG == "en" ? "Other_Female_En" : "Otro_Female_Sp", file_Extension: "mp3")
                            //                            }
                            
                            
                            if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                                
                                vc.concerns_Type = .Other
                                vc.logoImage = UIImage(named: "concerns_others")!
                                vc.typeTitle = "Something Else".localized()
                                self.navigationController?.pushViewController(vc, animated: false)
                            }
                            //                        playSound(fileName: "No Concerns", file_Extension: "wav")
                        }else{
                            if IS_MALE {
                                playSound(fileName:  SELECTED_LANG == "en" ? "No Concerns" : "Sin preocupaciones(no concerns)_male_Sp", file_Extension: "wav")
                            }else{
                                playSound(fileName:  SELECTED_LANG == "en" ? "No Concerns_female_en" : "sin preocupaciones (no concerns)female_Sp", file_Extension: "mp3")
                            }
                            if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                                
                                vc.concerns_Type = .Other
                                vc.logoImage = UIImage(named: "health_good")!
                                vc.typeTitle = "No Concerns".localized()
                                self.navigationController?.pushViewController(vc, animated: false)
                            }
                        }
                        // TabbarObj.selectedIndex = 3
                        return
                    }
                }
            }
        }
        
        if !(vcMode == "normal") {
            
            if let title = healthTypeListMore[indexPath.row].1 as? String {
                if title == "Something Else".localized(){
                    playSound_SomthingElse()
                }
                if title == "Something Else".localized() || title == "No Concerns".localized() {
                    
                    if title == "Other" {
                        if IS_MALE {
                            playSound(fileName:  SELECTED_LANG == "en" ? "Other" : "otro_other", file_Extension: "wav")
                        }else{
                            playSound(fileName:  SELECTED_LANG == "en" ? "Other_Female_En" : "Otro_Female_Sp", file_Extension: "wav")
                        }
                        if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                            
                            vc.concerns_Type = .Other
                            vc.logoImage = UIImage(named: "concerns_others")!
                            vc.typeTitle = "Other"
                            self.navigationController?.pushViewController(vc, animated: false)
                        }
                        
                    }else if title == "Something Else".localized(){
                        if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                            
                            vc.concerns_Type = .Other
                            vc.logoImage = UIImage(named: "concerns_others")!
                            vc.typeTitle = "Something Else".localized()
                            self.navigationController?.pushViewController(vc, animated: false)
                        }
                    }else{
                        if IS_MALE {
                            playSound(fileName:  SELECTED_LANG == "en" ? "No Concerns" : "Sin preocupaciones(no concerns)_male_Sp", file_Extension: "wav")
                        }else{
                            playSound(fileName:  SELECTED_LANG == "en" ? "No Concerns_female_en" : "sin preocupaciones (no concerns)female_Sp", file_Extension: "mp3")
                        }
                        
                    }
                    
                    return
                }
                
            }
        }
        
        if concerns_Type == .pain_feel {
            
            let title = healthPainFellList[indexPath.row].1
            if title == "Something Else".localized(){
                playSound_SomthingElse()
            }
            if title == "Squeezing / Tight".localized() {
                if IS_MALE{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Squeezing or Tight" : "squeezing or tight_apretando o apretado", file_Extension: "wav")
                }else{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Squeezing or Tight_PainDis_En" : "apretando o apretado (squeezing or tight)_PainDis_Sp", file_Extension: "mp3")
                }
                
            }else if title == "Aching / Dull".localized() {
                if IS_MALE{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Aching or Dull" : "Dolorido o sordo(AchingAndDull)_Male_Sp", file_Extension: "wav")
                }else{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Aching or Dull_PainDis_En" : "dolorido o sorso (aching or dull)_PainDis_Sp", file_Extension: "mp3")
                }
            }else if title == "Burning".localized() {
                
                if IS_MALE{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Burning(Urination)" : "burning_quema 1", file_Extension: "wav")
                }else{
                    playSound(fileName:  SELECTED_LANG == "en" ? "burning_PainDis_En" : "Quema (burning)_PainDis_Sp", file_Extension: "mp3")
                }
            }else if title == "Sharp / Stabbing".localized() {
                
                if IS_MALE{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Sharp or Stabbing" : "sharp or stabbing_afilado o punzante", file_Extension: "wav")
                }else{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Sharp or stabbing_PainDis_En" : "afilado o punzante (sharp or stabbing)_PainDis_Sp", file_Extension: "mp3")
                }
            }else if title == "Numb / Tingling".localized() {
                
                if IS_MALE{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Numb or Tingling" : "numb or tingling_entumecimiento u hormigueo", file_Extension: "wav")
                }else{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Numb or tingling_PainDis_En" : "Entumecimiento u hormigueo (numb or tingling)_PainDis_Sp", file_Extension: "mp3")
                }
            }
            else if title == "Heavy".localized() {
                
                if IS_MALE{
                    if SELECTED_LANG == "en" {
                        playSound(fileName: "Heavy_male_en", file_Extension: "mp3")
                    }else{
                        playSound(fileName: "Heavy_pesado", file_Extension: "wav")
                    }
                    
                }else{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Heavy_PainDis_En" : "pesado (heavy)_PainDis_Sp", file_Extension: "mp3")
                }
            }else if title == "Cramping".localized() {
                
                if IS_MALE{
                    if SELECTED_LANG == "en" {
                        playSound(fileName: "Cramping_male_en", file_Extension: "mp3")
                    }else{
                        playSound(fileName: "cramping_calambres-2", file_Extension: "wav")
                    }
                    
                }else{
                    playSound(fileName:  SELECTED_LANG == "en" ? "cramping_PainDis_En" : "calambres (cramping)_PainDis_Sp", file_Extension: "mp3")
                }
            }
            else if title == "Spasming".localized() {
                
                if IS_MALE{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Spasming" : "spasming_espasmos", file_Extension: "wav")
                }else{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Spasming_PainDis_En" : "espasmos (spasming)_PainDis_Sp", file_Extension: "mp3")
                }
            }else if title == "Throbbing".localized() {
                
                if IS_MALE{
                    if SELECTED_LANG == "en" {
                        playSound(fileName: "throbbing_male_en", file_Extension: "mp3")
                    }else{
                        playSound(fileName: "throbbing_palpitante", file_Extension: "wav")
                    }
                }else{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Throbbing_PainDis_En" : "palpitante (throbbing)_PainDis_Sp", file_Extension: "mp3")
                }
            }
            else if title == "Pins and Needles".localized() {
                
                if IS_MALE{
                    
                    if SELECTED_LANG == "en" {
                        playSound(fileName: "Pins and needles_male_en", file_Extension: "mp3")
                    }else{
                        playSound(fileName: "Hormigueo doloroso(PinsAndNidles)_Male_Sp", file_Extension: "wav")
                    }
                }else{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Pins and Needles_PainDis_En" : "hormigueo doloroso (pins and needles)_PainDis_Sp", file_Extension: "mp3")
                }
            }
            else if title == "Shooting".localized() {
                
                if IS_MALE{
                    if SELECTED_LANG == "en" {
                        playSound(fileName: "Shooting_male_en", file_Extension: "mp3")
                    }else{
                        playSound(fileName: "Disparando(Shooting)_Male_Sp", file_Extension: "wav")
                    }
                }else{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Shooting_PainDis_En" : "disperando (shooting)_PainDis_Sp", file_Extension: "mp3")
                }
            }
            else if title == "Itching".localized() {
                
                if IS_MALE{
                    if SELECTED_LANG == "en" {
                        playSound(fileName: "Itching_male_en", file_Extension: "mp3")
                    }else{
                        playSound(fileName: "Comezon(itching)_Male_Sp", file_Extension: "wav")
                    }
                }else{
                    playSound(fileName:  SELECTED_LANG == "en" ? "itching_PainDis_En" : "Comezón(Itching)_PainDis_Sp", file_Extension: "mp3")
                }
            } else{
                
            }
            
            if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                
                vc.goToPage = "Pain_meter"
                vc.concerns_Type = .Pain_meter
                vc.logoImage = healthPainFellList[indexPath.row].0!
                vc.typeTitle = healthPainFellList[indexPath.row].1
                self.navigationController?.pushViewController(vc, animated: false)
            }
            
        }else if concerns_Type == .Medication {
            var title = ""
            title = healthTypeList_Medication[indexPath.row].1
            if title == "Something Else".localized(){
                isMedicationLastIcon = true
                playSound_SomthingElse()
            }
            if fromWholeCrushLiquid == "Whole /Crushed /Liquid".localized() {
                title = wholeCrushedLiquied_ProblemsList[indexPath.row].1
                isMedicationLastIcon = true
                if title == "Whole".localized(){
                    
                    if IS_MALE{
                        if SELECTED_LANG == "en" {
                            playSound(fileName: "whole_Male_en", file_Extension: "mp3")
                        }else{
                            playSound(fileName: "Entero(whole)_MaleSp", file_Extension: "wav")
                        }
                    }else{
                        playSound(fileName: SELECTED_LANG == "en" ? "whole_Medication_En" : "entero (whole)_Medication_Sp", file_Extension: "mp3")
                    }
                }else if title == "Crushed".localized(){
                    
                    if IS_MALE{
                        if SELECTED_LANG == "en" {
                            playSound(fileName: "crushed_Male_en", file_Extension: "mp3")
                        }else{
                            playSound(fileName: "Aplastado_crushed", file_Extension: "mp3")
                        }
                        
                    }else{
                        playSound(fileName: SELECTED_LANG == "en" ? "crushed_Medication_En" : "aplastado (crushed)_Medication_Sp", file_Extension: "mp3")
                    }
                }else if title == "Liquid".localized(){
                    
                    if IS_MALE{
                        if SELECTED_LANG == "en" {
                            playSound(fileName: "liquid_Male_en", file_Extension: "mp3")
                        }else{
                            playSound(fileName: "liquido_liquid", file_Extension: "mp3")
                        }
                    }else{
                        playSound(fileName: SELECTED_LANG == "en" ? "liquid_Medication_En" : "liquido (liquid)_Medication_Sp", file_Extension: "mp3")
                    }
                }
                
                if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                    
                    vc.isShowSummryBtn = isMedicationLastIcon
                    vc.goToPage = "White Board"
                    vc.concerns_Type = .Medication
                    vc.logoImage = wholeCrushedLiquied_ProblemsList[indexPath.row].0!
                    vc.typeTitle = wholeCrushedLiquied_ProblemsList[indexPath.row].1
                    self.navigationController?.pushViewController(vc, animated: false)
                }
                return
            }else if fromTiming == "timing".localized(){
                title = timing_List[indexPath.row].1
                isMedicationLastIcon = true
                if title == "Schedule Pain Meds".localized(){
                    
                    if IS_MALE{
                        if SELECTED_LANG == "en" {
                            playSound(fileName: "scheduled pain meds_Male_en", file_Extension: "mp3")
                        }else{
                            playSound(fileName: "medicamentos para el dolor programados_scheduled pain meds_male_Sp", file_Extension: "wav")
                        }
                    }else{
                        playSound(fileName: SELECTED_LANG == "en" ? "scheduled pain meds_Medication_En" : "Medicamentos para el dolor programados (scheduled pain meds)_Medication_Sp", file_Extension: "mp3")
                    }
                }else if title == "Different Times".localized(){
                    if IS_MALE{
                        if SELECTED_LANG == "en" {
                            playSound(fileName: "different times_Male_en", file_Extension: "mp3")
                        }else{
                            playSound(fileName: "tiempos diferentes_different times_male_Sp", file_Extension: "wav")
                        }
                    }else{
                        playSound(fileName: SELECTED_LANG == "en" ? "different times_Medication_En" : "tiempos diferentes (different times)_Medication_Sp", file_Extension: "mp3")
                    }
                }else if title == "Spread Out".localized(){
                    
                    if IS_MALE{
                        if SELECTED_LANG == "en" {
                            playSound(fileName: "spread out_Male_en", file_Extension: "mp3")
                        }else{
                            playSound(fileName: "Repartidos en el tiempo(spread out)_Male_Sp", file_Extension: "wav")
                        }
                    }else{
                        playSound(fileName: SELECTED_LANG == "en" ? "Spread out_Medication_En" : "repartidos en el tiempo (spread out)_Medication_Sp", file_Extension: "mp3")
                    }
                }
                
                if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                    
                    vc.goToPage = "White Board"
                    vc.concerns_Type = .Medication
                    vc.isShowSummryBtn = isMedicationLastIcon
                    vc.logoImage = timing_List[indexPath.row].0!
                    vc.typeTitle = timing_List[indexPath.row].1
                    self.navigationController?.pushViewController(vc, animated: false)
                }
                return
            }
            if title == "Side Effects".localized() {
                
                if IS_MALE{
                    if SELECTED_LANG == "en" {
                        playSound(fileName: "side effects_Male_en", file_Extension: "mp3")
                    }else{
                        playSound(fileName: "Efectos Secundarios_Side Effects", file_Extension: "wav")
                    }
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "Side effects_Medication_En" : "efectos secundarios (side effects)_Medication_Sp", file_Extension: "mp3")
                }
            }else if title == "Hard to Swallow".localized(){
                
                if IS_MALE{
                    if SELECTED_LANG == "en" {
                        playSound(fileName: "hard to swallow_Male_en", file_Extension: "mp3")
                    }else{
                        playSound(fileName: "Dificil de tragar_Hard to Swallow(Joaquin Vignoli)", file_Extension: "wav")
                    }
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "hard to swallow_Medication_En" : "dificil de tragar (trouble swallowing)_Medication_Sp", file_Extension: "mp3")
                }
            }
            else if title == "Whole /Crushed /Liquid".localized(){
                
                if IS_MALE{
                    if SELECTED_LANG == "en" {
                        playSound(fileName: "whole, crushed, or liquid_Male_en", file_Extension: "mp3")
                    }else{
                        playSound(fileName: "Enteros, triturados, o liquids - revised", file_Extension: "mp3")
                    }
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "whole, crushed, or liquid_Medication_En" : "enteros, aplastados, o liquidos_Medication_Sp", file_Extension: "mp3")
                }
            }else if title == "Too Little".localized(){
                isMedicationLastIcon = true
                if IS_MALE{
                    playSound(fileName: SELECTED_LANG == "en" ? "Too Little" : "muy poco_too little", file_Extension: "wav")
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "Too little_Medication_En" : "muy poco (too little_few)_Medication_Sp", file_Extension: "mp3")
                }
            }else if title == "Don't Want".localized(){
                isMedicationLastIcon = true
                if IS_MALE{
                    if SELECTED_LANG == "en" {
                        playSound(fileName: "don't want_Male_en", file_Extension: "mp3")
                    }else{
                        playSound(fileName: "No quiero_don't want", file_Extension: "wav")
                    }
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "Don't want_Medication_En" : "No quiero (don't want)_Medication_Sp", file_Extension: "mp3")
                }
            }
            else if title == "Timing".localized(){
                
                if IS_MALE{
                    if SELECTED_LANG == "en" {
                        playSound(fileName: "timing_Male_en", file_Extension: "mp3")
                    }else{
                        playSound(fileName: "tiempo_timing", file_Extension: "wav")
                    }
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "Timing_Medication_En" : "tiempo (timing)_Medication_Sp", file_Extension: "mp3")
                }
            }
            else if title == "Too Much".localized(){
                isMedicationLastIcon = true
                if IS_MALE{
                    playSound(fileName: SELECTED_LANG == "en" ? "Too Much" : "demasiado_too much", file_Extension: "wav")
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "too much_Medication_En" : "demasiado (too much)_Medication_Sp", file_Extension: "mp3")
                }
            }else if title == "Information".localized(){
                isMedicationLastIcon = false
                if IS_MALE{
                    playSound(fileName: SELECTED_LANG == "en" ? "Information" : "informacion_information", file_Extension: "wav")
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "information_Medication_En" : "informacion (information)_Medication_Sp", file_Extension: "mp3")
                }
            }
            else{
                //                playSound(fileName: title, file_Extension: "wav")
            }
            
            if title == "Too Much".localized() || title == "Too Little".localized(){
                isMedicationLastIcon = true
                if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {

                    vc.goToPage = "White Board"
                    vc.concerns_Type = .Medication
                    vc.isShowSummryBtn = isMedicationLastIcon
                    vc.logoImage = healthTypeList_Medication[indexPath.row].0!
                    vc.typeTitle = healthTypeList_Medication[indexPath.row].1
                    self.navigationController?.pushViewController(vc, animated: false)
                }









                return
            }else
            if title == "Whole /Crushed /Liquid".localized(){
                if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                    
                    vc.goToPage = "Whole /Crushed /Liquid".localized()
                    vc.concerns_Type = .Medication
                    vc.logoImage = healthTypeList_Medication[indexPath.row].0!
                    vc.typeTitle = healthTypeList_Medication[indexPath.row].1
                    self.navigationController?.pushViewController(vc, animated: false)
                }
                
                //                else if title == "Whole".localized() || title == "Crushed".localized() || title == "Liquid".localized(){
                //                    if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                //
                //                        vc.goToPage = "Whole /Crushed /Liquid"
                //                        vc.concerns_Type = .Medication
                //                        vc.logoImage = healthTypeList_Medication[indexPath.row].0!
                //                        vc.typeTitle = healthTypeList_Medication[indexPath.row].1
                //                        self.navigationController?.pushViewController(vc, animated: false)
                //                    }
                
            }else if title == "Timing".localized(){
                if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                    
                    vc.goToPage = "Timing".localized()
                    vc.concerns_Type = .Medication
                    vc.isShowSummryBtn = isMedicationLastIcon
                    vc.logoImage = healthTypeList_Medication[indexPath.row].0!
                    vc.typeTitle = healthTypeList_Medication[indexPath.row].1
                    self.navigationController?.pushViewController(vc, animated: false)
                }
            }else{
                if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                    
                    vc.goToPage = "Concerns 2"
                    vc.concerns_Type = .Medication
                    vc.isShowSummryBtn = isMedicationLastIcon
                    vc.logoImage = healthTypeList_Medication[indexPath.row].0!
                    vc.typeTitle = healthTypeList_Medication[indexPath.row].1
                    
                  
                    self.navigationController?.pushViewController(vc, animated: false)
                }
            }
            
            
        }else if concerns_Type == .Nausea {
            
            var title = nausea_ProblemsList[indexPath.row].1
            if title == "Something Else".localized(){
                playSound_SomthingElse()
            }
            if title == "With Medication".localized() {
                
                if IS_MALE{
                    playSound(fileName: SELECTED_LANG == "en" ? "With Medication(Fatigue)_com" : "con medicacion", file_Extension: "wav")
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "With medication_ Nausea_En" : "con medicacion (with medication)_ Nausea_Sp", file_Extension: "mp3")
                }
            }else if title == "Constipation".localized(){
                
                if IS_MALE{
                    playSound(fileName: SELECTED_LANG == "en" ? "Constipation" : "constipation_estrenimiento(Nausea)", file_Extension: "wav")
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "Constipation_ Nausea_En" : "estrenimiento (constipation)_ Nausea_Sp", file_Extension: "mp3")
                }
            }else if title == "Diarrhea".localized(){
                
                if IS_MALE{
                    playSound(fileName: SELECTED_LANG == "en" ? "Diarrhea" : "diarrhea_diarrea", file_Extension: "wav")
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "diarrhea_ Nausea_En" : "diarrea (diarrhea)_ Nausea_Sp", file_Extension: "mp3")
                }
            }else if title == "Cramping".localized(){
                
                if IS_MALE{
                    if SELECTED_LANG == "en"{
                        playSound(fileName: "Cramping_male_en", file_Extension: "mp3")
                    }else{
                        playSound(fileName: "cramping_calambres", file_Extension: "wav")
                    }
                    
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "cramping_ Nausea_En" : "calambras (cramping)_ Nausea_Sp", file_Extension: "mp3")
                }
            }else if title == "Vomiting".localized(){
                if IS_MALE{
                    playSound(fileName: SELECTED_LANG == "en" ? "Vomiting" : "vomiting_vomitos", file_Extension: "wav")
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "vomiting _ Nausea_En" : "vomitos (vomiting)_ Nausea_Sp", file_Extension: "mp3")
                }
            }else if title == "With Food/Drink".localized(){
                
                if IS_MALE{
                    playSound(fileName: SELECTED_LANG == "en" ? "With Food or Drink" : "with food and drink_con comida y bebida", file_Extension: "wav")
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "With food and drink_ Nausea_En" : "Con comida y bebida (with food and drink)_ Nausea_Sp", file_Extension: "mp3")
                }
            }
            else if title == "With Food".localized(){
                
                
                if IS_MALE{
                    playSound(fileName: SELECTED_LANG == "en" ? "With Food(Nausea)" : "with food_con comida(Nausea)", file_Extension: "wav")
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "with food_ Nausea_En" : "con comida (with food)_ Nausea_Sp", file_Extension: "mp3")
                }
            }
            else{
                //                playSound(fileName: title, file_Extension: "wav")
            }
            
            if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                
                vc.goToPage = "How_often"
                vc.concerns_Type = .Nausea
                vc.logoImage = nausea_ProblemsList[indexPath.row].0!
                vc.typeTitle = nausea_ProblemsList[indexPath.row].1
                
              
                self.navigationController?.pushViewController(vc, animated: false)
            }
            
        }else if concerns_Type == .Topic_Board {
            let title = topicBoard_List[indexPath.row].1
            if title == "Something Else".localized(){
                playSound_SomthingElse()
            }
            if title == "Discharge".localized() {
                
                if IS_MALE{
                    playSound(fileName: SELECTED_LANG == "en" ? "discharge_male_en" : "Dar de Alta_TB_Male_Sp", file_Extension: "mp3")
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "discharge_TopicBoard_En" : "dar de alta (discharge)_TB_Sp", file_Extension: "mp3")
                }
                
            }else if title == "Will I Get Better?".localized(){
                
                if IS_MALE{
                    playSound(fileName: SELECTED_LANG == "en" ? "Will I get better_Male_en" : "Mejorare_TB_Male_Sp", file_Extension: "mp3")
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "Will I get better_TopicBoard_En" : "¿mejoraré_(Will I get better)_TB_Sp", file_Extension: "mp3")
                }
                
            }else if title == "Therapy".localized(){
                
                if IS_MALE{
                    playSound(fileName: SELECTED_LANG == "en" ? "therapy_male_en" : "Terapia_TB_Male_Sp", file_Extension: "mp3")
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "therapy_TopicBoard_En" : "terapia (therapy)_TB_Sp", file_Extension: "mp3")
                }
                
            }else if title == "Communication".localized(){
                
                if IS_MALE{
                    playSound(fileName: SELECTED_LANG == "en" ? "communication_male_en" : "Comunicacion v2_TB_Male_Sp", file_Extension: "mp3")
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "Communication_TopicBoard_En" : "Comunicacion (communiation)_TB_Sp", file_Extension: "mp3")
                }
                
            }else if title == "Driving".localized(){
                if IS_MALE{
                    playSound(fileName: SELECTED_LANG == "en" ? "driving_male_en" : "Conduciendo_TB_Male_Sp", file_Extension: "mp3")
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "Driving_TopicBoard_En" : "conduciendo (driving)_TB_Sp", file_Extension: "mp3")
                }
                
            }else if title == "Home Set-Up".localized(){
                
                if IS_MALE{
                    playSound(fileName: SELECTED_LANG == "en" ? "home set-up_male_en" : "Configuracion del hogar v2_TB_Male_Sp", file_Extension: "mp3")
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "home set-up_TopicBoard_En" : "Configuración del hogar (home set up)_TB_Sp", file_Extension: "mp3")
                }
                
            }
            else if title == "Stroke Prevention".localized(){
                
                
                if IS_MALE{
                    playSound(fileName: SELECTED_LANG == "en" ? "stroke prevention_male_en" : "Prevencion de accidentes cerebrovasculares v2_TB_Male_Sp", file_Extension: "mp3")
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "stroke prevention_TopicBoard_En" : "Prevención de accidentes cerebrovasculares (stroke prevention)_TB_Sp", file_Extension: "mp3")
                }
                
            }
            else if title == "Family Support".localized(){
                
                
                if IS_MALE{
                    playSound(fileName: SELECTED_LANG == "en" ? "family support_male_en" : "Apoyo de la familia v2_TB_Male_Sp", file_Extension: "mp3")
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "family support_TopicBoard_En" : "apoyo de la familia (family support)_TB_Sp", file_Extension: "mp3")
                }
                
            }
            else if title == "Feeding Tube".localized(){
                
                if IS_MALE{
                    playSound(fileName: SELECTED_LANG == "en" ? "feeding tube_male_en" : "Tubo de alimentacion v2_TB_Male_Sp", file_Extension: "mp3")
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "feeding tube_TopicBoard_En" : "Tubo de alimentación (feeding tube)_TB_Sp", file_Extension: "mp3")
                }
                
            }
            else if title == "Trach".localized(){
                
                
                if IS_MALE{
                    if SELECTED_LANG == "en" {
                        playSound(fileName: "trach_male_en", file_Extension: "mp3")
                    }else{
                        playSound(fileName: "traqueotomia_trach", file_Extension: "wav")
                    }
                    
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "trach_TopicBoard_En" : "traqueotomia (trach)_B&C_Sp", file_Extension: "mp3")
                }
                
            }
            else if title == "Money".localized(){
                
                if IS_MALE{
                    playSound(fileName: SELECTED_LANG == "en" ? "money_male_en" : "Dinero_TB_Male_Sp", file_Extension: "mp3")
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "Money_TopicBoard_En" : "dinero_TB_Sp", file_Extension: "mp3")
                }
                
            }
            else if title == "Follow Up Appointments".localized(){
                
                
                if IS_MALE{
                    playSound(fileName: SELECTED_LANG == "en" ? "follow up appointments_male_en" : "Citas de seguimiento v2_TB_Male_Sp", file_Extension: "mp3")
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "Follow up appointments_TopicBoard_En" : "Citas de seguimiento (follow up appointments)_TB_Sp", file_Extension: "mp3")
                }
                
            }
            else if title == "Household Activity".localized(){
                
                
                if IS_MALE{
                    playSound(fileName: SELECTED_LANG == "en" ? "household activity_male_en" : "Actividad del hogar v2_TB_Male_Sp", file_Extension: "mp3")
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "Household activity_TopicBoard_En" : "actividad del hogar (household activity)_TB_Sp", file_Extension: "mp3")
                }
                
            }
            else if title == "Community Activity".localized(){
                
                if IS_MALE{
                    playSound(fileName: SELECTED_LANG == "en" ? "community activity_male_en" : "Actividad de la comunidad v2_TB_Male_Sp", file_Extension: "mp3")
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "community activity_TopicBoard_En" : "Actividad de la comunidad (community activity)_TB_Sp", file_Extension: "mp3")
                }
                
            }
            else if title == "Work".localized(){
                if IS_MALE{
                    playSound(fileName: SELECTED_LANG == "en" ? "work_male_en" : "Trabajo(work)_male_Sp", file_Extension: "mp3")
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "Work_TopicBoard_En" : "trabajo (work)_TB_Sp", file_Extension: "mp3")
                }
            }
            else if title == "Sex".localized(){
                
                if IS_MALE{
                    playSound(fileName: SELECTED_LANG == "en" ? "sex_male_en" : "Sexo_TB_Male_Sp", file_Extension: "mp3")
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "sex_TopicBoard_En" : "sexo (sex)_TB_Sp", file_Extension: "mp3")
                }
                
            }
            else if title == "Family".localized(){
                
                
                if IS_MALE{
                    playSound(fileName: SELECTED_LANG == "en" ? "family_male_en" : "Familia_TB_Male_Sp", file_Extension: "mp3")
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "family_TopicBoard_En" : "familia (family)_TB_Sp", file_Extension: "mp3")
                }
                
            }
            else if title == "Raising Children".localized(){
                
                
                if IS_MALE{
                    playSound(fileName: SELECTED_LANG == "en" ? "raising children_male_en" : "Criar a los niños_TB_Male_Sp", file_Extension: "mp3")
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "raising children_TopicBoard_En" : "criar a los niños (raising children)_TB_Sp", file_Extension: "mp3")
                }
            }
            else if title == "Food".localized(){
                
                if IS_MALE{
                    playSound(fileName: SELECTED_LANG == "en" ? "food_male_en" : "Comida_TB_Male_Sp", file_Extension: "mp3")
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "food_TopicBoard_En" : "comida (food)_TB_Sp", file_Extension: "mp3")
                }
            }
            else if title == "Alcohol".localized(){
                
                if IS_MALE{
                    playSound(fileName: SELECTED_LANG == "en" ? "alcohol_male_en" : "Alcohol_TB_Male_Sp", file_Extension: "mp3")
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "alcohol_TopicBoard_En" : "alcohol (alcohol)_TB_Sp", file_Extension: "mp3")
                }
                
            }
            else{
                //                playSound(fileName: "", file_Extension: "wav")
            }
            
            if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                
                vc.goToPage = "White Board"
                vc.concerns_Type = .Topic_Board
                vc.logoImage = topicBoard_List[indexPath.row].0!
                vc.typeTitle = topicBoard_List[indexPath.row].1
                vc.topicTitleText = topicBoard_List[indexPath.row].1
                vc.topicSubTitleText = SELECTED_LANG == "en" ? "" : topicBoard_List[indexPath.row].2
                
                self.navigationController?.pushViewController(vc, animated: false)
            }
            
        }else if concerns_Type == .Fatigue {
            
            let title = fatigue_ProblemsList[indexPath.row].1
            if title == "Something Else".localized(){
                playSound_SomthingElse()
            }
            if title == "All Day".localized() {
                
                if IS_MALE{
                    playSound(fileName: SELECTED_LANG == "en" ? "All Day(Fatigue)" : "todo el dia", file_Extension: "wav")
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "all day_Fatigue_En" : "todo el dia (all day)_Fatigue_Sp", file_Extension: "mp3")
                }
            }else if title == "With Medication".localized() {
                
                if IS_MALE{
                    playSound(fileName: SELECTED_LANG == "en" ? "With Medication(Fatigue)" : "con medicacion", file_Extension: "wav")
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "with medication_Fatigue_En" : "con medicacion (with medication)_Fatigue_Sp", file_Extension: "mp3")
                }
            }else if title == "With Activity".localized() {
                
                if IS_MALE{
                    playSound(fileName: SELECTED_LANG == "en" ? "With Activity(How Often)" : "con actividad", file_Extension: "wav")
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "with activity_Fatigue_En" : "con actividad (with activity)_Fatigue_Sp", file_Extension: "mp3")
                }
            }else if title == "More Than Usual".localized() {
                
                if IS_MALE{
                    playSound(fileName: SELECTED_LANG == "en" ? "More Than Usual" : "mas de lo habitual", file_Extension: "wav")
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "more than usual_Fatigue_En" : "mas de lo habitual (more than usual)_Fatigue_Sp", file_Extension: "mp3")
                }
            }else if title == "Trouble Sleeping".localized() {
                
                if IS_MALE{
                    playSound(fileName: SELECTED_LANG == "en" ? "Trouble Sleeping" : "problemas para dormir", file_Extension: "wav")
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "trouble sleeping_Fatigue_En" : "problemas para dormir (trouble sleeping)_Fatigue_Sp", file_Extension: "mp3")
                }
            }else if title == "Wake up Tired".localized() {
                
                if IS_MALE{
                    playSound(fileName: SELECTED_LANG == "en" ? "Wake Up Tired" : "Despertar cansado(wake up tired)_Male_Sp", file_Extension: "wav")
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "wake up tired_Fatigue_En" : "despertar cansado (wake up tired)_Fatigue_Sp", file_Extension: "mp3")
                }
            }else{
                //                playSound(fileName: title, file_Extension: "wav")
                
            }
            
            
            
            if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                
                vc.goToPage = "How_often"
                vc.concerns_Type = .Fatigue
                vc.logoImage = fatigue_ProblemsList[indexPath.row].0!
                vc.typeTitle = fatigue_ProblemsList[indexPath.row].1
                
                
              
                self.navigationController?.pushViewController(vc, animated: false)
            }
            
            
        }else if concerns_Type == .Breathing_Problems {
            
            let title = breathing_ProblemsList[indexPath.row].1
            if title == "Something Else".localized(){
                playSound_SomthingElse()
            }
            if title == "Choking".localized() {
                
                if IS_MALE{
                    playSound(fileName: SELECTED_LANG == "en" ? "Choking" : "asfixia_choking", file_Extension: "wav")
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "choking_B&C_En" : "asfixia (choking)_B&C_Sp" , file_Extension: "mp3")
                }
            }else if title == "Shortness of Breath".localized() {
                
                if IS_MALE{
                    playSound(fileName: SELECTED_LANG == "en" ? "Shortness of Breath" : "falta de aliento_shortness of breath", file_Extension: "wav")
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "shortness of breath_B&C_En" : "falta de aliento (shortness of breath)_B&C_Sp" , file_Extension: "mp3")
                }
            }else if title == "Coughing".localized() {
                if IS_MALE{
                    if SELECTED_LANG == "en"{
                        playSound(fileName: "Coughing", file_Extension: "wav")
                    }else{
                        playSound(fileName: "tos-coughing", file_Extension: "mp3")
                    }
                    
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "coughing _B&C_En" : "tos (coughing)_B&C_Sp" , file_Extension: "mp3")
                    
                }
            }else if title == "Trach".localized(){
                
                if IS_MALE{
                    if SELECTED_LANG == "en" {
                        playSound(fileName: "trach_male_en", file_Extension: "mp3")
                    }else{
                        playSound(fileName: "traqueotomia_trach", file_Extension: "wav")
                    }
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "trach_B&C_En" : "traqueotomia (trach)_B&C_Sp" , file_Extension: "mp3")
                }
            }else if title == "Chest Pain".localized() {
                
                if IS_MALE{
                    playSound(fileName: SELECTED_LANG == "en" ? "Chest Pain" : "dolor de pecho_chest pain", file_Extension: "wav")
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "chest pain_B&C_En" : "dolor de pecho (chest pain)_B&C_Sp" , file_Extension: "mp3")
                }
            }else{
                //                playSound(fileName: title, file_Extension: "wav")
            }
            
            
            
            if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                
                vc.goToPage = "How_often"
                vc.concerns_Type = .Breathing_Problems
                vc.logoImage = breathing_ProblemsList[indexPath.row].0!
                vc.typeTitle = breathing_ProblemsList[indexPath.row].1
                
              
                self.navigationController?.pushViewController(vc, animated: false)
            }
            
            
        }else if concerns_Type == .Bowels {
            
            var title = bowels_ProblemsList[indexPath.row].1
            if title == "Something Else".localized(){
                playSound_SomthingElse()
            }
            if title == "Blood".localized() {
                
                if IS_MALE{
                    playSound(fileName: SELECTED_LANG == "en" ? "Blood(Urination)" : "Blood_Sangre", file_Extension: "wav")
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "Blood_Bowels_En" : "sangre(blood)_Bowels_Sp" , file_Extension: "mp3")
                }
                //                playSound(fileName: "Blood".localized(), file_Extension: "wav")
            }else if title == "Diarrhea".localized(){
                
                if IS_MALE{
                    playSound(fileName: SELECTED_LANG == "en" ? "Diarrhea" : "diarrhea_diarrea", file_Extension: "wav")
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "Diarrhea_Bowels_En" : "diarrea(diarrhea)_Bowels_Sp" , file_Extension: "mp3")
                }
            }else if title == "Cramping".localized(){
                if IS_MALE{
                    if SELECTED_LANG == "en"{
                        playSound(fileName: "Cramping_male_en", file_Extension: "mp3")
                    }else{
                        playSound(fileName: "cramping_calambres", file_Extension: "wav")
                    }
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "cramping_ Nausea_En" : "calambras (cramping)_ Nausea_Sp", file_Extension: "mp3")
                }
            }else if title == "Bed Pan".localized(){
                
                if IS_MALE{
                    if SELECTED_LANG == "en" {
                        playSound(fileName: "bed pan_Male_en", file_Extension: "mp3")
                    }else{
                        playSound(fileName: "Bed pan_bandeja de cama", file_Extension: "wav")
                    }
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "Bed pan_Bowels_En" : "bandeja_de_cama(bed pan)_Bowels_Sp", file_Extension: "mp3")
                }
            }else if title == "Constipation".localized(){
                
                if IS_MALE{
                    playSound(fileName: SELECTED_LANG == "en" ? "Constipation" : "constipation_estreñimiento", file_Extension: "wav")
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "Constipation_Bowels_En" : "estrenimiento(constipation)_Bowels_Sp", file_Extension: "mp3")
                }
            }else if title == "Gas/Bloating".localized(){
                
                if IS_MALE{
                    if SELECTED_LANG == "en" {
                        playSound(fileName: "gas or bloating_Male_en", file_Extension: "mp3")
                    }else{
                        playSound(fileName: "Gas_hinchazon_gas_bloating", file_Extension: "wav")
                    }
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "Gas or bloating_Bowels_En" : "gas_hinchazon(gas_bloating)_Bowels_Sp", file_Extension: "mp3")
                }
            }else{
                //                playSound(fileName: title, file_Extension: "wav")
            }
            
            if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                
                vc.goToPage = "How_often"
                vc.concerns_Type = .Bowels
                vc.logoImage = bowels_ProblemsList[indexPath.row].0!
                vc.typeTitle = bowels_ProblemsList[indexPath.row].1
                
                
              
                self.navigationController?.pushViewController(vc, animated: false)
            }
            
        }else if concerns_Type == .Swallowing {
            
            var title = swallowing_ProblemsList[indexPath.row].1
            if title == "Something Else".localized(){
                playSound_SomthingElse()
            }
            if title == "Heartburn".localized(){
                
                if IS_MALE{
                    if SELECTED_LANG == "en"{
                        playSound(fileName: "Heartburn", file_Extension: "wav")
                    }else{
                        playSound(fileName: "Acidez estomacal_heartburn", file_Extension: "mp3")
                    }
                }else{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Heartburn_Swallowing_En" : "Acidez estomacal (heartburn)_Swallowing_Sp", file_Extension: "mp3")
                }
            }else if title == "Something Else".localized(){
                
                playSound_SomthingElse()
            }else if title == "Choking".localized(){
                
                if IS_MALE{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Choking" : "asfixia_choking", file_Extension: "wav")
                }else{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Choking_Swallowing_En" : "asfixia (choking)_Swallowing_Sp", file_Extension: "mp3")
                }
            }else if title == "Food Sticking".localized(){
                
                if IS_MALE{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Food Sticking" : "comida que se pega_food sticking", file_Extension: "wav")
                }else{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Food Sticking_Swallowing_En" : "comida que se pega (food sticking)_Swallowing_Sp", file_Extension: "mp3")
                }
            }else if title == "Coughing".localized(){
                
                if IS_MALE{
                    if SELECTED_LANG == "en"{
                        playSound(fileName: "Coughing", file_Extension: "wav")
                    }else{
                        playSound(fileName: "tos-coughing", file_Extension: "mp3")
                    }
                    
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "coughing _B&C_En" : "tos (coughing)_B&C_Sp" , file_Extension: "mp3")
                }
            }else if title == "Trach".localized(){
                
                if IS_MALE{
                    if SELECTED_LANG == "en" {
                        playSound(fileName: "trach_male_en", file_Extension: "mp3")
                    }else{
                        playSound(fileName: "traqueotomia_trach", file_Extension: "wav")
                    }
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "trach_Trach_En" : "traqueotomia (trach)_Trach_Sp", file_Extension: "mp3")
                }
            }else if title == "Breathing / Coughing".localized(){
                
                if IS_MALE{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Breathing or Coughing" : "Respiración o tos_breathing or coughing", file_Extension: "wav")
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "Breathing or Coughing _B&C_En" : "respiracion o tos (breathing or coughing)_B&C_Sp" , file_Extension: "mp3")
                }
            }else{
                
            }
            
            if title == "Nausea".localized() {
                title = "Nausea(Concerns page 2)".localized()
            }else if title == "With Food & Drinks".localized() {
                title = "With Food or Drink".localized()
            }else if title == "Other".localized() {
                
                // TabbarObj.selectedIndex = 3
                
                if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                    
                    vc.concerns_Type = .Other
                    vc.logoImage = UIImage(named: "concerns_others")!
                    vc.typeTitle = "Other"
                    self.navigationController?.pushViewController(vc, animated: false)
                }
                
                return
                
            }
            
            if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                
                vc.goToPage = "How_often"
                vc.concerns_Type = .Swallowing
                vc.logoImage = swallowing_ProblemsList[indexPath.row].0!
                vc.typeTitle = swallowing_ProblemsList[indexPath.row].1
                
               
                self.navigationController?.pushViewController(vc, animated: false)
            }
            
        }else if concerns_Type == .Urination {
            
            //            var urination_ProblemsList = [(UIImage(named: "Urgency"),"Urgency"),(UIImage(named: "Frequent"),"Frequent"),(UIImage(named: "Very_Little_Infrequent"),"Very Little/ Infrequent"),(UIImage(named: "Pain_ur"),"Pain"),(UIImage(named: "Burning_ur"),"Burning"),(UIImage(named: "Blood_ur"),"Blood")]
            
            
            let title = urination_ProblemsList[indexPath.row].1
            if title == "Something Else".localized(){
                playSound_SomthingElse()
            }
            if title == "Blood".localized() {
                
                if IS_MALE{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Blood(Urination)" : "Blood_sangre 1", file_Extension: "wav")
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "blood_Urination_En" : "sangre (blood)_Urination_Sp", file_Extension: "mp3")
                }
            }else if title == "Burning".localized() {
                
                if IS_MALE{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Burning(Urination)" : "burning_quema", file_Extension: "wav")
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "Burning_Urination_En" : "quema (burning)_Urination_Sp", file_Extension: "mp3")
                }
            }else if title == "Very Little/ Infrequent".localized() {
                
                if IS_MALE{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Very Little or Infrequent" : "very little or infrequent_muy poco o poco frequente", file_Extension: "wav")
                }else{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Very Little or Infrequent_Urination_En" : "muy poco o poca frequencia_Urination_Sp", file_Extension: "mp3")
                }
            }else if title == "Frequent".localized() {
                
                if IS_MALE{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Frequent" : "frequent_frequente", file_Extension: "wav")
                }else{
                    playSound(fileName:  SELECTED_LANG == "en" ? "frequent_Urination_En" : "frequente (frequent)_Urination_Sp", file_Extension: "mp3")
                }
            }else if title == "Urgency".localized() {
                
                if IS_MALE{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Urgency" : "urgency_urgencia", file_Extension: "wav")
                }else{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Urgency_Urination_En" : "Urgencia (urgency)_Urination_Sp", file_Extension: "mp3")
                }
            }
            else{
                //                playSound(fileName: title, file_Extension: "wav")
            }
            
            if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                
                vc.goToPage = "How_often"
                vc.concerns_Type = .Urination
                vc.logoImage = urination_ProblemsList[indexPath.row].0!
                vc.typeTitle = urination_ProblemsList[indexPath.row].1
               
                self.navigationController?.pushViewController(vc, animated: false)
            }
            
        }else if concerns_Type == .vision_problem {
            
            //            var urination_ProblemsList = [(UIImage(named: "Urgency"),"Urgency"),(UIImage(named: "Frequent"),"Frequent"),(UIImage(named: "Very_Little_Infrequent"),"Very Little/ Infrequent"),(UIImage(named: "Pain_ur"),"Pain"),(UIImage(named: "Burning_ur"),"Burning"),(UIImage(named: "Blood_ur"),"Blood")]
            
            
            let title = vision_ProblemsList[indexPath.row].1
            if title == "Something Else".localized(){
                playSound_SomthingElse()
            }
            //            playSound(fileName: title, file_Extension: "wav")
            if title == "Double Vision".localized(){
                
                if IS_MALE{
                    if SELECTED_LANG == "en" {
                        playSound(fileName: "double_male_en", file_Extension: "mp3")
                    }else{
                        playSound(fileName: "double_doble", file_Extension: "wav")
                    }
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "Double_VP_En" : "doble (double)_VP_Sp", file_Extension: "mp3")
                }
            }else if title == "Blurry".localized(){
                
                if IS_MALE{
                    if SELECTED_LANG == "en" {
                        playSound(fileName: "Blurry_male_en", file_Extension: "mp3")
                    }else{
                        playSound(fileName: "blurry_borroso", file_Extension: "wav")
                    }
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "Blurry_VP_En" : "borroso (blurry)_VP_Sp", file_Extension: "mp3")
                }
            }else if title == "Left Vision".localized() {
                
                if IS_MALE{
                    if SELECTED_LANG == "en" {
                        playSound(fileName: "left_male_en", file_Extension: "mp3")
                    }else{
                        playSound(fileName: "left_izquierda", file_Extension: "wav")
                    }
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "Left_VP_En" : "izquierda (left)_VP_Sp", file_Extension: "mp3")
                }
            }else if title == "Right Vision".localized() {
                if IS_MALE{
                    if SELECTED_LANG == "en" {
                        playSound(fileName: "Right_male_en", file_Extension: "mp3")
                    }else{
                        playSound(fileName: "right_derecha", file_Extension: "wav")
                    }
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "Right_VP_En" : "derecha (right)_VP_Sp", file_Extension: "mp3")
                }
            }else if title == "Glasses".localized() {
                
                if IS_MALE{
                    if SELECTED_LANG == "en" {
                        playSound(fileName: "glasses_male_en", file_Extension: "mp3")
                    }else{
                        playSound(fileName: "glasses_gafas", file_Extension: "wav")
                    }
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "Glasses_VP_En" : "gafas (glasses)_VP_Sp", file_Extension: "mp3")
                }
            }
            else{
                //                playSound(fileName: title, file_Extension: "wav")
            }
            
            if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                
                vc.goToPage = title == "Glasses" ? "whiteboard" : "How_often"
                vc.concerns_Type = .vision_problem
                vc.logoImage = vision_ProblemsList[indexPath.row].0 ?? UIImage(named: "applogo2")!
                vc.typeTitle = vision_ProblemsList[indexPath.row].1
                
               
                self.navigationController?.pushViewController(vc, animated: false)
            }
        }else if concerns_Type == .Needs_Board {
            
            let title = Needs_board_List[indexPath.row].1
            if title == "Something Else".localized(){
                playSound_SomthingElse()
            }
            //            playSound(fileName: title, file_Extension: "wav")
            if title == "Bathroom".localized(){
                
//                if IS_MALE{
//                    if SELECTED_LANG == "en" {
//                        playSound(fileName: "double_male_en", file_Extension: "mp3")
//                    }else{
//                        playSound(fileName: "double_doble", file_Extension: "wav")
//                    }
//                }else{
//                    playSound(fileName: SELECTED_LANG == "en" ? "Double_VP_En" : "doble (double)_VP_Sp", file_Extension: "mp3")
//                }
            }else if title == "Bed".localized(){
                
//                if IS_MALE{
//                    if SELECTED_LANG == "en" {
//                        playSound(fileName: "Blurry_male_en", file_Extension: "mp3")
//                    }else{
//                        playSound(fileName: "blurry_borroso", file_Extension: "wav")
//                    }
//                }else{
//                    playSound(fileName: SELECTED_LANG == "en" ? "Blurry_VP_En" : "borroso (blurry)_VP_Sp", file_Extension: "mp3")
//                }
            }else if title == "Food".localized() {
                
//                if IS_MALE{
//                    if SELECTED_LANG == "en" {
//                        playSound(fileName: "left_male_en", file_Extension: "mp3")
//                    }else{
//                        playSound(fileName: "left_izquierda", file_Extension: "wav")
//                    }
//                }else{
//                    playSound(fileName: SELECTED_LANG == "en" ? "Left_VP_En" : "izquierda (left)_VP_Sp", file_Extension: "mp3")
//                }
            }else if title == "Drink".localized() {
//                if IS_MALE{
//                    if SELECTED_LANG == "en" {
//                        playSound(fileName: "Right_male_en", file_Extension: "mp3")
//                    }else{
//                        playSound(fileName: "right_derecha", file_Extension: "wav")
//                    }
//                }else{
//                    playSound(fileName: SELECTED_LANG == "en" ? "Right_VP_En" : "derecha (right)_VP_Sp", file_Extension: "mp3")
//                }
            }
        }else if concerns_Type == .Trach {
            
            let title = trash_list[indexPath.row].1
            if title == "Something Else".localized(){
                playSound_SomthingElse()
            }
            if title == "Mucus / Secretion".localized() {
                
                if IS_MALE{
                    if SELECTED_LANG == "en" {
                        playSound(fileName: "mucous or secretions_male_en", file_Extension: "mp3")
                    }else{
                        playSound(fileName: "mucosidad_secreciones_mucus_secretions", file_Extension: "wav")
                    }
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "mucous or secretions_Trach_En" : "mucosidad o secreciones (mucous or secretions)_Trach_Sp", file_Extension: "mp3")
                }
            }else if title == "Suction".localized() {
                
                if IS_MALE{
                    if SELECTED_LANG == "en" {
                        playSound(fileName: "suction_male_en", file_Extension: "mp3")
                    }else{
                        playSound(fileName: "succion_suction", file_Extension: "wav")
                    }
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "suction_Trach_En" : "succion (suction)_Trach_Sp", file_Extension: "mp3")
                }
            }
            else if title == "Cap".localized() {
                
                if IS_MALE{
                    if SELECTED_LANG == "en"{
                        playSound(fileName: "cap_male_en", file_Extension: "mp3")
                    }else{
                        playSound(fileName: "Cánula(Cap)_Male_Sp", file_Extension: "wav")
                    }
                    
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "cap_Trach_En" : "Canula (cap)_Trach_Sp", file_Extension: "mp3")
                }
            }
            else if title == "Speaking Valve".localized() {
                
                if IS_MALE{
                    if SELECTED_LANG == "en" {
                        playSound(fileName: "speaking valve_male_en", file_Extension: "mp3")
                    }else{
                        playSound(fileName: "Valvula Parlante(speaking valve)_Male_Sp", file_Extension: "wav")
                    }
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "speaking valve_Trach_En" : "Valvula Parlante (speaking valve)_Trach_Sp", file_Extension: "mp3")
                }
            }
            else if title == "Remove".localized() {
                
                if IS_MALE{
                    if SELECTED_LANG == "en" {
                        playSound(fileName: "remove_male_en", file_Extension: "mp3")
                    }else{
                        playSound(fileName: "Quitar_Remove", file_Extension: "wav")
                    }
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "Remove_Trach_En" : "quitar (remove)_Trach_Sp", file_Extension: "mp3")
                }
            }else if title == "Breathing / Coughing".localized() {
                if IS_MALE{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Breathing or Coughing" : "Respiración o tos_breathing or coughing", file_Extension: "wav")
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "Breathing or Coughing _B&C_En" : "respiracion o tos (breathing or coughing)_B&C_Sp" , file_Extension: "mp3")
                }
            }else if title == "Swallowing".localized() {
                if IS_MALE{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Swallowing" : "tragar_swallowing", file_Extension: "wav")
                }else{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Swallowing_Swallowing_En" : "tragar (swallow)_female_Sp", file_Extension: "mp3")
                }
            }else{
                //                playSound(fileName: title, file_Extension: "wav")
            }
            
            if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                
                vc.goToPage = "How_often"
                vc.concerns_Type = .Trach
                vc.logoImage = trash_list[indexPath.row].0 ?? UIImage(named: "applogo2")!
                vc.typeTitle = trash_list[indexPath.row].1
                
                self.navigationController?.pushViewController(vc, animated: false)
            }
        }
        else if concerns_Type == .Emotions_Feelings {
            
            //            var urination_ProblemsList = [(UIImage(named: "Urgency"),"Urgency"),(UIImage(named: "Frequent"),"Frequent"),(UIImage(named: "Very_Little_Infrequent"),"Very Little/ Infrequent"),(UIImage(named: "Pain_ur"),"Pain"),(UIImage(named: "Burning_ur"),"Burning"),(UIImage(named: "Blood_ur"),"Blood")]
            
            let title = emotions_feelings_list[indexPath.row].1
            if title == "Something Else".localized(){
                playSound_SomthingElse()
            }
            if title == "Angry".localized() {
                
                if IS_MALE{
                    if SELECTED_LANG == "en" {
                        playSound(fileName: "angry_Male_en", file_Extension: "mp3")
                    }else{
                        playSound(fileName: "enojado(angry)_male_Sp", file_Extension: "wav")
                    }
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "angry_E&F_En" : "enojado (angry)_E&F_Sp" , file_Extension: "mp3")
                }
            }else if title == "Anxious".localized() {
                
                if IS_MALE{
                    if SELECTED_LANG == "en" {
                        playSound(fileName: "anxious_male_en", file_Extension: "mp3")
                    }else{
                        playSound(fileName: "Anxious_Ansioso", file_Extension: "wav")
                    }
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "anxious_E&F_En" : "ansioso (anxious)_E&F_Sp" , file_Extension: "mp3")
                }
            }else if title == "Frustrated".localized() {
                
                if IS_MALE{
                    if SELECTED_LANG == "en" {
                        playSound(fileName: "frustrated_Male_en", file_Extension: "mp3")
                    }else{
                        playSound(fileName: "frustrado_frustrated", file_Extension: "wav")
                    }
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "frustrated_E&F_En" : "frustrado (frustrated)_E&F_Sp" , file_Extension: "mp3")
                }
            }else if title == "Depressed".localized() {
                if IS_MALE{
                    if SELECTED_LANG == "en" {
                        playSound(fileName: "Depressed_Male_en", file_Extension: "mp3")
                    }else{
                        playSound(fileName: "deprimido_depressed", file_Extension: "wav")
                    }
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "depressed_E&F_En" : "deprimido (depressed)_E&F_Sp" , file_Extension: "mp3")
                }
            }else if title == "Exhausted".localized(){
                
                if IS_MALE{
                    if SELECTED_LANG == "en" {
                        playSound(fileName: "exhausted_Male_en", file_Extension: "mp3")
                    }else{
                        playSound(fileName: "agotado_exhausted", file_Extension: "wav")
                    }
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "exhausted_E&F_En" : "agotado (exhausted)_E&F_Sp" , file_Extension: "mp3")
                }
            }else if title == "Sad".localized(){
                if IS_MALE{
                    if SELECTED_LANG == "en" {
                        playSound(fileName: "sad_Male_en", file_Extension: "mp3")
                    }else{
                        playSound(fileName: "Triste(sad)_male_Sp", file_Extension: "wav")
                    }
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "sad_E&F_En" : "triste (sad)_E&F_Sp" , file_Extension: "mp3")
                }
            }else if title == "Scared".localized(){
                
                if IS_MALE{
                    if SELECTED_LANG == "en" {
                        playSound(fileName: "scared_male_en", file_Extension: "mp3")
                    }else{
                        playSound(fileName: "asustado_angry", file_Extension: "wav")
                    }
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "scared_E&F_En" : "asustado (scared)_E&F_Sp" , file_Extension: "mp3")
                }
            }else{
                //                playSound(fileName: title, file_Extension: "wav")
            }
            
            if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                
                vc.goToPage = "How_often"
                vc.concerns_Type = .Emotions_Feelings
                vc.logoImage = emotions_feelings_list[indexPath.row].0 ?? UIImage(named: "applogo2")!
                vc.typeTitle = emotions_feelings_list[indexPath.row].1
                
              
                
                self.navigationController?.pushViewController(vc, animated: false)
            }
        }else if comeFromhardToSwallow {
            
            let title = hardToSwallow_List[indexPath.row].1
            if title == "Something Else".localized(){
                playSound_SomthingElse()
            }
            if title == "Whole /Crushed /Liquid".localized(){
                if IS_MALE{
                    if SELECTED_LANG == "en" {
                        playSound(fileName: "whole, crushed, or liquid_Male_en", file_Extension: "mp3")
                    }else{
                        playSound(fileName: "Enteros, triturados, o liquids - revised", file_Extension: "mp3")
                    }
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "whole, crushed, or liquid_Medication_En" : "enteros, aplastados, o liquidos_Medication_Sp", file_Extension: "mp3")
                }
                if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                    
                    vc.goToPage = "Whole /Crushed /Liquid".localized()
                    vc.concerns_Type = .Medication
                    vc.logoImage = hardToSwallow_List[indexPath.row].0!
                    vc.typeTitle = hardToSwallow_List[indexPath.row].1
                    self.navigationController?.pushViewController(vc, animated: false)
                }
            }else{
                isMedicationLastIcon = true
                if title == "Too Many".localized(){
                    if IS_MALE{
                        if SELECTED_LANG == "en" {
                            playSound(fileName: "too many_Male_en", file_Extension: "mp3")
                        }else{
                            playSound(fileName: "Demasiados(too many)_Male_Sp", file_Extension: "wav")
                        }
                    }else{
                        playSound(fileName: SELECTED_LANG == "en" ? "Too many_Medication_En" : "demaciados (too many)_Medication_Sp", file_Extension: "mp3")
                    }
                }else if title == "Too Big".localized(){
                    if IS_MALE{
                        if SELECTED_LANG == "en" {
                            playSound(fileName: "too big_Male_en", file_Extension: "mp3")
                        }else{
                            playSound(fileName: "Demasiadas grandes", file_Extension: "mp3")
                        }
                    }else{
                        playSound(fileName: SELECTED_LANG == "en" ? "Too big_Medication_En" : "demasiado grande (too big)_Medication_Sp", file_Extension: "mp3")
                    }
                }
                if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                    
                    vc.isShowSummryBtn = isMedicationLastIcon
                    vc.goToPage = "White Board"
                    vc.concerns_Type = .Medication
                    vc.logoImage = hardToSwallow_List[indexPath.row].0!
                    vc.typeTitle = hardToSwallow_List[indexPath.row].1
                    self.navigationController?.pushViewController(vc, animated: false)
                }
            }
            
        }
    }
    
}
