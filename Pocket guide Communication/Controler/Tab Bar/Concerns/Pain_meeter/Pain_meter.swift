//
//  Pain_meter.swift
//  Pocket guide Communication
//
//  Created by Deepak on 08/11/21.
//

import UIKit

class Pain_meter: UIViewController,UIScrollViewDelegate {
    
    @IBOutlet weak var viewImage1: UIView!
    @IBOutlet weak var viewImage2: UIView!
    @IBOutlet weak var viewImage3: UIView!
    @IBOutlet weak var viewImage4: UIView!
    @IBOutlet weak var viewImage5: UIView!
    @IBOutlet weak var viewImage6: UIView!
    @IBOutlet weak var viewImage7: UIView!
    @IBOutlet weak var viewImage8: UIView!
    
    
    @IBOutlet weak var viewlbl1: UIView!
    @IBOutlet weak var viewlbl2: UIView!
    @IBOutlet weak var viewlbl3: UIView!
    @IBOutlet weak var viewIlbl4: UIView!
    @IBOutlet weak var viewlbl5: UIView!
    @IBOutlet weak var viewlbl6: UIView!
    @IBOutlet weak var viewlbl7: UIView!
    @IBOutlet weak var viewlbl8: UIView!

    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var btn2: UIButton!
    @IBOutlet weak var btn3: UIButton!
    @IBOutlet weak var btn4: UIButton!
    @IBOutlet weak var btn5: UIButton!
    @IBOutlet weak var btn6: UIButton!
    @IBOutlet weak var btn7: UIButton!
    @IBOutlet weak var btn8: UIButton!
    
    @IBOutlet weak var topBtn1: UIButton!
    @IBOutlet weak var topBtn2: UIButton!
    @IBOutlet weak var topBtn3: UIButton!
    @IBOutlet weak var topBtn4: UIButton!
    @IBOutlet weak var topBtn5: UIButton!
    @IBOutlet weak var topBtn6: UIButton!

    var allViewImages = [UIView()]
    var allViewlabal = [UIView()]
    var allButtons = [UIButton()]
    var allTopButtons = [UIButton()]
    
    @IBOutlet weak var zoomView : ZoomableView!
    @IBOutlet weak var contentView : UIView!
    @IBOutlet weak var sclView : UIScrollView!
    @IBOutlet weak var imgPain : UIImageView!
    
    var all_list = [(UIImage(named: "pain0"),"No Pain".localized()),(UIImage(named: "pain1"),"Mild".localized()),(UIImage(named: "pain2"),"Moderate".localized()),(UIImage(named: "pain3"),"Severe".localized()),(UIImage(named: "pain4"),"Very Severe".localized()),(UIImage(named: "pain5"),"Worst Pain Imaginable".localized())]
    
    var isNext = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sclView.minimumZoomScale = 1
        sclView.maximumZoomScale = 10.0
        sclView.delegate = self
        imgPain.image = UIImage(named: SELECTED_LANG == "en" ? "pain_fell_meter" : "pain_fell_meter_Sp")
//        zoomView.sourceView = contentView
//        zoomView.isZoomable = true
//        zoomView.backgroundViewColor = "white"
    
        allViewImages = [viewImage1,viewImage2,viewImage3,viewImage4,viewImage5,viewImage6]
        allViewlabal = [viewlbl1,viewlbl2,viewlbl3,viewIlbl4,viewlbl5,viewlbl6]
        allButtons = [btn1,btn2,btn3,btn4,btn5,btn6]
        allTopButtons = [topBtn1,topBtn2,topBtn3,topBtn4,topBtn5,topBtn6]
        DispatchQueue.main.async {
            for i in 0 ... self.allButtons.count - 1{
                self.allButtons[i].tag = i
                self.allTopButtons[i].tag = i
                self.allTopButtons[i].isUserInteractionEnabled = true
                self.allButtons[i].isUserInteractionEnabled = true
                self.allButtons[i].addTarget(self, action: #selector(self.btnSelectAction(_:)), for: .touchUpInside)
                self.allTopButtons[i].addTarget(self, action: #selector(self.btnSelectAction(_:)), for: .touchUpInside)
                self.allViewImages[i].layer.cornerRadius = self.allViewImages[i].frame.width/2
            }
        }
        
        let button = UIButton()
        button.tag = -1
        btnSelectAction(button)
        isNext = true
        
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
            return zoomView
        }
    @objc func btnSelectAction(_ sender: UIButton) {
        
        let getTag = sender.tag
        print(getTag)
        DispatchQueue.main.async { [self] in
            
            for i in 0 ... allViewImages.count - 1 {
                if i == getTag{
                    allViewImages[i].borderColor = hexStringToUIColor(hex: "#0F9AF0")
                }else{
                    allViewImages[i].borderColor = .clear
                }
            }
            
            for i in 0 ... allViewlabal.count - 1 {
                if i == getTag{
                    allViewlabal[i].borderColor = hexStringToUIColor(hex: "#0F9AF0")
                    allViewlabal[i].borderWidth = 2
                    allViewlabal[i].cornerRadius = 13
                }else{
                    allViewlabal[i].borderColor = .clear
                    allViewlabal[i].borderWidth = 0
                }
            }
        }
        
        if isNext{
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: { [self] in
                
                if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                    
                    vc.fromPain = all_list[getTag].1
                    vc.concerns_Type = .Pain_meter
                    vc.logoImage = all_list[getTag].0!
                    vc.typeTitle = all_list[getTag].1
                    self.navigationController?.pushViewController(vc, animated: false)
                }
            })
        }
    }

    
    @IBAction func btnBackAction(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: false)
        
    }

}
