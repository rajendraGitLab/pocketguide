//
//  How_often.swift
//  Pocket guide Communication
//
//  Created by Deepak on 08/11/21.
//

import UIKit

class How_often: UIViewController,UIScrollViewDelegate {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var zoomView : ZoomableView!
    @IBOutlet weak var sclView : UIScrollView!
    
    @IBOutlet weak var indicatorView: UIView!
    @IBOutlet weak var indicatorViewTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var lblTitle: UILabel!
    var oftenList = [(UIImage(named: "Morning"),"Morning".localized()),(UIImage(named: "All Day"),"All Day".localized()),(UIImage(named: "Evening"),"Evening".localized()),(UIImage(named: "With Activity"),"With Activity".localized()),(UIImage(named: "Comes and Goes"),"Comes and Goes".localized()),(UIImage(named: "just_started"),"Just Started".localized()),(UIImage(named: "when_standing"),"When Standing".localized()),(UIImage(named: "getting_better"),"Better".localized()),(UIImage(named: "getting_worse"),"Worse".localized()),(UIImage(named: "when_sitting"),"When Sitting".localized()),(UIImage(named: "With Medication"),"With Medication".localized())]

    var selectIndex = -1
    
    var titleVC = "How Often?".localized()
    fileprivate (set) var scrollBar : VerticalScrollBar!
    
    var currentCellFrame = Int()
    var concerns_Type: Concerns_Type = .none
    var addMoreIcon = false
    var isZooming = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sclView.minimumZoomScale = 1
        sclView.maximumZoomScale = 10.0
        sclView.delegate = self
//        zoomView.sourceView = collectionView
//        zoomView.isZoomable = true
        lblTitle.text = titleVC.localized()
        if addMoreIcon {
            oftenList.append((UIImage(named: "concerns_swallowing"),"Swallowing".localized()))
            oftenList.append((UIImage(named: "Choking"),"Choking".localized()))
        }
//        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
//            layout.scrollDirection = isIpad == true ? .vertical : .horizontal
//        }
        collectionView.reloadData()
        self.scrollBar = VerticalScrollBar(frame: CGRect.zero, targetScrollView: self.collectionView)
                self.view.addSubview(self.scrollBar!)
        
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.scrollBar.frame = CGRect(x: UIScreen.main.bounds.width - (isIpad ? 40 : 20) - (UIApplication.shared.windows[0].safeAreaInsets.left), y: self.view.frame.height - self.collectionView.frame.height - (isIpad ? 90 : (UIDevice.current.hasNotch ? 60 : 42)) - (isIpad ? 40 : 0), width: self.indicatorView.frame.width * 1.6, height: self.collectionView.frame.height)
        self.scrollBar.backgroundColor = .clear
    }
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        isZooming = true
    }
    func scrollViewDidEndZooming(_ scrollView: UIScrollView, with view: UIView?, atScale scale: CGFloat) {
        isZooming = false
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if !isZooming{
            let totalScroll = scrollView.contentSize.height - self.collectionView.frame.height
            let ratio = totalScroll/(self.collectionView.frame.height - self.indicatorView.frame.height)
            self.indicatorViewTopConstraint.constant = (scrollView.contentOffset.y)/(ratio)
        }
    }
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
            return zoomView
        }
    @IBAction func btnBackAction(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: false)
    }

}

extension How_often : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return oftenList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let getData = oftenList
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Health_type_Cell_More", for: indexPath) as! Health_type_Cell_More
            cell.image.image = getData[indexPath.row].0
        cell.title.text = getData[indexPath.row].1
        cell.title.font = iconTitleFont
        cell.image.contentMode = .scaleToFill
        if selectIndex != -1{
            if indexPath.row == selectIndex{
                cell.bgView.borderColor =  hexStringToUIColor(hex: "#0F9AF0")
                cell.bgView.borderWidth = isIpad ? 8 : 4
            }else{
                cell.bgView.borderColor = .clear
            }
        }else{
            cell.bgView.borderColor = .clear
        }
        cell.bgView.dropShadow()
            return cell
        }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let collectionHeight = collectionView.bounds.height

        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
//        flowLayout.collectionView?.isPagingEnabled = true
        let totalSpace = flowLayout.sectionInset.left
        + flowLayout.sectionInset.right
        + ( (noOfCellsInRow == 2 ? (isIpad ? collectionView.frame.height/5 : collectionView.frame.height/1.5) : flowLayout.minimumLineSpacing) * CGFloat((noOfCellsInRow == 6 ? 3 : noOfCellsInRow) - 1))

        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat((noOfCellsInRow == 6 ? 3 : noOfCellsInRow)))
        currentCellFrame = noOfCellsInRow == 1 ? Int(collectionHeight - (isIpad ? 40 : 20)) : size - (isIpad ? 25 : 12)
        
//        self.GuideView_Category2WidthConstraint?.constant = CGFloat(currentCellFrame)
//        self.GuideView_Category2TopConstraint.constant = collectionView.bounds.height/2 - CGFloat(currentCellFrame/2)
        return CGSize(width: noOfCellsInRow == 6 ? Int(collectionHeight/1.9 - (4 * flowLayout.minimumInteritemSpacing)) : currentCellFrame, height: noOfCellsInRow == 6 ? Int(collectionHeight/2.05 - (4 * flowLayout.minimumInteritemSpacing)) : currentCellFrame)
        
        
//                if (vcMode == "normal"){
//                    if concerns_Type == .pain_feel || concerns_Type == .Swallowing || concerns_Type == .Nausea || concerns_Type == .Fatigue || concerns_Type == .Bowels || concerns_Type == .Urination{
//                        return CGSize(width: (collectionView.bounds.width / 3 - 20), height: 175)
//                    }
//                    return isIpad == true ? CGSize(width: collectionWidth/3, height: collectionWidth/3) : CGSize(width: collectionWidth - 9, height: collectionWidth - 9)
//                }else{
//                    return CGSize(width: (collectionView.bounds.width / 3 - 20), height: 175)
//                }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        if noOfCellsInRow <= 2 {
            return isIpad ? 40 : 20
        }
        return 1
    }
   
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if noOfCellsInRow == 6 {
            return isIpad ? 20 : 8
        }
        return collectionView.bounds.height/2 - CGFloat(currentCellFrame/2)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
//
//        KtFlowlayout.minimumInteritemSpacing = 10
////        layout.minimumLineSpacing = 401
//        KtFlowlayout.minimumLineSpacing = collectionView.bounds.height/2 - CGFloat(currentCellFrame/2)
////        flowLayout.collectionView?.isPagingEnabled = true
        if noOfCellsInRow <= 2 {
            let totalCellWidth = Int(currentCellFrame) * noOfCellsInRow
            let totalSpacingWidth = Int(isIpad ? 60 : 30) * (noOfCellsInRow - 1)

            let leftInset = (collectionView.bounds.width - CGFloat(totalCellWidth + totalSpacingWidth)) / 2
            let rightInset = leftInset
            return UIEdgeInsets(top: collectionView.bounds.height/2 - CGFloat(currentCellFrame/2), left: rightInset, bottom: collectionView.bounds.height/2 - CGFloat(currentCellFrame/2), right: rightInset)
        }else{
            if noOfCellsInRow == 6 {
                return UIEdgeInsets(top: 0, left: isIpad ? 25 : hasTopNotch ? screenHeight/3 : screenHeight/4, bottom: 0, right: isIpad ? 15 : hasTopNotch ? screenHeight/3 : screenHeight/4)
            }
            return UIEdgeInsets(top: collectionView.bounds.height/2 - CGFloat(currentCellFrame/2), left: 0, bottom: collectionView.bounds.height/2 - CGFloat(currentCellFrame/2), right: 0)
      }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        selectIndex = indexPath.row
        collectionView.reloadData()
        
        if concerns_Type == .Swallowing {
        return
        }
    
        //
        let title = oftenList[indexPath.row].1
        
        if title == "With Activity".localized() {
            isDaySelected = false
            if IS_MALE{
                playSound(fileName: SELECTED_LANG == "en" ? "With Activity(How Often)" : "con actividad", file_Extension: "wav")
            }else{
                playSound(fileName: SELECTED_LANG == "en" ? "With activity_HO_En" : "con actividad (with activity)_HO_Sp", file_Extension: "mp3")
            }
        } else if title == "Better".localized(){
            isDaySelected = false
            if IS_MALE{
                if SELECTED_LANG == "en" {
                    playSound(fileName: "Better_Male_en", file_Extension: "mp3")
                }else{
                    playSound(fileName: "better_mejor", file_Extension: "wav")
                }
            }else{
                playSound(fileName: SELECTED_LANG == "en" ? "Better_HO_En" : "mejor (better)_HO_Sp", file_Extension: "mp3")
            }
        }else if title == "All Day".localized(){
            isDaySelected = true
            if IS_MALE{
                playSound(fileName: SELECTED_LANG == "en" ? "All Day(Fatigue)" : "todo el dia", file_Extension: "wav")
            }else{
                playSound(fileName: SELECTED_LANG == "en" ? "All day_HO_En" : "todo el dia (all day)_HO_Sp", file_Extension: "mp3")
            }
        }else if title == "Comes and Goes".localized(){
            isDaySelected = false
            if IS_MALE{
                playSound(fileName: SELECTED_LANG == "en" ? "Comes and Goes" : "Viene y Va(comes&goes)", file_Extension: "wav")
            }else{
                playSound(fileName: SELECTED_LANG == "en" ? "Comes and Goes_HO_En" : "viene y va (comes and goes)_HO_Sp", file_Extension: "mp3")
            }
        }
        else if title == "Evening".localized(){
            isDaySelected = true
//            if IS_MALE{
//                playSound(fileName: SELECTED_LANG == "en" ? "Evening" : "Tarde - afternoon_male_Sp", file_Extension: "wav")
//            }else{
//                playSound(fileName: SELECTED_LANG == "en" ? "evening_HO_En" : "tarde (evening)_HO_Sp", file_Extension: "mp3")
//            }
            if IS_MALE{
                playSound(fileName: SELECTED_LANG == "en" ? "Evening" : "Noche Temprano - evening_male_Sp", file_Extension: "wav")
            }else{
                playSound(fileName: SELECTED_LANG == "en" ? "Evening_Calender_en" : "noche temprano (evening - early evening)_Calender_Sp", file_Extension: "mp3")
            }
        }
        
        else if title == "Just Started".localized(){
            isDaySelected = false
            if IS_MALE{
                if SELECTED_LANG == "en" {
                    playSound(fileName: "just started_male_en", file_Extension: "mp3")
                }else{
                    playSound(fileName: "Recien Comenzado(just started)_male_Sp", file_Extension: "wav")
                }
            }else{
                playSound(fileName: SELECTED_LANG == "en" ? "Just started_HO_En" : "recien comenzando (just started)_HO_Sp", file_Extension: "mp3")
            }
        }
        else if title == "Morning".localized(){
            isDaySelected = true
            if IS_MALE{
                
                playSound(fileName: SELECTED_LANG == "en" ? "Morning" : "Mañana - morning_male_Sp", file_Extension: "wav")
            }else{
                playSound(fileName: SELECTED_LANG == "en" ? "morning_HO_En" : "manana (morning)_HO_Sp", file_Extension: "mp3")
            }
        }
        else if title == "When Standing".localized(){
            isDaySelected = false
            if IS_MALE{
                if SELECTED_LANG == "en" {
                    playSound(fileName: "When Standing_Male_en", file_Extension: "mp3")
                }else{
                    playSound(fileName: "Al estar de pie_male_SP", file_Extension: "wav")
                }
            }else{
                playSound(fileName: SELECTED_LANG == "en" ? "When Standing_HO_En" : "al estar de pie (when standing)_HO_Sp", file_Extension: "mp3")
            }
        }else if title == "When Sitting".localized(){
            isDaySelected = false
            if IS_MALE{
                if SELECTED_LANG == "en" {
                    playSound(fileName: "When Sitting_Male_en", file_Extension: "mp3")
                }else{
                    playSound(fileName: "Al estar sentado(When Siting)_male_Sp", file_Extension: "wav")
                }
            }else{
                playSound(fileName: SELECTED_LANG == "en" ? "when sitting_HO_En" : "al estar sentado (when sitting)_HO_Sp", file_Extension: "mp3")
            }
        }
        else if title == "Worse".localized(){
            isDaySelected = false
            if IS_MALE{
                if SELECTED_LANG == "en" {
                    playSound(fileName: "Worse_Male_en", file_Extension: "mp3")
                }else{
                    playSound(fileName: "worse_peor", file_Extension: "wav")
                }
            
            }else{
                playSound(fileName: SELECTED_LANG == "en" ? "Worse_HO_En" : "peor (worse)_HO_Sp", file_Extension: "mp3")
            }
        }else if title == "Swallowing".localized(){
            isDaySelected = false
            if IS_MALE{
                playSound(fileName:  SELECTED_LANG == "en" ? "Swallowing" : "tragar_swallowing", file_Extension: "wav")
            }else{
                playSound(fileName:  SELECTED_LANG == "en" ? "Swallowing_Swallowing_En" : "tragar (swallow)_female_Sp", file_Extension: "mp3")
            }
        }else if title == "Choking".localized(){
            isDaySelected = false
            if IS_MALE{
                playSound(fileName: SELECTED_LANG == "en" ? "Choking" : "asfixia_choking", file_Extension: "wav")
            }else{
                playSound(fileName: SELECTED_LANG == "en" ? "choking_B&C_En" : "asfixia (choking)_B&C_Sp" , file_Extension: "mp3")
            }
        }else if title == "With Medication".localized() {
            isDaySelected = false
            if IS_MALE{
                playSound(fileName: SELECTED_LANG == "en" ? "With Medication(Fatigue)_com" : "con medicacion", file_Extension: "wav")
            }else{
                playSound(fileName: SELECTED_LANG == "en" ? "With medication_ Nausea_En" : "con medicacion (with medication)_ Nausea_Sp", file_Extension: "mp3")
            }
        }else{
            isDaySelected = false
            playSound(fileName: oftenList[indexPath.row].1.localized(), file_Extension: "wav")
        }
        
        var getTitle = "When did it start?".localized()
        if concerns_Type == .Pain{
            getTitle = "When did this pain start?".localized()
        }
        if concerns_Type == .Nausea{
            getTitle = "When did the this nausea start?".localized()
        }else if concerns_Type == .Breathing_Problems{
            getTitle = "When did this breathing problem start?".localized()
        }else if concerns_Type == .Fatigue{
            getTitle = "When did this start?".localized()
        }else if concerns_Type == .Bowels{
            getTitle = "When did this bowel problem start?".localized()
        }else if concerns_Type == .Bladder{
            getTitle = "When did this bladder problem start".localized()
        }
        
        if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
            
            vc.goToPage = getTitle
            vc.isFrome = "How_often"
            vc.concerns_Type = concerns_Type
            vc.logoImage = oftenList[indexPath.row].0!
            vc.typeTitle = oftenList[indexPath.row].1
            self.navigationController?.pushViewController(vc, animated: false)
        }

        }
    }
    

