//
//  HumanBodyBack.swift
//  Pocket guide Communication
//
//  Created by Deepak on 01/11/21.
//

import UIKit
import AVFoundation

class HumanBodyBack: UIViewController {
    
    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var btn2: UIButton!
    @IBOutlet weak var btn3: UIButton!
    @IBOutlet weak var btn4: UIButton!

    @IBOutlet weak var btn5: UIButton!
    @IBOutlet weak var btn6: UIButton!
    
    @IBOutlet weak var btn7: UIButton!
    @IBOutlet weak var btn8: UIButton!
    
    @IBOutlet weak var btn9: UIButton!
    @IBOutlet weak var btn10: UIButton!
    
    @IBOutlet weak var btn11: UIButton!
    @IBOutlet weak var btn12: UIButton!

    @IBOutlet weak var btn13: UIButton!
    @IBOutlet weak var btn14: UIButton!
    
    @IBOutlet weak var btn15: UIButton!
    @IBOutlet weak var btn16: UIButton!
    
    @IBOutlet weak var btn17: UIButton!
    @IBOutlet weak var btn18: UIButton!
    
    @IBOutlet weak var btn19: UIButton!
    @IBOutlet weak var btn20: UIButton!

    @IBOutlet weak var btn21: UIButton!
    @IBOutlet weak var btn22: UIButton!
    
    @IBOutlet weak var btn23: UIButton!
    @IBOutlet weak var btn24: UIButton!
    
    @IBOutlet weak var btn25: UIButton!
    @IBOutlet weak var btn26: UIButton!
    
    @IBOutlet weak var btn27: UIButton!
    @IBOutlet weak var btn28: UIButton!
    
    @IBOutlet weak var btn29: UIButton!
    @IBOutlet weak var btn30: UIButton!
    
    @IBOutlet weak var btn31: UIButton!
    @IBOutlet weak var btn32: UIButton!
    
    @IBOutlet weak var handStackRightConstant: NSLayoutConstraint!
    @IBOutlet weak var handStackLeftConstant: NSLayoutConstraint!
    
    @IBOutlet weak var wristRightWidthConstant: NSLayoutConstraint!
    @IBOutlet weak var wristLeftWidthConstant: NSLayoutConstraint!
    
    @IBOutlet weak var handViewWidthConstant_T: NSLayoutConstraint!
    @IBOutlet weak var handViewWidthConstant_B: NSLayoutConstraint!
    
    
    @IBOutlet weak var bodyImg: UIImageView!
    @IBOutlet weak var bodyView_TopConstraint: NSLayoutConstraint!
    @IBOutlet weak var bodyView_BottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var bodyView_HeightConstraint: NSLayoutConstraint!

    var voiceNameBack = ["","Back","Lower Back","Upper-Arm","Upper-Arm","Lower-Back","Lower-Back","Elbow","Elbow","Forearm","Forearm","Hand","Hand","Fingers","Fingers","hip","hip","Thigh","Thigh","knee","knee","Lower Leg","Lower Leg","Ankle","Ankle","Foot-Toes","Foot-Toes"]

    var allButtons = [UIButton()]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        allButtons = [btn1,btn2,btn3,btn4,btn5,btn6,btn7,btn8,btn9,btn10,btn11,btn12,btn13,btn14,btn15,btn16,btn17,btn18,btn19,btn20,btn21,btn22,btn23,btn24,btn25,btn26,btn27,btn28,btn29,btn30,btn31,btn32]
        for i in 0 ..< allButtons.count{
            allButtons[i].tag = i+1
            allButtons[i].setTitle("", for: .normal)
            allButtons[i].titleLabel?.textColor = .clear
            allButtons[i].addTarget(self, action: #selector(btntuchAllEvent), for: .touchUpInside)
        }

        bodyView_TopConstraint.priority = UILayoutPriority(isIpad ? 250 : 1000)
        bodyView_BottomConstraint.priority = UILayoutPriority(isIpad ? 250 : 1000)
        bodyView_HeightConstraint.priority = UILayoutPriority(isIpad ? 1000 : 250)
        bodyImg.image = UIImage(named: IS_MALE ? "HumanBody_back" : "female_bodyBack")
        
        handViewWidthConstant_T.constant = IS_MALE ? 0 : (isIpad ? 50 : 25)
        handViewWidthConstant_B.constant = IS_MALE ? 0 : (isIpad ? 50 : 25)
        
//        wristRightWidthConstant.constant = IS_MALE ? 0 : (isIpad ? -50 : -25)
//        wristLeftWidthConstant.constant = IS_MALE ? 0 : (isIpad ? -50 : -25)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
//            playSound(fileName:  SELECTED_LANG == "en" ? "Back" : "Espalda_back", file_Extension: "wav")
        }
    }

    @objc func btntuchAllEvent(button : UIButton,forEvent event: UIEvent){
        
        print(button.tag)
//        let name = voiceNameBack[button.tag - 1]
//        let utterance = AVSpeechUtterance(string: name)
//        utterance.voice = AVSpeechSynthesisVoice(language: "en-GB")
//        utterance.rate = 0.5
////        utterance.voice =
//        let synthesizer = AVSpeechSynthesizer()
//        synthesizer.speak(utterance)
        getImage(button_tag: button.tag, forEvent: event)

    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: false)
        
    }

}
extension HumanBodyBack{
    
    // Matrk:- Get Image Tuch
    
    func getImage(button_tag : Int,forEvent event: UIEvent){
        
        let tapButton = allButtons[button_tag - 1]
        let touches = event.touches(for: tapButton)
        let touch = touches?.first
        let touchPoint = touch?.location(in: tapButton)
       // print("touchPoint\(touchPoint)")
        
        let tapImage = UIImageView(frame: CGRect(x: ((touchPoint?.x ?? 0) - 15), y: ((touchPoint?.y ?? 0) - 15), width: 30, height: 30))
        tapImage.image = UIImage(named: "TapImage")
        tapButton.addSubview(tapImage)
        print(tapButton.frame)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
            tapImage.removeFromSuperview()
            
            if [1].contains(button_tag){
                
                if let vc = SB_HumanBody.instantiateViewController(withIdentifier: "FaceBody") as? FaceBody {
                    self.navigationController?.pushViewController(vc, animated: false)
                }
                
            }else if [2].contains(button_tag){
                
                let getImage = UIImage(named: IS_MALE ? "Back" : "Back_F")
                
                if IS_MALE{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Back" : "Espalda_back", file_Extension: "wav")
                }else{
                    playSound(fileName:  SELECTED_LANG == "en" ? "back_PL_En" : "espalda (back)_PL_Sp", file_Extension: "mp3")
                }
                    if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                        vc.logoImage = getImage!
                        vc.typeTitle = "Back".localized()
                        vc.concerns_Type = .pain_feel

                        self.navigationController?.pushViewController(vc, animated: false)
                    }
                
                
            }else if [3,4].contains(button_tag){
                
                let getImage = UIImage(named: IS_MALE ? "Upper-Arm" : "Upper-Arm_F")
                
                if IS_MALE{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Upper Arm" : "brazo superior_upper arm", file_Extension: "wav")
                }else{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Upper Arm_PL_En" : "brazo superior (upper arm)_PL_Sp", file_Extension: "mp3")
                }
                
                    if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                        vc.logoImage = getImage!
                        if button_tag == 3 {
                            vc.logoImage = getImage!.mirrorImg()
                        }
                        vc.typeTitle = "Upper-Arm".localized()
                        vc.concerns_Type = .pain_feel

                        self.navigationController?.pushViewController(vc, animated: false)
                    }
                
            }else if [27,28].contains(button_tag){
                
                let getImage = UIImage(named: IS_MALE ? "Shoulder" : "Shoulder_F")
                
                if IS_MALE{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Shoulder" : "Hombro_shoulder", file_Extension: "wav")
                }else{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Shoulder_PL_En" : "hombro (shoulder)_PL_Sp", file_Extension: "mp3")
                }
                
                    if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                        vc.logoImage = getImage!
                        if button_tag == 27 {
                            vc.logoImage = getImage!.mirrorImg()
                        }
                        vc.typeTitle = "Shoulder".localized()
                        vc.concerns_Type = .pain_feel

                        self.navigationController?.pushViewController(vc, animated: false)
                    }
            }
            
            else if [5,6].contains(button_tag){
                
                let getImage = UIImage(named: IS_MALE ? "Lower-Back" : "Lower-Back_F")
                
               
                if IS_MALE{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Lower Back" : "Parte inferior de la espalda_lower back", file_Extension: "wav")
                }else{
                    playSound(fileName:  SELECTED_LANG == "en" ? "lower back_PL_En" : "parte inferior de la espalda (lower back)_PL_Sp", file_Extension: "mp3")
                }
                    if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                        vc.logoImage = getImage!
                        if button_tag == 5 {
                            vc.logoImage = getImage!.mirrorImg()
                        }
                        vc.typeTitle = "Lower Back".localized()
                        vc.concerns_Type = .pain_feel

                        self.navigationController?.pushViewController(vc, animated: false)
                    }
                
//                let getImage = UIImage(named: "Lower-Leg")
//
//                playSound(fileName: "Lower Leg", file_Extension: "wav")
//
//                    if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
//                        vc.logoImage = getImage!
//                        vc.typeTitle = "Lower Leg"
//                        vc.concerns_Type = .pain_feel
//
//                        self.navigationController?.pushViewController(vc, animated: false)
//                    }
            }else if [7,8].contains(button_tag){
                
                let getImage = UIImage(named: IS_MALE ? "Elbow" : "Elbow_F")

                if IS_MALE{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Elbow" : "Codo_Elbow", file_Extension: "wav")
                }else{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Elbow" : "Codo_Elbow", file_Extension: "mp3")
                }

                    if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                        vc.logoImage = getImage!
                        if button_tag == 7 {
                            vc.logoImage = getImage!.mirrorImg()
                        }
                        vc.typeTitle = "Elbow".localized()
                        vc.concerns_Type = .pain_feel

                        self.navigationController?.pushViewController(vc, animated: false)
                    }
                
            }else if [9,10].contains(button_tag){
                
                let getImage = UIImage(named: IS_MALE ? "Forearm" : "Forearm_F")

                if IS_MALE{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Forearm" : "antebrazo_forearm", file_Extension: "wav")
                }else{
                    playSound(fileName:  SELECTED_LANG == "en" ? "forearm_PL_En" : "antebrazo (forearm)_PL_Sp", file_Extension: "mp3")
                }
                
                    if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                        vc.logoImage = getImage!
                        if button_tag == 9 {
                            vc.logoImage = getImage!.mirrorImg()
                        }
                        vc.typeTitle = "Forearm".localized()
                        vc.concerns_Type = .pain_feel

                        self.navigationController?.pushViewController(vc, animated: false)
                    }
                
            }else if [11,12].contains(button_tag){
                
                let getImage = UIImage(named: IS_MALE ? "Hand" : "Hand_F")

                
                if IS_MALE{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Hand" : "mano_hand", file_Extension: "wav")
                }else{
                    playSound(fileName:  SELECTED_LANG == "en" ? "hand_PL_En" : "mano (hand)_PL_Sp", file_Extension: "mp3")
                }
                
                    if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                        vc.logoImage = getImage!
                        if button_tag == 11 {
                            vc.logoImage = getImage!.mirrorImg()
                        }
                        vc.typeTitle = "Hand".localized()
                        vc.concerns_Type = .pain_feel

                        self.navigationController?.pushViewController(vc, animated: false)
                    }
                
            }else if [13,14].contains(button_tag){
                
                let getImage =  UIImage(named: IS_MALE ? "Fingers" : "Fingers_F")
                if IS_MALE{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Fingers" : "dedos_fingers", file_Extension: "wav")
                }else{
                    playSound(fileName:  SELECTED_LANG == "en" ? "fingers_PL_En" : "dedos (fingers)_PL_Sp", file_Extension: "mp3")
                }
                    if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                        vc.logoImage = getImage!
                        if button_tag == 13 {
                            vc.logoImage = getImage!.mirrorImg()
                        }
                        vc.typeTitle = "Fingers".localized()
                        vc.concerns_Type = .pain_feel

                        self.navigationController?.pushViewController(vc, animated: false)
                    }
                
            }else if [15,16].contains(button_tag){
                
                let getImage =  UIImage(named: IS_MALE ? "hip" : "hip_F")
                
                if IS_MALE{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Hip" : "hip_cadera", file_Extension: "wav")
                }else{
                    playSound(fileName:  SELECTED_LANG == "en" ? "hip_PL_En" : "cadera (hip)_PL_Sp", file_Extension: "mp3")
                }
                    if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                        vc.logoImage = getImage!
                        if button_tag == 15 {
                            vc.logoImage = getImage!.mirrorImg()
                        }
                        vc.typeTitle = "Hip".localized()
                        vc.concerns_Type = .pain_feel

                        self.navigationController?.pushViewController(vc, animated: false)
                }
                
            }else if [17,18].contains(button_tag){
                
                let getImage = UIImage(named: IS_MALE ? "Thigh" : "Thigh_F")
                
                if IS_MALE{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Thigh" : "muslo_thigh", file_Extension: "wav")
                }else{
                    playSound(fileName:  SELECTED_LANG == "en" ? "thigh_PL_En" : "thigh_PL_En", file_Extension: "mp3")
                }
                
                    if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                        vc.logoImage = getImage!
                        if button_tag == 17 {
                            vc.logoImage = getImage!.mirrorImg()
                        }
                        vc.typeTitle = "Thigh".localized()
                        vc.concerns_Type = .pain_feel

                        self.navigationController?.pushViewController(vc, animated: false)
                    }
                
            }
            else if [19,20].contains(button_tag){
                
                let getImage = UIImage(named: IS_MALE ? "knee" : "knee_F")
                if IS_MALE{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Knee" : "Rodilla(Knee)_male_Sp", file_Extension: "wav")
                }else{
                    playSound(fileName:  SELECTED_LANG == "en" ? "knee_PL_En" : "rodilla (knee)_PL_Sp", file_Extension: "mp3")
                }

                    if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                        vc.logoImage = getImage!
                        if button_tag == 19 {
                            vc.logoImage = getImage!.mirrorImg()
                        }
                        vc.typeTitle = "Knee".localized()
                        vc.concerns_Type = .pain_feel

                        self.navigationController?.pushViewController(vc, animated: false)
                    }
                
            } else if [21,22].contains(button_tag){
                
                let getImage = UIImage(named: IS_MALE ? "Lower-Leg" : "Lower-Leg_F")
                
                if IS_MALE{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Lower Leg" : "parte inferior de la pierna_lower leg", file_Extension: "wav")
                }else{
                    playSound(fileName:  SELECTED_LANG == "en" ? "lower leg_PL_En" : "parte inferior de la pierna (lower leg)_PL_Sp", file_Extension: "mp3")
                }
                
                    if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                        vc.logoImage = getImage!
                        if button_tag == 21 {
                            vc.logoImage = getImage!.mirrorImg()
                        }
                        vc.typeTitle = "Lower Leg".localized()
                        vc.concerns_Type = .pain_feel

                        self.navigationController?.pushViewController(vc, animated: false)
           
                    }
                
            }else if [23,24].contains(button_tag){
                
                let getImage = UIImage(named: IS_MALE ? "Ankle" : "Ankle_F")
                
                if IS_MALE{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Ankle" : "Tobillo(Ankle)_male_Sp", file_Extension: "wav")
                }else{
                    playSound(fileName:  SELECTED_LANG == "en" ? "ankle_PL_En" : "tobillo (ankle)_PL_Sp", file_Extension: "mp3")
                }

                    if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                        vc.logoImage = getImage!
                        vc.typeTitle = "Ankle".localized()
                        vc.concerns_Type = .pain_feel

                        self.navigationController?.pushViewController(vc, animated: false)
                    }
                
            }else if [25,26].contains(button_tag){
                
                let getImage = UIImage(named: IS_MALE ? "Foot-Toes" : "Foot-Toes_F")
                if IS_MALE{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Foot or Toes" : "pie o dedos de los pies", file_Extension: "wav")
                }else{
                    playSound(fileName:  SELECTED_LANG == "en" ? "foot or toes_PL_En" : "pie o dedos de los pies (foot or toes)_PL_Sp", file_Extension: "mp3")
                }
                    if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                        vc.logoImage = getImage!
                        if button_tag == 25 {
                            vc.logoImage = getImage!.mirrorImg()
                        }
                        vc.typeTitle = "Foot-Toes".localized()
                        vc.concerns_Type = .pain_feel

                        self.navigationController?.pushViewController(vc, animated: false)
                    }
            }
            else if [29,30].contains(button_tag){
                
                let getImage = UIImage(named: IS_MALE ? "butt" : "butt_F")
                if IS_MALE{
                    if SELECTED_LANG == "en" {
                        playSound(fileName: "butt_male_en", file_Extension: "mp3")
                    }else{
                        playSound(fileName: "Las nalgas(butt)_Male_Sp", file_Extension: "wav")
                    }
                }else{
                    playSound(fileName:  SELECTED_LANG == "en" ? "butt_PL_En" : "las nalgas (butt)_PL_Sp", file_Extension: "mp3")
                }
                
                    if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                        vc.logoImage = getImage!
                        if button_tag == 29 {
                            vc.logoImage = getImage!.mirrorImg()
                        }
                        vc.typeTitle = "Butt".localized()
                        vc.concerns_Type = .pain_feel

                        self.navigationController?.pushViewController(vc, animated: false)
                    }
            }
            else if [31,32].contains(button_tag){
                
                let getImage = UIImage(named: IS_MALE ? "Wrist" : "Wrist_F")
                if IS_MALE{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Wrist" : "muneca_wrist", file_Extension: "wav")
                }else{
                    playSound(fileName:  SELECTED_LANG == "en" ? "wrist_PL_En" : "muneca (wrist)_PL_Sp", file_Extension: "mp3")
                }

                if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                        vc.logoImage = getImage!
                        if button_tag == 31 {
                            vc.logoImage = getImage!.mirrorImg()
                        }
                    vc.typeTitle = "Wrist".localized()
                        vc.concerns_Type = .pain_feel
                        self.navigationController?.pushViewController(vc, animated: false)
                    }
            }

        
    })
    }
}
