//
//  HumanBody.swift
//  Pocket guide Communication
//
//  Created by Deepak on 01/11/21.
//

import UIKit
import AVFoundation

class HumanBody: UIViewController {
    
    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var btn2: UIButton!
    @IBOutlet weak var btn3: UIButton!
    @IBOutlet weak var btn4: UIButton!
    @IBOutlet weak var btn5: UIButton!
    @IBOutlet weak var btn6: UIButton!
    
    @IBOutlet weak var btn7: UIButton!
    @IBOutlet weak var btn8: UIButton!
    @IBOutlet weak var btn9: UIButton!
    
    @IBOutlet weak var btn10: UIButton!
    @IBOutlet weak var btn11: UIButton!

    @IBOutlet weak var btn12: UIButton!
    @IBOutlet weak var btn13: UIButton!
    @IBOutlet weak var btn14: UIButton!
    
    
    @IBOutlet weak var btn15: UIButton!
    @IBOutlet weak var btn16: UIButton!
    @IBOutlet weak var btn17: UIButton!
    @IBOutlet weak var btn18: UIButton!
    @IBOutlet weak var btn19: UIButton!

    @IBOutlet weak var btn20: UIButton!
    @IBOutlet weak var btn21: UIButton!
    @IBOutlet weak var btn22: UIButton!
    @IBOutlet weak var btn23: UIButton!
    
    @IBOutlet weak var btn24: UIButton!
    @IBOutlet weak var btn25: UIButton!
    
    @IBOutlet weak var btn26: UIButton!
    @IBOutlet weak var btn27: UIButton!

    @IBOutlet weak var btn28: UIButton!
    @IBOutlet weak var btn29: UIButton!
    
    @IBOutlet weak var btn30: UIButton!
    @IBOutlet weak var btn31: UIButton!
    
    @IBOutlet weak var btn32: UIButton!
    @IBOutlet weak var btn33: UIButton!

    @IBOutlet weak var btn34: UIButton!
    @IBOutlet weak var btn35: UIButton!
    
    @IBOutlet weak var btn36: UIButton!
    @IBOutlet weak var btn37: UIButton!
   
    @IBOutlet weak var btn38: UIButton!
    @IBOutlet weak var btn39: UIButton!
    
    @IBOutlet weak var btn40: UIButton!
    
    @IBOutlet weak var btnup1: UIButton!
    @IBOutlet weak var btnup2: UIButton!
    
    @IBOutlet weak var handStackRightConstant: NSLayoutConstraint!
    @IBOutlet weak var handStackLeftConstant: NSLayoutConstraint!
    
    @IBOutlet weak var chestViewTopConstant: NSLayoutConstraint!
    @IBOutlet weak var chestViewBottomConstant: NSLayoutConstraint!
    
    @IBOutlet weak var bodyImg: UIImageView!
    
    @IBOutlet weak var bodyView_TopConstraint: NSLayoutConstraint!
    @IBOutlet weak var bodyView_BottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var bodyView_HeightConstraint: NSLayoutConstraint!

    var allButtons = [UIButton()]
    
//    Chest or Breasts Abdomen Pelvis or Genitals Back
//    Lower Back Shoulder Upper Arm Elbow Forearm
//    Wrist, Hand, or Fingers Hip
//    Thigh Knee Lower Leg
//    Ankle, Foot, or Toes
    
    var voiceNameFlat = ["Forehead",
                         "Ear",
                         "Eye",
                         "Nose",
                         "Eye",
                         "Ear",
                         "Mouth",
                         "Neck",
                         "Chest/Breasts",
                         "Shoulder",
                         "Shoulder",
                         "Upper Arm",
                         "Chest/Breasts",
                         "Upper Arm",
                         "Elbow",
                         "Abdomen",
                         "hip",
                         "Abdomen",
                         "hip",
                         "Elbow",
                         "Lower Leg",
                         "Lower Leg",
                         "Pelvis or Genitals",
                         "Hand",
                         "Hand",
                         "Fingers",
                         "Fingers",
                         "Foot-Toes",
                         "Foot-Toes",
                         "Ankle",
                         "Ankle",
                         "Wrist",
                         "Wrist",
                         "thigh",
                         "thigh",
                         "Knee",
                         "Knee",
                         "Forearm",
                         "Forearm"
    ]
    

    var isTap = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        allButtons = [btn1,btn2,btn3,btn4,btn5,btn6,btn7,btn8,btn9,btn10,btn11,btn12,btn13,btn14,btn15,btn16,btn17,btn18,btn19,btn20,btn21,btn22,btn23,btn24,btn25,btn26,btn27,btn28,btn29,btn30,btn31,btn32,btn33,btn34,btn35,btn36,btn37,btn38,btn39]
        
            for i in 0 ..< allButtons.count{
                
                allButtons[i].tag = i+1
                allButtons[i].setTitle("", for: .normal)
//                allButtons[i].backgroundColor = .clear
                allButtons[i].isUserInteractionEnabled = true
                allButtons[i].addTarget(self, action: #selector(btntuchAllEvent), for: .touchUpInside)
            }
        bodyView_TopConstraint.priority = UILayoutPriority(isIpad ? 250 : 1000)
        bodyView_BottomConstraint.priority = UILayoutPriority(isIpad ? 250 : 1000)
        bodyView_HeightConstraint.priority = UILayoutPriority(isIpad ? 1000 : 250)
        bodyImg.image = UIImage(named: IS_MALE ? "HumanBody" : "front_femaleB")
        handStackRightConstant.constant = IS_MALE ? 0 : (isIpad ? -80 : -40)
        handStackLeftConstant.constant = IS_MALE ? 0 : (isIpad ? -80 : -40)
        
        chestViewTopConstant.constant = isIpad ? -60 : -30
        chestViewBottomConstant.constant = isIpad ? -60 : -30
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            if IS_MALE {
                if SELECTED_LANG == "en"{
                    playSound(fileName:  "where is your pain_male_en",file_Extension: "mp3")
                }else {
                  playSound(fileName:  "Donde está tu dolor_where is your pain?", file_Extension: "wav")
                }
            }else{
                playSound(fileName:  SELECTED_LANG == "en" ? "Where is your pain_Female_en" : "Donde esta su dolor (where is your pain_)_Female_Sp", file_Extension: "mp3")
            }
        }
    }
    
    
    @objc func btntuchAllEvent(button : UIButton,forEvent event: UIEvent){
        
        print(button.tag)
//        let name = voiceNameFlat[button.tag - 1]
//        let utterance = AVSpeechUtterance(string: name)
//        utterance.voice = AVSpeechSynthesisVoice(language: "en-GB")
//        utterance.rate = 0.5
////        utterance.voice =
//        let synthesizer = AVSpeechSynthesizer()
//        synthesizer.speak(utterance)
        
        getImage(button_tag: button.tag, forEvent: event)
        
    }
    
    @IBAction func btnFaceAreaAction(_ sender: UIButton) {
    
        if let vc = SB_HumanBody.instantiateViewController(withIdentifier: "FaceBody") as? FaceBody {
            self.navigationController?.pushViewController(vc, animated: false)
        }
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func btnNextAction(_ sender: UIButton) {
        if let vc = SB_HumanBody.instantiateViewController(withIdentifier: "HumanBodyBack") as? HumanBodyBack {
            self.navigationController?.pushViewController(vc, animated: false)
        }
    }
    
}

extension HumanBody{
    
    // Matrk:- Get Image Tuch
    
    func getImage(button_tag : Int,forEvent event: UIEvent){
        
//        if !isTap {
//            return
//        }
        
        let tapButton = allButtons[button_tag - 1]
        let touches = event.touches(for: tapButton)
        let touch = touches?.first
        let touchPoint = touch?.location(in: tapButton)
        print("touchPoint\(touchPoint)")
        
        let tapImage = UIImageView(frame: CGRect(x: ((touchPoint?.x ?? 0) - 15), y: ((touchPoint?.y ?? 0) - 15), width: 30, height: 30))
        tapImage.image = UIImage(named: "TapImage")
        tapButton.addSubview(tapImage)
        print(tapButton.frame)
        
        
//        if allButtons.count > button_tag{
//            return
//        }
        
//        isTap = false

        _ = Timer.scheduledTimer(withTimeInterval: 1, repeats: false) {
            
            timer in 
            
            tapImage.removeFromSuperview()
            self.isTap = true

            if [1].contains(button_tag){

                let getImage = UIImage(named: IS_MALE ? "body_1" : "Forehead_F")
                if IS_MALE{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Forehead" : "frente", file_Extension: "wav")
                }else{
                    playSound(fileName:  SELECTED_LANG == "en" ? "forehead_PL_En" : "frente (forehead)_PL_Sp", file_Extension: "mp3")
                }
               
                
                if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                    vc.logoImage = getImage!
                    vc.typeTitle = "Forehead".localized()
                    vc.concerns_Type = .pain_feel
                    self.navigationController?.pushViewController(vc, animated: false)
                }

            }else if [4].contains(button_tag){

                let getImage = UIImage(named: IS_MALE ? "Nose" : "Nose_F")

                    if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                        vc.logoImage = getImage!
                        vc.typeTitle = "Nose".localized()
                        vc.concerns_Type = .pain_feel
                        self.navigationController?.pushViewController(vc, animated: false)
                    }

            }else if [5,3].contains(button_tag){

                let getImage = UIImage(named: IS_MALE ? "body_3" : "Eye_F")

                    if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                        vc.logoImage = getImage!
                        vc.typeTitle = "Eye".localized()
                        vc.concerns_Type = .pain_feel
                        self.navigationController?.pushViewController(vc, animated: false)
                    }


            }else if [2,6].contains(button_tag){

                let getImage = UIImage(named: IS_MALE ? "body_2" : "Ear_F")

                    if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                        vc.logoImage = getImage!
                        vc.typeTitle = "ear".localized()
                        vc.concerns_Type = .pain_feel
                        self.navigationController?.pushViewController(vc, animated: false)
                }
            }else if [7].contains(button_tag){

                let getImage = UIImage(named: IS_MALE ? "Mouth" : "Mouth_F")
                
//                playSound(fileName:  SELECTED_LANG == "en" ? "Mouth" : "boca_mouth", file_Extension: "wav")

                    if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                        vc.logoImage = getImage!
                        vc.typeTitle = "Mouth".localized()
                        vc.concerns_Type = .pain_feel
                        self.navigationController?.pushViewController(vc, animated: false)
                }
            }else if [8].contains(button_tag){

                let getImage = UIImage(named: IS_MALE ? "Neck" : "Neck_F")
                
                if IS_MALE{
                    if SELECTED_LANG == "en" {
                        playSound(fileName: "neck_male_en", file_Extension: "mp3")
                    }else{
                        playSound(fileName: "Cuello(Neck)_male_Sp", file_Extension: "wav")
                    }
                   
                }else{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Neck_PL_En" : "cuello (neck)_PL_Sp", file_Extension: "mp3")
                }

                    if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                        vc.logoImage = getImage!
                        vc.typeTitle = "Neck".localized()
                        vc.concerns_Type = .pain_feel
                        self.navigationController?.pushViewController(vc, animated: false)
                }
            }else if [9,13].contains(button_tag){
                
                if IS_MALE{
                    if SELECTED_LANG == "en" {
                        playSound(fileName: "chest or breasts_male_en", file_Extension: "mp3")
                    }else{
                        playSound(fileName: "Pecho o mama_chest or breast", file_Extension: "wav")
                    }
                }else{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Chest or breasts_PL_En" : "pecho o mama (chest or breast)_PL_Sp", file_Extension: "mp3")
                }
                let getImage = UIImage(named: IS_MALE ? "Chest-Breasts" : "Chest-Breasts_F")

                    if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                        vc.logoImage = getImage!
                        vc.typeTitle = "Chest/Breasts".localized()
                        vc.concerns_Type = .pain_feel
                        self.navigationController?.pushViewController(vc, animated: false)
                }
            }else if [10,11].contains(button_tag){

                if IS_MALE{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Shoulder" : "Hombro_shoulder", file_Extension: "wav")
                }else{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Shoulder_PL_En" : "hombro (shoulder)_PL_Sp", file_Extension: "mp3")
                }

                let getImage = UIImage(named: IS_MALE ? "Shoulder" : "Shoulder_F")

                    if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                        vc.logoImage = getImage!
                        if button_tag == 10 {
                            vc.logoImage = getImage!.mirrorImg()
                        }
                        vc.typeTitle = "Shoulder".localized()
                        vc.concerns_Type = .pain_feel
                        self.navigationController?.pushViewController(vc, animated: false)
                }
            }else if [12,14].contains(button_tag){

                let getImage = UIImage(named: IS_MALE ? "Upper-Arm" : "Upper-Arm_F")
                
                if IS_MALE{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Upper Arm" : "brazo superior_upper arm", file_Extension: "wav")
                }else{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Upper Arm_PL_En" : "brazo superior (upper arm)_PL_Sp", file_Extension: "mp3")
                }

                    if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                        vc.logoImage = getImage!
                        if button_tag == 12 {
                            vc.logoImage = getImage!.mirrorImg()
                        }
                        vc.typeTitle = "Upper Arm".localized()
                        vc.concerns_Type = .pain_feel
                        self.navigationController?.pushViewController(vc, animated: false)
                }
            }else if [15,20].contains(button_tag){

                let getImage = UIImage(named: IS_MALE ? "Elbow" : "Elbow_F")
                
               
                if IS_MALE{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Elbow" : "Codo_Elbow", file_Extension: "wav")
                }else{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Elbow" : "Codo_Elbow", file_Extension: "mp3")
                }

                    if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                        vc.logoImage = getImage!
                        if button_tag == 15 {
                            vc.logoImage = getImage!.mirrorImg()
                        }
                        vc.typeTitle = "Elbow".localized()
                        vc.concerns_Type = .pain_feel
                        self.navigationController?.pushViewController(vc, animated: false)
                }
            }else if [16,18].contains(button_tag){

                let getImage = UIImage(named: IS_MALE ? "Abdomen" : "Abdomen_F")
                
                if IS_MALE{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Abdomen" : "Abdomen_abdomen", file_Extension: "wav")
                }else{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Abdomen_PL_En" : "abdomen (abdomen)_PL_Sp", file_Extension: "mp3")
                }
                    if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                        vc.logoImage = getImage ?? UIImage()
                        vc.typeTitle = "Abdomen".localized()
                        vc.concerns_Type = .pain_feel
                        self.navigationController?.pushViewController(vc, animated: false)
                }
            }else if [17,19].contains(button_tag){

                let getImage = UIImage(named: IS_MALE ? "hip" : "hip_F")
                
                if IS_MALE{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Hip" : "hip_cadera", file_Extension: "wav")
                }else{
                    playSound(fileName:  SELECTED_LANG == "en" ? "hip_PL_En" : "cadera (hip)_PL_Sp", file_Extension: "mp3")
                }

                    if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                        vc.logoImage = getImage!
                        if button_tag == 17 {
                            vc.logoImage = getImage!.mirrorImg()
                        }
                        vc.typeTitle = "Hip".localized()
                        vc.concerns_Type = .pain_feel
                        self.navigationController?.pushViewController(vc, animated: false)
                }
            }else if [21,22].contains(button_tag){

                let getImage = UIImage(named: IS_MALE ? "Lower-Leg" : "Lower-Leg_F")
                
                
                if IS_MALE{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Lower Leg" : "parte inferior de la pierna_lower leg", file_Extension: "wav")
                }else{
                    playSound(fileName:  SELECTED_LANG == "en" ? "lower leg_PL_En" : "parte inferior de la pierna (lower leg)_PL_Sp", file_Extension: "mp3")
                }

                    if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                        vc.logoImage = getImage!
                        if button_tag == 21 {
                            vc.logoImage = getImage!.mirrorImg()
                        }
                        vc.typeTitle = "Lower Leg".localized()
                        vc.concerns_Type = .pain_feel
                        self.navigationController?.pushViewController(vc, animated: false)
                }
            }else if [23].contains(button_tag){

                let getImage = UIImage(named: IS_MALE ? "Pelvis-Genitals" : "Pelvis-Genitals_F")
                
                if IS_MALE{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Pelvis or Genitals" : "Pelvis o genitales_Pelvis or genitals", file_Extension: "wav")
                }else{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Pelvis or genitals_PL_En" : "pevis o genitales (pelvis or genitals)_PL_Sp", file_Extension: "mp3")
                }
                    if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                        vc.logoImage = getImage!
                        vc.typeTitle = "Pelvis or Genitals".localized()
                        vc.concerns_Type = .pain_feel
                        self.navigationController?.pushViewController(vc, animated: false)
                    }
            }else if [24,25].contains(button_tag){
                
                let getImage = UIImage(named: IS_MALE ? "Hand" : "Hand_F")
                
                
                if IS_MALE{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Hand" : "mano_hand", file_Extension: "wav")
                }else{
                    playSound(fileName:  SELECTED_LANG == "en" ? "hand_PL_En" : "mano (hand)_PL_Sp", file_Extension: "mp3")
                }
                    if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                        vc.logoImage = getImage!
                        if button_tag == 24 {
                            vc.logoImage = getImage!.mirrorImg()
                        }
                        vc.typeTitle = "Hand".localized()
                        vc.concerns_Type = .pain_feel
                        self.navigationController?.pushViewController(vc, animated: false)
                    }
            }else if [26,27].contains(button_tag){
                
                let getImage = UIImage(named: IS_MALE ? "Fingers" : "Fingers_F")
                
                if IS_MALE{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Fingers" : "dedos_fingers", file_Extension: "wav")
                }else{
                    playSound(fileName:  SELECTED_LANG == "en" ? "fingers_PL_En" : "dedos (fingers)_PL_Sp", file_Extension: "mp3")
                }
                    if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                        vc.logoImage = getImage!
                        if button_tag == 26 {
                            vc.logoImage = getImage!.mirrorImg()
                        }
                        vc.typeTitle = "Fingers".localized()
                        vc.concerns_Type = .pain_feel
                        self.navigationController?.pushViewController(vc, animated: false)
                    }
            }else if [28,29].contains(button_tag){
                
                let getImage = UIImage(named: IS_MALE ? "Foot-Toes" : "Foot-Toes_F")
                
                if IS_MALE{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Foot or Toes" : "pie o dedos de los pies", file_Extension: "wav")
                }else{
                    playSound(fileName:  SELECTED_LANG == "en" ? "foot or toes_PL_En" : "pie o dedos de los pies (foot or toes)_PL_Sp", file_Extension: "mp3")
                }
                    if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                        vc.logoImage = getImage!
                        if button_tag == 28 {
                            vc.logoImage = getImage!.mirrorImg()
                        }
                        vc.typeTitle = "Foot-Toes".localized()
                        vc.concerns_Type = .pain_feel
                        self.navigationController?.pushViewController(vc, animated: false)
                    }
            }else if [30,31].contains(button_tag){
                
                let getImage = UIImage(named: IS_MALE ? "Ankle" : "Ankle_F")
                
                if IS_MALE{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Ankle" : "Tobillo(Ankle)_male_Sp", file_Extension: "wav")
                }else{
                    playSound(fileName:  SELECTED_LANG == "en" ? "ankle_PL_En" : "tobillo (ankle)_PL_Sp", file_Extension: "mp3")
                }
                    if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                        vc.logoImage = getImage!
                        if button_tag == 30 {
                            vc.logoImage = getImage!.mirrorImg()
                        }
                        vc.typeTitle = "Ankle".localized()
                        vc.concerns_Type = .pain_feel
                        self.navigationController?.pushViewController(vc, animated: false)
                    }
            }else if [32,33].contains(button_tag){
                
                let getImage = UIImage(named: IS_MALE ? "Wrist" : "Wrist_F")
                
                if IS_MALE{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Wrist" : "muneca_wrist", file_Extension: "wav")
                }else{
                    playSound(fileName:  SELECTED_LANG == "en" ? "wrist_PL_En" : "muneca (wrist)_PL_Sp", file_Extension: "mp3")
                }
                    if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                        vc.logoImage = getImage!
                        if button_tag == 32 {
                            vc.logoImage = getImage!.mirrorImg()
                        }
                        vc.typeTitle = "Wrist".localized()
                        vc.concerns_Type = .pain_feel
                        self.navigationController?.pushViewController(vc, animated: false)
                    }
            }else if [34,35].contains(button_tag){
                
                let getImage = UIImage(named: IS_MALE ? "Thigh" : "Thigh_F")
                
                if IS_MALE{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Thigh" : "muslo_thigh", file_Extension: "wav")
                }else{
                    playSound(fileName:  SELECTED_LANG == "en" ? "thigh_PL_En" : "muslo (thigh)_PL_Sp", file_Extension: "mp3")
                }
                    if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                        vc.logoImage = getImage!
                        if button_tag == 34 {
                            vc.logoImage = getImage!.mirrorImg()
                        }
                        vc.typeTitle = "Thigh".localized()
                        vc.concerns_Type = .pain_feel
                        self.navigationController?.pushViewController(vc, animated: false)
                    }
            }else if [36,37].contains(button_tag){
                
                let getImage = UIImage(named: IS_MALE ? "knee" : "knee_F")
              
                if IS_MALE{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Knee" : "Rodilla(Knee)_male_Sp", file_Extension: "wav")
                }else{
                    playSound(fileName:  SELECTED_LANG == "en" ? "knee_PL_En" : "rodilla (knee)_PL_Sp", file_Extension: "mp3")
                }
                    if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                        vc.logoImage = getImage!
                        if button_tag == 36 {
                            vc.logoImage = getImage!.mirrorImg()
                        }
                        vc.typeTitle = "Knee".localized()
                        vc.concerns_Type = .pain_feel
                        self.navigationController?.pushViewController(vc, animated: false)
                    }
            }else if [38,39].contains(button_tag){
            
                let getImage = UIImage(named: IS_MALE ? "Forearm" : "Forearm_F")
                
                if IS_MALE{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Forearm" : "antebrazo_forearm", file_Extension: "wav")
                }else{
                    playSound(fileName:  SELECTED_LANG == "en" ? "forearm_PL_En" : "antebrazo (forearm)_PL_Sp", file_Extension: "mp3")
                }
                    if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                        vc.logoImage = getImage!
                        if button_tag == 38 {
                            vc.logoImage = getImage!.mirrorImg()
                        }
                        vc.typeTitle = "Forearm".localized()
                        vc.concerns_Type = .pain_feel
                        self.navigationController?.pushViewController(vc, animated: false)
                    }
            
            }
            
            
            
            
        }
        
//        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: )
        
        
        
        
        
        
    }
    
    
}
 
