//
//  FaceBody.swift
//  Pocket guide Communication
//
//  Created by Deepak on 11/11/21.
//

import UIKit
import AVFoundation

class FaceBody: UIViewController {
    
    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var btn2: UIButton!
    @IBOutlet weak var btn3: UIButton!
    
    @IBOutlet weak var btn4: UIButton!
    @IBOutlet weak var btn5: UIButton!
    
    @IBOutlet weak var btn6: UIButton!
    @IBOutlet weak var btn7: UIButton!
    
    @IBOutlet weak var btn8: UIButton!
    
    @IBOutlet weak var btn9: UIButton!
    
    @IBOutlet weak var btn10: UIButton!
    @IBOutlet weak var btn11: UIButton!
    
    @IBOutlet weak var btn12: UIButton!
    @IBOutlet weak var btn13: UIButton!
    
    @IBOutlet weak var faceImg: UIImageView!
    
        var voiceNameBack = ["Forehead","Ear","Ear","Eye","Eye","Cheek","Cheek","Mouth","Chin","Neck","Nose","Cheek","Cheek"]
    var allButtons = [UIButton()]

    override func viewDidLoad() {
        super.viewDidLoad()
        faceImg.image = UIImage(named: IS_MALE ? "Face_body" : "female_head")
        allButtons = [btn1,btn2,btn3,btn4,btn5,btn6,btn7,btn8,btn9,btn10,btn11,btn12,btn13]
        for i in 0 ..< allButtons.count{
            allButtons[i].tag = i+1
            allButtons[i].setTitle("", for: .normal)
//            allButtons[i].titleLabel?.textColor = .clear
            allButtons[i].addTarget(self, action: #selector(self.btntuchAllEvent), for: .touchUpInside)
        }

    }

    @objc func btntuchAllEvent(button : UIButton,forEvent event: UIEvent){
        
        print(button.tag)
//        let name = voiceNameBack[button.tag - 1]
//        let utterance = AVSpeechUtterance(string: name)
//        utterance.voice = AVSpeechSynthesisVoice(language: "en-GB")
//        utterance.rate = 0.5
////        utterance.voice =
//        let synthesizer = AVSpeechSynthesizer()
//        synthesizer.speak(utterance)
        
        getImage(button_tag: button.tag, forEvent: event)

    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: false)
        
    }
}
extension FaceBody{
    
    // Matrk:- Get Image Tuch
    
    func getImage(button_tag : Int,forEvent event: UIEvent){
        
        let tapButton = allButtons[button_tag - 1]
        let touches = event.touches(for: tapButton)
        let touch = touches?.first
        let touchPoint = touch?.location(in: tapButton)
       // print("touchPoint\(touchPoint)")
        
        let tapImage = UIImageView(frame: CGRect(x: ((touchPoint?.x ?? 0) - 15), y: ((touchPoint?.y ?? 0) - 15), width: 30, height: 30))
//        let tapImage = UIImageView(frame: CGRect(x: -15, y: -15, width: 30, height: 30))
        tapImage.image = UIImage(named: "TapImage")
        tapButton.addSubview(tapImage)
        print(tapButton.frame)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
            
            tapImage.removeFromSuperview()
            
            if [1].contains(button_tag){
                
                let getImage = UIImage(named: IS_MALE ? "body_1" : "Forehead_F")
                
                if IS_MALE{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Forehead" : "frente", file_Extension: "wav")
                }else{
                    playSound(fileName:  SELECTED_LANG == "en" ? "forehead_PL_En" : "frente (forehead)_PL_Sp", file_Extension: "mp3")
                }
                
                    if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                        vc.logoImage = getImage!
                        vc.typeTitle = self.voiceNameBack[button_tag - 1]
                        vc.concerns_Type = .pain_feel

                        self.navigationController?.pushViewController(vc, animated: false)
                    }
            }else if [2,3].contains(button_tag){
                
                if IS_MALE{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Ear" : "oreja_ear", file_Extension: "wav")
                }else{
                    playSound(fileName:  SELECTED_LANG == "en" ? "ear_PL_En" : "oreja (ear)_PL_Sp", file_Extension: "mp3")
                }

                
                let getImage = UIImage(named: IS_MALE ? "body_2" : "Ear_F")
                
                    if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                        vc.logoImage = getImage!
                        if button_tag == 3 {
                            vc.logoImage = getImage!.mirrorImg()
                        }
                        vc.typeTitle = self.voiceNameBack[button_tag - 1]
                        vc.concerns_Type = .pain_feel

                        self.navigationController?.pushViewController(vc, animated: false)
                    }
            }else if [5,4].contains(button_tag){
                
               
                
                if IS_MALE{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Eye" : "ojo_eye", file_Extension: "wav")
                }else{
                    playSound(fileName:  SELECTED_LANG == "en" ? "eye_PL_En" : "ojo (eye)_PL_Sp", file_Extension: "mp3")
                }
                let getImage = UIImage(named: IS_MALE ? "body_3" : "Eye_F")
                
                    if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                        vc.logoImage = getImage!
                        vc.typeTitle = self.voiceNameBack[button_tag - 1]
                        vc.concerns_Type = .pain_feel

                        self.navigationController?.pushViewController(vc, animated: false)
                    }
            }else if [6,7,12,13].contains(button_tag){
                
                if IS_MALE{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Cheeks" : "Mejilla(Cheek)_male_Sp", file_Extension: "wav")
                }else{
                    playSound(fileName:  SELECTED_LANG == "en" ? "cheek_PL_En" : "mejilla (cheek)_PL_Sp", file_Extension: "mp3")
                }
                let getImage = UIImage(named: IS_MALE ? "Cheeks" : "Cheeks_F")
                
                    if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                        vc.logoImage = getImage!
                        if button_tag == 7 || button_tag == 13 {
                            vc.logoImage = getImage!.mirrorImg()
                        }
                        vc.typeTitle = self.voiceNameBack[button_tag - 1]
                        vc.concerns_Type = .pain_feel

                        self.navigationController?.pushViewController(vc, animated: false)
                    }
            }else if [8].contains(button_tag){
                
                let getImage = UIImage(named: IS_MALE ? "Mouth" : "Mouth_F")
                
                
                if IS_MALE{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Mouth" : "boca_mouth", file_Extension: "wav")
                }else{
                    playSound(fileName:  SELECTED_LANG == "en" ? "mouth_PL_En" : "boca (mouth)_PL_Sp", file_Extension: "mp3")
                }
                    if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                        vc.logoImage = getImage!
                        vc.typeTitle = self.voiceNameBack[button_tag - 1]
                        vc.concerns_Type = .pain_feel

                        self.navigationController?.pushViewController(vc, animated: false)
                    }
            }else if [9].contains(button_tag){
                
                let getImage = UIImage(named: IS_MALE ? "Chin" : "Chin_F")
               
                if IS_MALE{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Chin" : "Barbilla(chin)_male_Sp", file_Extension: "wav")
                }else{
                    playSound(fileName:  SELECTED_LANG == "en" ? "chin_PL_En" : "barbilla (chin)_PL_Sp", file_Extension: "mp3")
                }
                    if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                        vc.logoImage = getImage!
                        vc.typeTitle = self.voiceNameBack[button_tag - 1]
                        vc.concerns_Type = .pain_feel

                        self.navigationController?.pushViewController(vc, animated: false)
                    }
            }else if [10].contains(button_tag){
                
                let getImage = UIImage(named: IS_MALE ? "Neck" : "Neck_F")
               
                if IS_MALE{
                    if SELECTED_LANG == "en" {
                        playSound(fileName: "neck_male_en", file_Extension: "mp3")
                    }else{
                        playSound(fileName: "Cuello(Neck)_male_Sp", file_Extension: "wav")
                    }
                }else{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Neck_PL_En" : "cuello (neck)_PL_Sp", file_Extension: "mp3")
                }
                    if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                        vc.logoImage = getImage!
                        vc.typeTitle = self.voiceNameBack[button_tag - 1]
                        vc.concerns_Type = .pain_feel

                        self.navigationController?.pushViewController(vc, animated: false)
                    }
            }else if [11].contains(button_tag){
                
                let getImage = UIImage(named: IS_MALE ? "Nose" : "Nose_F")
               
                 if IS_MALE{
                     playSound(fileName:  SELECTED_LANG == "en" ? "Nose" : "nariz_nose", file_Extension: "wav")
                 }else{
                     playSound(fileName:  SELECTED_LANG == "en" ? "nose_PL_En" : "nariz (nose)_PL_Sp", file_Extension: "mp3")
                 }
                    if let vc = SB_Concerns_Details.instantiateViewController(withIdentifier: "Concerns_Details") as? Concerns_Details {
                        vc.logoImage = getImage!
                        vc.typeTitle = self.voiceNameBack[button_tag - 1]
                        vc.concerns_Type = .pain_feel

                        self.navigationController?.pushViewController(vc, animated: false)
                    }
            }
            
        })
        
    }
}
