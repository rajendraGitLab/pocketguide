//
//  Contacts.swift
//  Pocket guide Communication
//
//  Created by Deepak on 29/10/21.
//

import UIKit

class Contacts: UIViewController {
    
    @IBOutlet weak var btnBack: UIButton!

    @IBOutlet weak var btnNext: UIButton!
    
    @IBOutlet weak var lblemail: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnBack.isHidden = !isNew
        btnNext.isHidden = isNew
        
//        isNew = false
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapOnEmail(sender:)))
        lblemail.isUserInteractionEnabled = true
        lblemail.addGestureRecognizer(tap)


    }
    
    @objc func tapOnEmail(sender:UITapGestureRecognizer) {
            
        print("tap working")
        if let url = URL(string: "mailto:\(lblemail.text ?? "")") {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func btnNextAction(_ sender: UIButton) {
//        if let vc = SB_Healthcare_Providers_Aphasia.instantiateViewController(withIdentifier: "Healthcare_Providers_Aphasia") as? Healthcare_Providers_Aphasia{
//            self.navigationController?.pushViewController(vc, animated: false)
//        }
        methodMakeRootTabbar(index: 1)
    }

}
