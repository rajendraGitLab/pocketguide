//
//  Yes_no_Static.swift
//  Pocket guide Communication
//
//  Created by Deepak on 13/11/21.
//

import UIKit

class Yes_no_Static: UIViewController {
    
    @IBOutlet weak var bgImage1: UIImageView!
    @IBOutlet weak var bgImage2: UIImageView!
    @IBOutlet weak var bgImage3: UIImageView!

    @IBOutlet weak var lblText: UILabel!
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    var selectIndex = 1
    
    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var btn2: UIButton!
    @IBOutlet weak var btn3: UIButton!
    
    var allButton = [UIButton()]
    
    @IBOutlet var viewall: [UIView]!
    
    @IBOutlet weak var viewHeader: UIView!
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        allButton = [btn1,btn2,btn3]
        
        
        
        
        
        for i in 0 ... allButton.count - 1 {
            
            allButton[i].tag = i+1
            allButton[i].addTarget(self, action: #selector(btnSelectBtnall(_:)), for: .touchUpInside)
            viewall[i].backgroundColor = .white
        }

    }
    
    @IBAction func btnMenuAction(_ sender: UIButton) {
    
            Util.showSideMenu(navC: self)
        }
    
    @IBAction func btnSelectBtnall(_ sender: UIButton) {
        
        if sender.tag == 1 {
            if IS_MALE{
                playSound(fileName:  SELECTED_LANG == "en" ? "Yes" : "yes_si", file_Extension: "wav")
            }else{
                playSound(fileName: SELECTED_LANG == "en" ? "Yes_HRU_En" : "si (yes)_HRU_Sp", file_Extension: "mp3")
            }
        }else if sender.tag == 2 {
            if IS_MALE{
                playSound(fileName:  SELECTED_LANG == "en" ? "No" : "No_no", file_Extension: "wav")
            }else{
                playSound(fileName: SELECTED_LANG == "en" ? "No_HRU_En" : "No (no)_HRU_Sp", file_Extension: "mp3")
            }
        }else{
            if IS_MALE{
                playSound(fileName:  SELECTED_LANG == "en" ? "I_Do_not_Know" : "No se(i don't know)_male_Sp", file_Extension: "wav")
            }else{
                playSound(fileName:  SELECTED_LANG == "en" ? "I dont know_Female_en" : "no se(i don't know)_Female_Sp", file_Extension: "wav")
            }
        }
        
        for i in viewall {
            if i.tag == sender.tag {
                i.borderColor = hexStringToUIColor(hex: "#0F9AF0")
                i.borderWidth = isIpad ? 8 : 4
            }else{
                i.borderColor = .clear
                i.borderWidth = isIpad ? 8 : 4
            }
            
            if sender.tag == 3 {
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    TabbarObj.selectedIndex = 3
                    self.clearSelection()
                }
            }
            
        }
        
    }
    
    func clearSelection(){
        viewall.forEach { view in view.borderColor = .clear }
    }
    
   
}
