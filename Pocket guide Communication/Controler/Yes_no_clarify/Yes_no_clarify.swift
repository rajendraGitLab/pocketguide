//
//  Yes_no_clarify.swift
//  Pocket guide Communication
//
//  Created by Deepak on 29/10/21.
//

import UIKit

class Yes_no_clarify: UIViewController {
    
    @IBOutlet weak var bgImage1: UIImageView!
    @IBOutlet weak var bgImage2: UIImageView!
    @IBOutlet weak var bgImage3: UIImageView!

    @IBOutlet weak var lblText: UILabel!
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    var selectIndex = 1
    
    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var btn2: UIButton!
    @IBOutlet weak var btn3: UIButton!
    
    var allButton = [UIButton()]

    override func viewDidLoad() {
        super.viewDidLoad()
        
//        btnBack.isHidden = !isNew
//        btnNext.isHidden = !isNew
       
        allButton = [btn1,btn2,btn3]
        setIndexData(selectIndex: 1)
        
        for i in 0 ... allButton.count - 1 {
            
            allButton[i].tag = i
            allButton[i].addTarget(self, action: #selector(btnSelectBtnall(_:)), for: .touchUpInside)
        }
    }
    
    @IBAction func btnSelectBtnall(_ sender: UIButton) {
        if sender.tag == 0 {
            if IS_MALE{
                playSound(fileName:  SELECTED_LANG == "en" ? "Yes" : "yes_si", file_Extension: "wav")
            }else{
                playSound(fileName: SELECTED_LANG == "en" ? "Yes_HRU_En" : "si (yes)_HRU_Sp", file_Extension: "mp3")
            }
        }
        else if sender.tag == 1{
            if IS_MALE{
                playSound(fileName:  SELECTED_LANG == "en" ? "No" : "No_no", file_Extension: "wav")
            }else{
                playSound(fileName: SELECTED_LANG == "en" ? "No_HRU_En" : "No (no)_HRU_Sp", file_Extension: "mp3")
            }
            
        }else if sender.tag == 2{
            if IS_MALE{
                playSound(fileName:  SELECTED_LANG == "en" ? "I_Do_not_Know" : "No se(i don't know)_male_Sp", file_Extension: "wav")
            }else{
                playSound(fileName:  SELECTED_LANG == "en" ? "I dont know_Female_en" : "no se(i don't know)_Female_Sp", file_Extension: "wav")
            }
        }
        
        selectIndex = sender.tag
        btnNextAction(sender)
        
    }
    
    @IBAction func btnNextAction(_ sender: UIButton) {
        if selectIndex == 1 {
            setIndexData(selectIndex: 2)
            selectIndex = selectIndex + 1
            
        }else if selectIndex == 2 {
            setIndexData(selectIndex: 3)
            selectIndex = selectIndex + 1
        }else if selectIndex == 3{
            // Go tu Root VC
            selectIndex = 1
            setIndexData(selectIndex: 1)
            
            if isNew == true{
                
                if let vc = SB_Fill_Details.instantiateViewController(withIdentifier: "Fill_Details") as? Fill_Details {
                    
                    self.navigationController?.pushViewController(vc, animated: false)
                }
            }else{
                if let vc = SB_Tab.instantiateViewController(withIdentifier: "Home_page") as? Home_page {
                    self.navigationController?.pushViewController(vc, animated: false)
                }

                
//                if let vc = Tab_bar.instantiateViewController(withIdentifier: "Concerns") as? Concerns {
//
//                    self.navigationController?.pushViewController(vc, animated: false)
//                }
            }
            
        }else{
            selectIndex = 1
            setIndexData(selectIndex: 1)
            
//            if isNew {
//
//                if let Concerns_vc = SB_Tips.instantiateViewController(withIdentifier: "Tips") as? Tips{
//                    self.navigationController?.pushViewController(Concerns_vc, animated: false)
//
//                }
//            }
            
        }
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        
        if selectIndex == 1 {
            setIndexData(selectIndex: 1)
            selectIndex = 1
            self.navigationController?.popViewController(animated: false)
        }else if selectIndex == 2 {
            setIndexData(selectIndex: 1)
            selectIndex = selectIndex - 1
        }else if selectIndex == 3{
            setIndexData(selectIndex: 2)
            selectIndex = selectIndex - 1
        }
    }
    
    func setIndexData(selectIndex : Int){
        
        if selectIndex == 1{
            
            btnNext.isHidden = false
            btnNext.setImage(UIImage(named: "next"), for: .normal)

            if btn1.isSelected == false{
                if IS_MALE{
                    playSound(fileName:  SELECTED_LANG == "en" ? "Yes" : "yes_si", file_Extension: "wav")
                }else{
                    playSound(fileName: SELECTED_LANG == "en" ? "Yes_HRU_En" : "si (yes)_HRU_Sp", file_Extension: "mp3")
                }
            }else{
               
            }
            
            let string = " to verify your understanding of the PWA’s intended response\n\nRephrase your understanding as a question, ex: “Yes, you have burning pain?” (nodding and pointing)\n\n If “no”, return to previous concerns page and try again.".localized()
            let attrStri = NSMutableAttributedString.init(string:string)
            let nsRange1 = NSString(string: string).range(of: "")
            let nsRange2 = NSString(string: string).range(of: "")
            let nsRange3 = NSString(string: string).range(of: "")
            attrStri.addAttributes([NSAttributedString.Key.font: UIFont.init(name: "Navigo-Bold", size: 15.0) as Any], range: nsRange1)
            attrStri.addAttributes([NSAttributedString.Key.font: UIFont.init(name: "Navigo-Bold", size: 15.0) as Any], range: nsRange2)
            attrStri.addAttributes([NSAttributedString.Key.font: UIFont.init(name: "Navigo-Bold", size: 15.0) as Any], range: nsRange3)
            
            
            let normalNameString = NSMutableAttributedString.init(string: "Use ")

            let attachment = NSTextAttachment()
            attachment.image = imageHelper.pgImage(textValue: "YES".localized(),txtColor: hexStringToUIColor(hex: "#26A52A"))
            attachment.bounds = CGRect(x: 0, y: -1, width: (attachment.image?.size.width)!, height: (attachment.image?.size.height)!)
            normalNameString.append(NSAttributedString(attachment: attachment))
            normalNameString.append(attrStri)
            
            
            self.lblText.attributedText = normalNameString
            self.lblText.textColor = hexStringToUIColor(hex: "#26A52A")
            
//            bgImage1.isHidden = false
            bgImage1.backgroundColor = .clear
            bgImage1.image = UIImage(named: "bg_1")
            bgImage2.backgroundColor = .white
            bgImage2.image = nil
            bgImage3.backgroundColor = .white
            bgImage3.image = nil
//            bgImage2.isHidden = true
//            bgImage3.isHidden = true

        }else if selectIndex == 2{
            
            btnNext.isHidden = false
            btnNext.setImage(UIImage(named: "next"), for: .normal)
            if IS_MALE{
                playSound(fileName:  SELECTED_LANG == "en" ? "No" : "No_no", file_Extension: "wav")
            }else{
                playSound(fileName: SELECTED_LANG == "en" ? "No_HRU_En" : "No (no)_HRU_Sp", file_Extension: "mp3")
            }
            let string = " to verify your understanding of the PWA’s intended response\n\nRephrase your understanding as a question, ex., “No, you have no concerns?” (shake head and point to no)".localized()
            let attrStri = NSMutableAttributedString.init(string:string)
            let nsRange1 = NSString(string: string).range(of: "")
            let nsRange2 = NSString(string: string).range(of: "")
            attrStri.addAttributes([NSAttributedString.Key.font: UIFont.init(name: "Navigo-Bold", size: 15.0) as Any], range: nsRange1)
            attrStri.addAttributes([NSAttributedString.Key.font: UIFont.init(name: "Navigo-Bold", size: 15.0) as Any], range: nsRange2)
            
            let normalNameString = NSMutableAttributedString.init(string: "Use ")

            let attachment = NSTextAttachment()
            attachment.image = imageHelper.pgImage(textValue: "NO",txtColor: hexStringToUIColor(hex: "#E40014"))
            attachment.bounds = CGRect(x: 0, y: -1, width: (attachment.image?.size.width)!, height: (attachment.image?.size.height)!)
            normalNameString.append(NSAttributedString(attachment: attachment))
            normalNameString.append(attrStri)
            
            self.lblText.attributedText = normalNameString
            self.lblText.textColor = hexStringToUIColor(hex: "#E40014")
            
            bgImage1.backgroundColor = .white
            bgImage1.image = nil
            bgImage2.backgroundColor = .clear
            bgImage2.image = UIImage(named: "bg_1")
            bgImage3.backgroundColor = .white
            bgImage3.image = nil
//            bgImage1.isHidden = true
//            bgImage2.isHidden = false
//            bgImage3.isHidden = true

        }else if selectIndex == 3{
            
            if IS_MALE{
                playSound(fileName:  SELECTED_LANG == "en" ? "I_Do_not_Know" : "No se(i don't know)_male_Sp", file_Extension: "wav")
            }else{
                playSound(fileName:  SELECTED_LANG == "en" ? "I dont know_Female_en" : "no se(i don't know)_Female_Sp", file_Extension: "wav")
            }
            if isNew{
            }else{
                btnNext.setImage(UIImage(named: "next"), for: .normal)
            }
            btn1.isSelected = true
            
            
            let string = " could mean: “I don’t know”, “I’m not sure”, “I don’t understand, or “something else”.\n\nAsk your question in a different way to help with understanding or to verify that the intended response is truly “I don’t know”.\n\nAsk a different question or move to a new topic.".localized()
            let attrStri = NSMutableAttributedString.init(string:string)
            let nsRange1 = NSString(string: string).range(of: "")
            let nsRange2 = NSString(string: string).range(of: "")
            let nsRange3 = NSString(string: string).range(of: "")
            attrStri.addAttributes([NSAttributedString.Key.font: UIFont.init(name: "Navigo-Bold", size: 15.0) as Any], range: nsRange1)
            attrStri.addAttributes([NSAttributedString.Key.font: UIFont.init(name: "Navigo-Bold", size: 15.0) as Any], range: nsRange2)
            attrStri.addAttributes([NSAttributedString.Key.font: UIFont.init(name: "Navigo-Bold", size: 15.0) as Any], range: nsRange3)
            
            let normalNameString = NSMutableAttributedString.init(string: "")
            let attachment = NSTextAttachment()
            attachment.image = imageHelper.pgImage(textValue: "?",txtColor: hexStringToUIColor(hex: "#1435F8"))
            attachment.bounds = CGRect(x: 0, y: -1, width: (attachment.image?.size.width)!, height: (attachment.image?.size.height)!)
            normalNameString.append(NSAttributedString(attachment: attachment))
            normalNameString.append(attrStri)
            
            
            self.lblText.attributedText = normalNameString
            self.lblText.textColor = hexStringToUIColor(hex: "#1435F8")
            bgImage1.backgroundColor = .white
            bgImage1.image = nil
            bgImage2.backgroundColor = .white
            bgImage2.image = nil
            bgImage3.backgroundColor = .clear
            bgImage3.image = UIImage(named: "bg_1")

        }
    }
}
