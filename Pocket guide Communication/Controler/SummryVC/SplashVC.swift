//
//  SplashVC.swift
//  Pocket guide Communication
//
//  Created by Admin on 21/09/22.
//

import UIKit
import Alamofire

class SplashVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        APPLE_EMAIL = UserDefaults.standard.value(forKey: "RappleEmail") as? String ?? ""
        APPLE_NAME = UserDefaults.standard.value(forKey: "RappleName") as? String ?? ""
        APPLE_ID = UserDefaults.standard.value(forKey: "RappleId") as? String ?? ""
        SUBSCRIPTION_STATUS =  UserDefaults.standard.value(forKey: "SUBSCRIPTION_STATUS") as? Bool ?? false
        
        isShowGuideScreen = userDefault.object(forKey: "isShowGuideScreen") as? Bool ?? true
        isNew = userDefault.object(forKey: "isNew") as? Bool ?? true
        isAppleLogin = userDefault.object(forKey: "isAppleLogin") as? Bool ?? false
        noOfCellsInRow = DEFAULTS.value(forKey: "collectionViewItemCount") as? Int ?? 3
        IS_MALE = DEFAULTS.value(forKey: "userGender") as? Bool ?? false
        SELECTED_LANG = DEFAULTS.value(forKey: "userLanguage") as? String ?? "en"
        if isNew {
            self.goToFisrtScr()
        }else{
            self.gotoRootHome()
        }
//        getTransactionStatus()
    }
    
    private func gotoRootHome(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            if let vc = SB_Fill_Details.instantiateViewController(withIdentifier: "Fill_Details") as? Fill_Details {
                let nav = UINavigationController(rootViewController: vc)
                nav.interactivePopGestureRecognizer?.delegate = nil
                nav.setNavigationBarHidden(true, animated: false)
                self.window?.rootViewController = nav
                self.window?.makeKeyAndVisible()
            }
        }
    }
    private func gotoSubscriptionPage(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            if let vc = SB_Main.instantiateViewController(withIdentifier: "SubscriptionVC") as? SubscriptionVC {
                let nav = UINavigationController(rootViewController: vc)
                nav.interactivePopGestureRecognizer?.delegate = nil
                nav.setNavigationBarHidden(true, animated: false)
                self.window?.rootViewController = nav
                self.window?.makeKeyAndVisible()
            }
        }
    }
    
    private func goToFisrtScr(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 2){
            if let vc = SB_Tips.instantiateViewController(withIdentifier: "HowDoesWorkVC") as? HowDoesWorkVC {
                let nav = UINavigationController(rootViewController: vc)
                nav.interactivePopGestureRecognizer?.delegate = nil
                nav.setNavigationBarHidden(true, animated: false)
                self.window?.rootViewController = nav
                self.window?.makeKeyAndVisible()
            }
        }
    }
    func getTransactionStatus(){
        
        if !NetworkUtils.isNetworkReachable() {
            Toastnew.show(message: "No internet connection", controller: self)
            return
        }
        let parm = ["user_id" : APPLE_ID]
        print("parm : \(parm)")
        AF.request(getTransactionIAP, method: .post, parameters: parm , encoding: URLEncoding.httpBody,headers: nil).responseJSON {
                        response in
                        Util.hideHud(view: self.view)
                        switch(response.result) {
                        case .success(_):
                            if let data = response.value
                            {
                                DispatchQueue.main.async {
                                    print(data)
                                    if let getDataresponse = data as? NSDictionary{
                                        let data = getDataresponse.value(forKey: "data") as? NSArray
                                        
                                        let inAppPurchaseStatus = data?.value(forKey: "status") as? NSArray
                                        SUBSCRIPTION_STATUS = inAppPurchaseStatus?.firstObject as? Bool ?? false
                                        if SUBSCRIPTION_STATUS {
                                            UserDefaults.standard.setValue(true, forKey: "SUBSCRIPTION_STATUS")
                                        }else{
                                            UserDefaults.standard.setValue(false, forKey: "SUBSCRIPTION_STATUS")
                                        }
                                        if isNew {
                                            if SUBSCRIPTION_STATUS{
                                                self.goToFisrtScr()
                                            }else{
                                                if isAppleLogin{
                                                    self.gotoSubscriptionPage()
                                                }else{
                                                    self.goToFisrtScr()
                                                }
                                            }

                                        }else{
                                            if SUBSCRIPTION_STATUS{
                                                self.gotoRootHome()
                                            }else{
                                                self.gotoSubscriptionPage()
                                            }

                                        }
                                    }else{
                                    }
                                }
                            }
                            break
                        case .failure(_):
                            if response.error != nil
                            {
                                let statusCode = response.response?.statusCode ?? 0
                            }
                            break
                        }
        
                    }
    }
}
