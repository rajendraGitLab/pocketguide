//
//  SummeryVC.swift
//  Pocket guide Communication
//
//  Created by Admin on 26/08/22.
//

import UIKit

class SummeryVC: UIViewController {

    @IBOutlet weak var topViewImg: UIImageView!
    @IBOutlet weak var topViewTitle: UILabel!
    @IBOutlet weak var concernTypeLbl: UILabel!
    @IBOutlet weak var concernTypeLblView: UIView!
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var scroll_View: UIScrollView!
    
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var listBtn: UIButton!
    
    var problemsArr = [SummeryModel]()
    var summeryInf = [SummeryModel]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        concernTypeLblView.layer.cornerRadius = isIpad ? 13 : 6
        topViewImg.image = howRU.img
        topViewTitle.text = howRU.title?.localized()
        concernTypeLbl.text = SummeryDict["type_1"]?.title?.localized()
//        let sortedDict = SummeryDict.sorted(by: {$0.key.last! < $1.key.last!})
//        problemsArr = sortedDict.compactMap({$0.value})
        problemsArr = SummeryInfo.shared.getAll()
//        let arrName = summeryInf.map{$0.imgTitle}
        
//        saveBtn.isHidden = true
//        listBtn.isHidden = true
    }
    @IBAction func btnNextAction(_ sender: UIButton) {
      //      methodMakeRootTabbar(index: 0)
        if let vc = SB_SaveBoard.instantiateViewController(withIdentifier: "SaveBoard") as? SaveBoard {
            vc.imageData = scroll_View.asImage().pngData()!
            vc.comeFromSummry = true
            vc.enterTest = "Summery"
            self.navigationController?.pushViewController(vc, animated: false)
        }
    }
    @IBAction func btnListsAction(_ sender: UIButton) {
      //      methodMakeRootTabbar(index: 0)
        
        if let vc = SB_Whiteboard_List.instantiateViewController(withIdentifier: "WhiteboardList") as? WhiteboardList {
            vc.comeFromSummary = true
            self.navigationController?.pushViewController(vc, animated: false)
        }
       
    }

    @IBAction func btnBackAction(_ sender: UIButton) {
//        if concerns_Type == .All_Concerns {
//            Util.showSideMenu(navC: self)
//        }else{
            self.navigationController?.popViewController(animated: false)
//    }
    }
}
extension SummeryVC : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return problemsArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Health_type_Cell", for: indexPath) as! Health_type_Cell
        cell.title.text = self.problemsArr[indexPath.row].imgTitle.localized()
        cell.image.image = self.problemsArr[indexPath.row].imgName
        cell.bgView.dropShadow()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

//        let collectionWidth = collectionView.bounds.height
//        let cellsPerRow = problemsArr.count
//
//        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
//
//        let totalSpace = flowLayout.sectionInset.left
//              + flowLayout.sectionInset.right
//        + ( (cellsPerRow <= 2 ? collectionWidth/4 : 10) * CGFloat(cellsPerRow - 1))
//
//          let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(cellsPerRow))

//        return CGSize(width: size, height: size + (isIpad ? 30 : 15))
        
        return CGSize(width: isIpad ? 180 : 90, height: isIpad ? 200 : 100)
    }
  
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return isIpad ? 20 : 10
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
}
